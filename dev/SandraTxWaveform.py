###############################################################################
#                                                                             #
#   PROJECT     :   SanDRA                                                    #
#   FILE        :   PulseGenerator.py                                         #
#   AUTHOR      :   M.Eng. Nico Pfeffer                                       #
#   INSTITUTION :   Leibniz-Institute of Atmospheric Physics                  #
#   DEPARTMENT  :   Radar Remote Sensing                                      #
#   GROUP       :   Radio Science Group                                       #
#                                                                             #
###############################################################################

import os
import sys
import numpy
import scipy
import scipy.signal

from gnuradio import blocks
from gnuradio import filter
from gnuradio import gr
from gnuradio.filter import firdes

###############################################################################







###############################################################################

'''
class SandraTxWaveform(gr.hier_block2):

    ###########################################################################
    # DESCRIPTORS                                                             #
    ###########################################################################

    __SEED_DTYPE = int
    __SEED_INTERVAL = '(-inf,+inf)'

    __CODE_DTYPE = str
    __CODE_OPTIONS = ['cw','barker','custom','pseudo','complementary']

    __SHAPE_DTYPE = str
    __SHAPE_OPTIONS = ['rrg','rrc','box']

    __LENGTH_DTYPE = int
    __LENGTH_INTERVAL = '[0,+inf)'
    __LENGTH_OPTIONS_BARKER = [2,3,4,5,7,11,13]
    __LENGTH_OPTIONS_COMPLEMENTARY = [2,4,8,16,32]

    __PERIOD_DTYPE = int
    __PERIOD_INTERVAL = '[0,+inf)'

    __ROLLOFF_DTYPE = float
    __ROLLOFF_INTERVAL = '[0.0,1.0]'
    
    __INTERPOLATION_DTYPE = int
    __INTERPOLATION_INTERVAL = '[1,+inf)'

    __SYMBOLRATE_DTYPE = float
    __SYMBOLRATE_INTERVAL = '[)'

    __AMPLITUDE_DTYPE = float
    __AMPLITUDE_INTERVAL = '[0.0,1.0]'

    __PHASE_DTYPE = float
    __PHASE_INTERVAL = '[-180.0,+180.0)'

    __DELAY_DTYPE = int
    __DELAY_INTERVAL = '[0,+inf)'

    __ENABLE_DTYPE = bool
    __ENABLE_OPTIONS = [True,False]

    ###########################################################################

    def __str__(self,):
        info  = ""
        return info

    ###########################################################################
    # INITIALIZE                                                              #
    ###########################################################################

    def __init__(self,
            code="pseudo",
            interpolation=1,
            length=1000,
            period=1000,
            rolloff=0.518,
            seed=0,
            shape="root-raised-gaussian",
            symbolrate=200000.0):
        
        gr.hier_block2.__init__(
            self, "SanDRA Tx Waveform",
            gr.io_signature(0, 0, 0),
            gr.io_signature(1, 1, gr.sizeof_gr_complex*1),
        )

        #---------------------------------------------------------------------#
        # Defaults                                                            #
        #---------------------------------------------------------------------#

        self.cpu = [0]
        self.delay = 0
        self.phase = 0.0
        self.enable = True
        self.doppler = 0.0
        self.amplitude = 0.0

        #---------------------------------------------------------------------#
        # Parameters                                                          #
        #---------------------------------------------------------------------#

        self.code = str(code)
        self.seed = int(seed)
        self.shape = str(shape)
        self.enable = bool(enable)
        self.length = int(length)
        self.period = int(period)
        self.rolloff = float(rolloff)
        self.symbolrate = float(symbolrate)
        self.interpolation = int(interpolation)

        #---------------------------------------------------------------------#
        # Variables                                                           #
        #---------------------------------------------------------------------#

        self.outputrate = outputrate = symbolrate*interpolation
        self.dutycycle = dutycycle = str("%s%%")%(float(length)/float(period)*100.0)

        #---------------------------------------------------------------------#
        # Blocks                                                              #
        #---------------------------------------------------------------------#
        
        self.block_phasor = blocks.multiply_const_vcc((amplitude*numpy.exp(1j*numpy.deg2rad(phase)), ))
        self.block_pattern = blocks.vector_source_c([0.0], True, 1, [])
        self.block_interpolator = filter.interp_fir_filter_ccf(interpolation, ([1.0]))
        self.block_interpolator.declare_sample_delay(0)
        self.block_doppler = blocks.rotator_cc(doppler)
        self.block_delay = blocks.delay(gr.sizeof_gr_complex*1, delay)

        #---------------------------------------------------------------------#
        # Affinities                                                          #
        #---------------------------------------------------------------------#

        (self.block_phasor).set_processor_affinity([0])
        (self.block_pattern).set_processor_affinity([0])
        (self.block_interpolator).set_processor_affinity([0])
        (self.block_doppler).set_processor_affinity([0])
        (self.block_delay).set_processor_affinity([0])
        
        #---------------------------------------------------------------------#
        # Connections                                                         #
        #---------------------------------------------------------------------#
        
        self.connect((self.block_pattern, 0), (self.block_delay, 0))
        self.connect((self.block_delay, 0), (self.block_phasor, 0))
        self.connect((self.block_phasor, 0), (self.block_interpolator, 0))
        self.connect((self.block_interpolator, 0), (self.block_doppler, 0))
        self.connect((self.block_doppler, 0), (self, 0))

    ###########################################################################
    # ATTRIBUTES                                                              #
    ###########################################################################

    def get_code(self):
        return self.code

    def get_enable(self):
        return self.enable

    def get_interpolation(self):
        return self.interpolation

    def get_length(self):
        return self.length

    def get_period(self):
        return self.period

    def get_rolloff(self):
        return self.rolloff

    def get_seed(self):
        return self.seed

    def get_shape(self):
        return self.shape

    def get_symbolrate(self):
        return self.symbolrate

    def get_outputrate(self):
        return self.outputrate

    def get_dutycycle(self):
        return self.dutycycle

    def get_cpu(self):
        return self.cpu

    def set_cpu(self, cpu):
        self.cpu = cpu

    #-------------------------------------------------------------------------#

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay
        self.block_delay.set_dly(self.delay)

    #-------------------------------------------------------------------------#

    def get_phase(self):
        return self.phase

    def set_phase(self, phase):
        self.phase = phase
        self.block_phasor.set_k((self.amplitude*numpy.exp(1j*numpy.deg2rad(self.phase)),))

    #-------------------------------------------------------------------------#

    def get_doppler(self):
        return self.doppler

    def set_doppler(self, doppler):
        self.doppler = doppler
        self.block_doppler.set_phase_inc(self.doppler)

    #-------------------------------------------------------------------------#

    def get_amplitude(self):
        return self.amplitude

    def set_amplitude(self, amplitude):
        self.amplitude = amplitude
        self.block_phasor.set_k((self.amplitude*numpy.exp(1j*numpy.deg2rad(self.phase)),))

    #-------------------------------------------------------------------------#
'''