###############################################################################
#                                                                             #
#   PROJECT     :   SanDRA                                                    #
#   FILE        :   PulseGenerator.py                                         #
#   AUTHOR      :   M.Eng. Nico Pfeffer                                       #
#   INSTITUTION :   Leibniz-Institute of Atmospheric Physics                  #
#   DEPARTMENT  :   Radar Remote Sensing                                      #
#   GROUP       :   Radio Science Group                                       #
#                                                                             #
###############################################################################

import os
import sys
import numpy
import scipy
import scipy.signal
#import gnuradio.uhd

from gnuradio import blocks
from gnuradio import filter
from gnuradio.filter import firdes

from gnuradio.uhd import usrp_sink

###############################################################################





#block_usrp = gnuradio.uhd.usrp_sink(
#    device_addr="addr=192.168.10.2",
#    stream_args=gnuradio.uhd.stream_args(
#        cpu_format='fc32',
#        otw_format='sc16',
#        channels=range(1),
#    )
#)
#
#block_usrp.length = 30
#
#print block_usrp.length


class TxUsrp(usrp_sink()):

    def __init__(self,):
        super().__init__()



#  configuration section
# initialisation section
#      operation section



sys.exit()
class UsrpN200(object):

    def __str__(self,): pass
    
    def __init__(self,):

        self.__address = "192.168.10.2"
        self.__time_source = 'none'
        self.__clock_source = 'internal'

    ###########################################################################
    # MOTHERBOARD                                                             #
    ###########################################################################

    def get_address(self,): pass
    def get_cpu_format(self,): pass
    def get_otw_format(self,): pass
    def get_start_time(self,): pass
    def get_time_source(self,): pass
    def get_clock_source(self,): pass

    def set_address(self,): pass
    def set_cpu_format(self,): pass
    def set_otw_format(self,): pass
    def set_start_time(self,): pass
    def set_time_source(self,): pass
    def set_clock_source(self,): pass

    ###########################################################################
    # TRANSMITTER                                                             #
    ###########################################################################
    
    def get_tx_channel(self,): pass
    def get_tx_sample_rate(self,): pass

    def set_tx_channel(self,): pass
    def set_tx_sample_rate(self,): pass

    ###########################################################################
    # RECEIVER                                                                #
    ###########################################################################

    def get_rx_channel(self,): pass
    def get_rx_sample_rate(self,): pass

    def set_rx_channel(self,): pass
    def set_rx_sample_rate(self,): pass





###############################################################################

class Transceiver(object):

    def __init__(self,): pass

###############################################################################
# WAVEFORM CLASSES                                                            #
###############################################################################

class Pulse(object): pass
class Custom(object): pass
class Barker(object): pass
class Pseudo(object): pass
class Continuous(object): pass
class Complementary(object): pass

###############################################################################
# TRANSMITTER CLASSES                                                         #
###############################################################################

class Transmitter(object): pass
class TransmitterConfig(object): pass
class TransmitterDevice(object): pass
class TransmitterMixer(object): pass
class TransmitterChannel(object): pass
class TransmitterPattern(object): pass
class TransmitterScope(object): pass

###############################################################################
# RECEIVER CLASSES                                                            #
###############################################################################

class Receiver(object): pass
class ReceiverConfig(object): pass
class ReceiverDevice(object): pass
class ReceiverMixer(object): pass
class ReceiverChannel(object): pass
class ReceiverScope(object): pass
class ReceiverArchive(object): pass



class Pattern(object):

    def __init__(self,):

        self.__waveforms = []







class SandraTransceiver(object):

    def __str__(self,): pass
    def __init__(self,): pass

    ###########################################################################
    # ATTRIBUTES GETTER                                                       #
    ###########################################################################

    def get_tx_time_delay(self,): pass
    def get_tx_time_vector(self,): pass
    def get_tx_time_period(self,): pass
    def get_tx_time_resolution(self,): pass

    def get_tx_range_delay(self,): pass
    def get_tx_range_vector(self,): pass
    def get_tx_range_maximum(self,): pass
    def get_tx_range_resolution(self,): pass

    def get_tx_symbol_rate(self,): pass
    def get_tx_symbol_taps(self,): pass
    def get_tx_symbol_delay(self,): pass
    def get_tx_symbol_shape(self,): pass
    def get_tx_symbol_rolloff(self,): pass
    def get_tx_symbol_interpolation(self,): pass

    def get_tx_signal_phase(self,): pass
    def get_tx_signal_phasor(self,): pass
    def get_tx_signal_doppler(self,): pass
    def get_tx_signal_amplitude(self,): pass
    def get_tx_signal_dutycycle(self,): pass

    def get_tx_code_seed(self,): pass
    def get_tx_code_type(self,): pass
    def get_tx_code_length(self,): pass
    def get_tx_code_period(self,): pass

    def get_tx_limit_amplitude(self,): pass
    def get_tx_limit_dutycycle(self,): pass
    def get_tx_limit_bandwidth(self,): pass

    def get_tx_device_address(self,): pass
    def get_tx_device_subdevspec(self,): pass

    def get_tx_antenna_x(self,ch,band): pass
    def get_tx_antenna_y(self,ch,band): pass



    def get_rx_range_delay(self,): pass
    def get_rx_range_resolution(self,): pass

    def get_rx_acquire_path(self,): pass
    def get_rx_acquire_rate(self,): pass
    def get_rx_acquire_size(self,): pass
    def get_rx_acquire_format(self,): pass

    def get_rx_archive_path(self,): pass
    def get_rx_archive_size(self,): pass


    def get_rx_antenna_x(self,ch,band): pass
    def get_rx_antenna_y(self,ch,band): pass
    def get_rx_antenna_z(self,ch,band): pass


    def verify(self,): pass



class PulseGenerator(gnuradio.gr.hier_block2):

    def __str__(self,):
        """
        """
        pass

    ###########################################################################
    # INITIALIZE                                                              #
    ###########################################################################

    def __init__(self,):
        """
        """
        gnuradio.gr.hier_block2.__init__(self,
            name="Pulse Generator",
            input_signature=gnuradio.gr.io_signature(
                min_streams=0,
                max_streams=0,
                sizeof_stream_item=0,
            ),
            output_signature=gnuradio.gr.io_signature(
                min_streams=1,
                max_streams=1,
                sizeof_stream_item=8,
            ),
        )

        #---------------------------------------------------------------------#
        # Blocks                                                              #
        #---------------------------------------------------------------------#
        
        self.block_pattern = blocks.vector_source_c([0.0], True, 1, [])
        self.block_phasor = blocks.multiply_const_vcc((amplitude*numpy.exp(1j*numpy.deg2rad(phase)), ))
        self.block_interpolator = filter.interp_fir_filter_ccf(interpolation, ([1.0]))
        self.block_interpolator.declare_sample_delay(0)
        self.block_doppler = blocks.rotator_cc(doppler)
        self.block_delay = blocks.delay(gr.sizeof_gr_complex*1, delay)

    ###########################################################################
    # ATTRIBUTES GETTER                                                       #
    ###########################################################################

    def get_tx_time_delay(self,): pass
    def get_tx_time_vector(self,): pass
    def get_tx_time_period(self,): pass
    def get_tx_time_resolution(self,): pass

    def get_tx_range_delay(self,): pass
    def get_tx_range_vector(self,): pass
    def get_tx_range_maximum(self,): pass
    def get_tx_range_resolution(self,): pass

    def get_tx_symbol_rate(self,): pass
    def get_tx_symbol_taps(self,): pass
    def get_tx_symbol_delay(self,): pass
    def get_tx_symbol_shape(self,): pass
    def get_tx_symbol_rolloff(self,): pass
    def get_tx_symbol_interpolation(self,): pass

    def get_tx_signal_phase(self,): pass
    def get_tx_signal_phasor(self,): pass
    def get_tx_signal_doppler(self,): pass
    def get_tx_signal_amplitude(self,): pass
    def get_tx_signal_dutycycle(self,): pass

    def get_tx_code_seed(self,): pass
    def get_tx_code_type(self,): pass
    def get_tx_code_length(self,): pass
    def get_tx_code_period(self,): pass

    def get_tx_limit_amplitude(self,): pass
    def get_tx_limit_dutycycle(self,): pass

    ###########################################################################
    # METHODS SET                                                             #
    ###########################################################################

    def set_delay(self,delay): pass
    def set_phase(self,phase): pass
    def set_interpolation(self,interpolation): pass

    ###########################################################################
    # METHODS UPDATE                                                          #
    ###########################################################################

    def update_cpu(self,cpu): pass
    def update_delay(self,delay): pass
    def update_phase(self,phase): pass
    def update_doppler(self,doppler): pass
    def update_amplitude(self,amplitude): pass








###############################################################################

'''
class SandraTxWaveform(gr.hier_block2):

    ###########################################################################
    # DESCRIPTORS                                                             #
    ###########################################################################

    __SEED_DTYPE = int
    __SEED_INTERVAL = '(-inf,+inf)'

    __CODE_DTYPE = str
    __CODE_OPTIONS = ['cw','barker','custom','pseudo','complementary']

    __SHAPE_DTYPE = str
    __SHAPE_OPTIONS = ['rrg','rrc','box']

    __LENGTH_DTYPE = int
    __LENGTH_INTERVAL = '[0,+inf)'
    __LENGTH_OPTIONS_BARKER = [2,3,4,5,7,11,13]
    __LENGTH_OPTIONS_COMPLEMENTARY = [2,4,8,16,32]

    __PERIOD_DTYPE = int
    __PERIOD_INTERVAL = '[0,+inf)'

    __ROLLOFF_DTYPE = float
    __ROLLOFF_INTERVAL = '[0.0,1.0]'
    
    __INTERPOLATION_DTYPE = int
    __INTERPOLATION_INTERVAL = '[1,+inf)'

    __SYMBOLRATE_DTYPE = float
    __SYMBOLRATE_INTERVAL = '[)'

    __AMPLITUDE_DTYPE = float
    __AMPLITUDE_INTERVAL = '[0.0,1.0]'

    __PHASE_DTYPE = float
    __PHASE_INTERVAL = '[-180.0,+180.0)'

    __DELAY_DTYPE = int
    __DELAY_INTERVAL = '[0,+inf)'

    __ENABLE_DTYPE = bool
    __ENABLE_OPTIONS = [True,False]

    ###########################################################################

    def __str__(self,):
        info  = ""
        return info

    ###########################################################################
    # INITIALIZE                                                              #
    ###########################################################################

    def __init__(self,
            code="pseudo",
            interpolation=1,
            length=1000,
            period=1000,
            rolloff=0.518,
            seed=0,
            shape="root-raised-gaussian",
            symbolrate=200000.0):
        
        gr.hier_block2.__init__(
            self, "SanDRA Tx Waveform",
            gr.io_signature(0, 0, 0),
            gr.io_signature(1, 1, gr.sizeof_gr_complex*1),
        )

        #---------------------------------------------------------------------#
        # Defaults                                                            #
        #---------------------------------------------------------------------#

        self.cpu = [0]
        self.delay = 0
        self.phase = 0.0
        self.enable = True
        self.doppler = 0.0
        self.amplitude = 0.0

        #---------------------------------------------------------------------#
        # Parameters                                                          #
        #---------------------------------------------------------------------#

        self.code = str(code)
        self.seed = int(seed)
        self.shape = str(shape)
        self.enable = bool(enable)
        self.length = int(length)
        self.period = int(period)
        self.rolloff = float(rolloff)
        self.symbolrate = float(symbolrate)
        self.interpolation = int(interpolation)

        #---------------------------------------------------------------------#
        # Variables                                                           #
        #---------------------------------------------------------------------#

        self.outputrate = outputrate = symbolrate*interpolation
        self.dutycycle = dutycycle = str("%s%%")%(float(length)/float(period)*100.0)

        #---------------------------------------------------------------------#
        # Blocks                                                              #
        #---------------------------------------------------------------------#
        
        self.block_phasor = blocks.multiply_const_vcc((amplitude*numpy.exp(1j*numpy.deg2rad(phase)), ))
        self.block_pattern = blocks.vector_source_c([0.0], True, 1, [])
        self.block_interpolator = filter.interp_fir_filter_ccf(interpolation, ([1.0]))
        self.block_interpolator.declare_sample_delay(0)
        self.block_doppler = blocks.rotator_cc(doppler)
        self.block_delay = blocks.delay(gr.sizeof_gr_complex*1, delay)

        #---------------------------------------------------------------------#
        # Affinities                                                          #
        #---------------------------------------------------------------------#

        (self.block_phasor).set_processor_affinity([0])
        (self.block_pattern).set_processor_affinity([0])
        (self.block_interpolator).set_processor_affinity([0])
        (self.block_doppler).set_processor_affinity([0])
        (self.block_delay).set_processor_affinity([0])
        
        #---------------------------------------------------------------------#
        # Connections                                                         #
        #---------------------------------------------------------------------#
        
        self.connect((self.block_pattern, 0), (self.block_delay, 0))
        self.connect((self.block_delay, 0), (self.block_phasor, 0))
        self.connect((self.block_phasor, 0), (self.block_interpolator, 0))
        self.connect((self.block_interpolator, 0), (self.block_doppler, 0))
        self.connect((self.block_doppler, 0), (self, 0))

    ###########################################################################
    # ATTRIBUTES                                                              #
    ###########################################################################

    def get_code(self):
        return self.code

    def get_enable(self):
        return self.enable

    def get_interpolation(self):
        return self.interpolation

    def get_length(self):
        return self.length

    def get_period(self):
        return self.period

    def get_rolloff(self):
        return self.rolloff

    def get_seed(self):
        return self.seed

    def get_shape(self):
        return self.shape

    def get_symbolrate(self):
        return self.symbolrate

    def get_outputrate(self):
        return self.outputrate

    def get_dutycycle(self):
        return self.dutycycle

    def get_cpu(self):
        return self.cpu

    def set_cpu(self, cpu):
        self.cpu = cpu

    #-------------------------------------------------------------------------#

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay
        self.block_delay.set_dly(self.delay)

    #-------------------------------------------------------------------------#

    def get_phase(self):
        return self.phase

    def set_phase(self, phase):
        self.phase = phase
        self.block_phasor.set_k((self.amplitude*numpy.exp(1j*numpy.deg2rad(self.phase)),))

    #-------------------------------------------------------------------------#

    def get_doppler(self):
        return self.doppler

    def set_doppler(self, doppler):
        self.doppler = doppler
        self.block_doppler.set_phase_inc(self.doppler)

    #-------------------------------------------------------------------------#

    def get_amplitude(self):
        return self.amplitude

    def set_amplitude(self, amplitude):
        self.amplitude = amplitude
        self.block_phasor.set_k((self.amplitude*numpy.exp(1j*numpy.deg2rad(self.phase)),))

    #-------------------------------------------------------------------------#
'''