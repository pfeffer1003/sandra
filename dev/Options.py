###############################################################################
#                                                                             #
#   Project     :   SanDRA                                                    #
#   File        :   Options.py                                                #
#   Author      :   M.Eng. Nico Pfeffer                                       #
#   Institution :   Leibniz-Institute of Atmospheric Physics                  #
#   Department  :   Radar Remote Sensing                                      #
#   Group       :   Radio Science Group                                       #
#                                                                             #
###############################################################################

import os
import sys

###############################################################################

class Quantity(object):

    def __init__(self,value,name,unit,options):
        
        """
        """
        
        self.set_value(value)
        self.set_name(name)
        self.set_unit(unit)

        self.__set_lower__(lower)
        self.__set_upper__(upper)
        self.__set_options__(options)
        #self.__set_interval__()

    def get_name(self,):
        """
        """
        return self.__name

    def set_name(self,name):
        """
        """
        self.__name = name

    name = property(get_name,set_name)

    def get_value(self,):
        """
        """
        return self.__value

    def set_value(self,value):
        """
        """
        self.__value = value

    value = property(get_value,set_value)

    def get_options(self,):
        """
        """
        return self.__options

    def __set_options__(self,options):
        """
        """
        self.__options = options

    options = property(get_options)





class Bool(Quantity): pass
class Long(Quantity): pass
class Float(Quantity): pass
class String(Quantity):

    def __init__(self,value,name,options):
        Quantity.__init__(self,value,name,options)


class Integer(Quantity): pass



class Waveform(object):

    def __init__(self,):
        self.__code__ = String(value='cw',name='Code Type',options=[])
        print help(self.__code__)
        self.__code__.__set_options__(['Hi','Hwllo'])


    def get_code(self,):
        """
        """
        return self.__code__

    code = property(get_code)




wf = Waveform()
print wf.code.value
wf.code.value = 'barker'
print wf.code.value



#class WaveformType(String): pass
#class WaveformSeed(Integer): pass
#class WaveformLength(Integer): pass
#class WaveformPeriod(Integer): pass
#class WaveformDutyCycle(Float): pass

#class SignalPhase(Float): pass
#class SignalDelay(Integer): pass
#class SignalShape(String): pass
#class SignalDoppler(Float): pass
#class SignalRollOff(Float): pass
#class SignalAmplitude(Float): pass
#class SignalSymbolRate(Float): pass
#class SignalSampleRate(Float): pass
#class SignalInterpolation(Integer): pass

class StreamCore(Integer): pass
class StreamPort(Integer): pass
class StreamHost(String): pass
