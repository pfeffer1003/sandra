#!/usr/bin/env python


import os
import sys
import time
import tsip
import serial
import calendar
import SimpleXMLRPCServer

from serial import Serial
from serial import SerialException
from serial.tools import list_ports

from datetime import datetime

from func_timeout import func_timeout
from func_timeout import FunctionTimedOut







#class TrimbleThunderboltClient(object):
#
#    def __init__(self):
#        pass

class TrimbleThunderboltServer(object):

    def __init__(self,
            vendor_id,
            product_id,
            baudrate,
        ):
        
        self._vendor_id = int(vendor_id,16)
        self._product_id = int(product_id,16)
        self._baudrate = baudrate

        self.__connect()

        self._timestamp = None
        self._datum = None
        self._holdover_duration = None
        self._pps_offset = None
        self._pps_error = None
        self._clock_offset = None

        self._minor_alarms = None
        self._receiver_mode = None
        self._disciplining_mode = None
        self._disciplining_activity = None
        self._survey_progress = None
        self._minor_alarms = None
        self._decoding_status = None

        self._temperature = None
        self._latitude = None
        self._longitude = None
        self._altitude = None


    def get_port(self): return self._port
    def get_baudrate(self): return self._baudrate
    def get_vendor_id(self): return self._vendor_id
    def get_product_id(self): return self._product_id
    
    def get_datum(self): return self._datum
    def get_altitude(self): return self._altitude
    def get_latitude(self): return self._latitude
    def get_longitude(self): return self._longitude
    def get_timestamp(self): return self._timestamp
    def get_pps_error(self): return self._pps_error
    def get_pps_offset(self): return self._pps_offset
    def get_temperature(self): return self._temperature
    def get_clock_offset(self): return self._clock_offset
    def get_minor_alarms(self): return self._minor_alarms
    def get_receiver_mode(self): return self._receiver_mode
    def get_decoding_status(self): return self._decoding_status
    def get_survey_progress(self): return self._survey_progress
    def get_holdover_duration(self): return self._holdover_duration
    def get_disciplining_mode(self): return self._disciplining_mode
    def get_disciplining_activity(self): return self._disciplining_acitivity
    
    def __set__port(self):

        devs = list_ports.comports()
        if devs !=[]:
            for dev in devs:
                if ((dev.vid!=None)or
                    (dev.pid!=None)):
                    if ((dev.vid==self._vendor_id)and
                        (dev.pid==self._product_id)):
                        self._port = dev.device
                        break
                    self._port = None
        else:
            self._port = None

    def __set__interface(self):

        if not self._port is None:
            self._interface = Serial(self._port,self._baudrate)

    def __set__tsip(self):

        if not self._interface is None:
            self._device = tsip.GPS(self._interface)

    def __set__timestamp(self): pass
    def __set__datum(self): pass

    def __set__receiver_mode(self,value):

        if value==0: tmp_var = "Automatic (2D/3D)"
        if value==1: tmp_var = "Single Satellite (Time)"
        if value==3: tmp_var = "Horizontal (2D)"
        if value==4: tmp_var = "Full Position (3D)"
        if value==7: tmp_var = "Over-Determined Clock"
        self._receiver_mode = tmp_var

    def __set__disciplining_mode(self,value):

        if value==0: tmp_var = "Normal (Locked to GPS)"
        if value==1: tmp_var = "Power Up"
        if value==2: tmp_var = "Auto Holdover"
        if value==3: tmp_var = "Manual Holdover"
        if value==4: tmp_var = "Recovery"
        if value==5: tmp_var = "Not used"
        if value==6: tmp_var = "Disciplining disabled"
        self._disciplining_mode = tmp_var

    def __set__disciplining_activity(self,value):

        if value==0: tmp_var = "Phase locking"
        if value==1: tmp_var = "Oscillator warm-up"
        if value==2: tmp_var = "Frequency locking"
        if value==3: tmp_var = "Placing PPS"
        if value==4: tmp_var = "Initializing loop filter"
        if value==5: tmp_var = "Compensating OCXO (holdover)"
        if value==6: tmp_var = "Inactive"
        if value==7: tmp_var = "Not used"
        if value==8: tmp_var = "Recovery mode"
        if value==9: tmp_var = "Calibration/Control voltage"
        self._disciplining_acitivity = tmp_var

    def __set__decoding_status(self,value):

        if "0x%02X"%value=="0x00": tmp_var = "Doing fixes"
        if "0x%02X"%value=="0x01": tmp_var = "Don't have GPS time"
        if "0x%02X"%value=="0x03": tmp_var = "PDOP too high"
        if "0x%02X"%value=="0x08": tmp_var = "0 usable satellites"
        if "0x%02X"%value=="0x09": tmp_var = "1 usable satellites"
        if "0x%02X"%value=="0x0A": tmp_var = "2 usable satellites"
        if "0x%02X"%value=="0x0B": tmp_var = "3 usable satellites"
        if "0x%02X"%value=="0x0C": tmp_var = "Unusable satellite"
        if "0x%02X"%value=="0x10": tmp_var = "TRAIM rejected the fix"
        self._decoding_status = tmp_var

    def __set__minor_alarms(self,value):

        flags = "{0:{fill}13b}"
        flags = flags.format(value,fill='0')
        flags = flags[::-1]
        tmp_list = []
        if flags[0] =="1": tmp_list.append("DAC near rail")
        if flags[1] =="1": tmp_list.append("Antenna Open")
        if flags[2] =="1": tmp_list.append("Antenna Shorted")
        if flags[3] =="1": tmp_list.append("Not tracking satellites")
        if flags[4] =="1": tmp_list.append("Not disciplining oscillator")
        if flags[5] =="1": tmp_list.append("Survey in Progress")
        if flags[6] =="1": tmp_list.append("No stored position")
        if flags[7] =="1": tmp_list.append("Leap second pending")
        if flags[8] =="1": tmp_list.append("In test mode")
        if flags[9] =="1": tmp_list.append("Position is questionable")
        if flags[10]=="1": tmp_list.append("Not used")
        if flags[11]=="1": tmp_list.append("Almanac not complete")
        if flags[12]=="1": tmp_list.append("PPS not generated")
        self._minor_alarms = tmp_list


    def __set__altitude(self,value): self._altitude = self.__check__position(value)       
    def __set__latitude(self,value): self._latitude = self.__check__position(value)
    def __set__longitude(self,value): self._longitude = self.__check__position(value)
    
    def __set__pps_error(self,value): self._pps_error = value
    def __set__pps_offset(self,value): self._pps_offset = value
    def __set__temperature(self,value): self._temperature = value
    def __set__clock_offset(self,value): self._clock_offset = value
    def __set__survey_progress(self,value): self._survey_progress = value
    def __set__holdover_duration(self,value): self._holdover_duration = value

    # ======================================================================= #

    def __connect(self):

        while True:
            try:
                self.__set__port()
                self.__set__interface()
                self.__set__tsip()
                print "ConnectionSuccess"
                break
            except AttributeError as e:
                print "ConnectionError:",e
                time.sleep(1.0)
                continue

    def __disconnect(self):

        self._interface.close()
        del self._interface
        del self._device

    def __check__position(self,value):

        if "No stored position" in self._minor_alarms:
            return None
        else:        
            return value 

    # ======================================================================= #

    def start(self):

        while True:
            try:
                report = func_timeout(2.0,self._device.read,())
                if ((report[0]==0x8F)and 
                    (report[1]==0xAB)):
                    time_flags = "{0:{fill}5b}"
                    time_flags = time_flags.format(report[5],fill='0')
                    time_flags = time_flags[::-1]
                    if time_flags[3]=="0":
                        self._datum = str(
                            "%4d-%02d-%02d %02d:%02d:%02d"%(
                                report[11],
                                report[10],
                                report[9],
                                report[8],
                                report[7],
                                report[6],
                            )
                        )
                        self._timestamp = datetime.strptime(self._datum,"%Y-%m-%d %H:%M:%S")
                        self._timestamp = self._timestamp.utctimetuple()
                        self._timestamp = calendar.timegm(self._timestamp)
                    else:
                        self._timestamp = None
                        self._datum = None
                elif report[0] == 0x8F and report[1] == 0xAC:
                    self.__set__receiver_mode(report[2])
                    self.__set__disciplining_mode(report[3])
                    self.__set__disciplining_activity(report[9])
                    self.__set__survey_progress(report[4])
                    self.__set__minor_alarms(report[7])
                    self.__set__decoding_status(report[8])
                    self.__set__holdover_duration(report[5])
                    self.__set__pps_error(report[20])
                    self.__set__pps_offset(report[12])
                    self.__set__clock_offset(report[13])
                    self.__set__temperature(report[16])
                    self.__set__latitude(report[17])
                    self.__set__longitude(report[18])
                    self.__set__altitude(report[19])
                print "Survey Progress:\t\t",self._survey_progress
                print "Receivermode:\t\t\t",self._receiver_mode
                print "DiscipliningMode:\t\t",self._disciplining_mode
                print "DiscipliningActivity:\t",self._disciplining_acitivity
                print "DecodingStatus:\t\t\t",self._decoding_status
                print "MinorAlarms:\t\t\t",self._minor_alarms
                print "TimeDate:\t\t\t\t",self._datum
                print "Latitude:\t\t\t\t",self._latitude
                print "Longitude:\t\t\t\t",self._longitude
                print "Altitude:\t\t\t\t",self._altitude
                print ""
            except FunctionTimedOut:
                print "FunctionTimeOut"
                continue
            except serial.serialutil.SerialException as e:
                print "SerialException:",e
                self.__disconnect()
                self.__connect()
                continue
            except Exception as e:
                print e
                continue

    def stop(self): pass


    def reset(self):

        interface = Serial(self._port,self._baudrate)
        device = tsip.GPS(interface)
        device.write(tsip.Packet(0x1e,0x46))
        time.sleep(10.0)


if __name__ == '__main__':

    gps_server = TrimbleThunderboltServer("0403","6001",9600)
    gps_server.start()

    #gps_server.reset()

    sys.exit()