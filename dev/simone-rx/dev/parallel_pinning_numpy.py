#!/usr/bin/env python



import os

activate_this = "%s/.simone-rx/bin/activate_this.py"%os.getenv('HOME')
execfile(activate_this, dict(__file__=activate_this))

import sys
import time
import numpy
import psutil
import multiprocessing as mp
from multiprocessing import Process,Queue,Manager



N_CPUS = mp.cpu_count()



f_sampling = 100000

n_tx = 5
n_rx = 11

n_seconds = 60
n_ranges_raw = 1000
n_ranges_est = 350

n_channels = n_tx*n_rx

n_profiles  = n_seconds
n_profiles *= f_sampling
n_profiles /= n_ranges_raw



a_cores = numpy.asarray([1,2,3,4,5,6])
n_cores = len(a_cores)

print n_rx
print n_cores

if self.__n_rx < self.__n_cores:
    a_index = numpy.arange()
else:

sys.exit()




class Decoder(object):

    def __init__(self,
            n_tx,
            n_rx,
            n_profiles,
            n_ranges_raw,
            n_ranges_est,
            a_cpus,
        ):

        self.__n_tx = n_tx
        self.__n_rx = n_rx
        self.__n_profiles = n_profiles
        self.__n_ranges_raw = n_ranges_raw
        self.__n_ranges_est = n_ranges_est
        self.__n_channels = self.__n_tx*self.__n_rx

        self.__a_cpus = a_cpus

        self.__n_cores = len(self.__a_cpus)
        self.__n_slice = self.__n_rx/self.__n_cores



        sys.exit()

        self.__set__array_decode()

    def __set__array_decode(self):

        self.__array_decode = numpy.zeros((self.__n_tx,self.__n_ranges_raw,self.__n_ranges_est),dtype=numpy.complex64)
        self.__array_decode.real = numpy.random.normal(0.0,1.0,(self.__n_tx,self.__n_ranges_raw,self.__n_ranges_est))
        self.__array_decode.imag = numpy.random.normal(0.0,1.0,(self.__n_tx,self.__n_ranges_raw,self.__n_ranges_est))

    def __get__array_decode(self): return self.__array_decode

    array_decode = property(__get__array_decode)


    def get_array_raw_random(self):

        a      = numpy.zeros((self.__n_rx,self.__n_profiles,self.__n_ranges_raw),dtype=numpy.complex64)
        a.real = numpy.random.normal(0.0,1.0,(self.__n_rx,self.__n_profiles,self.__n_ranges_raw))
        a.imag = numpy.random.normal(0.0,1.0,(self.__n_rx,self.__n_profiles,self.__n_ranges_raw))

        return a


    def compute(self,a_raw):

        proc_manager = Manager()
        proc_dict = proc_manager.dict()
        print self.__a_cpus # the affinities
        print self.__n_rx # 
        print self.__n_cores #
        print self.__n_rx/self.__n_cores



        '''
        proc_0 = Process(target=self.run,args=(a_raw,self.__array_decode,1,proc_dict,0))
        #proc_1 = Process(target=self.run,args=(a_raw[2:4,:,:],self.__array_decode,2,proc_dict,1))
        
        proc_0.start()
        #proc_1.start()
        
        proc_0.join()
        #proc_1.join()

        print proc_dict[0].shape
        '''


    def run(self,a_raw,a_est,core,proc_dict,proc_key):

        pinning = psutil.Process()
        pinning.cpu_affinity([core])
        
        a_raw = numpy.expand_dims(a_raw,2)        
        a_dec = numpy.einsum("mopi,niq->mnopq",a_raw,a_est,optimize='optimal')
        a_dec = numpy.squeeze(a_dec,3)

        proc_dict[proc_key] = a_dec



if __name__ == "__main__":

    decoder = Decoder(n_tx,n_rx,n_profiles,n_ranges_raw,n_ranges_est,a_cpus)
    decoder.compute(decoder.get_array_raw_random())

    sys.exit()