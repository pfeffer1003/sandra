#!/usr/bin/env python



import os

activate_this = "%s/.simone-rx/bin/activate_this.py"%os.getenv('HOME')
execfile(activate_this, dict(__file__=activate_this))

import sys
import time
import terminaltables

from datetime import datetime
from colorclass import Color
from collections import OrderedDict
from terminaltables import DoubleTable




class LoggingStats(object):


    def __init__(self,log_dir):

        self.__set__log_dir(log_dir)
        self.__set__log_files(self.__log_dir)
        self.__set__log_dates(self.__log_files)
        self.__set__log_paths()
        self.__set__log_stats()

    def __str__(self):

        table_data = []
        table_data.append(
            [
                '',
                "\033[1m%s\033[0m"%'START',
                "\033[1m%s\033[0m"%'RUN',
                "\033[1m%s\033[0m"%'STOP',
                "\033[1m%s\033[0m"%'FAIL',
            ],
        )
        for stat in self.__stats:
            table_row = []
            table_row.append(Color("\033[1m%s\033[0m" % stat[0]))
            table_row.append(stat[1]['Start'])
            table_row.append(stat[1]['Run'])
            table_row.append(stat[1]['Stop'])
            table_row.append(stat[1]['Fail'])
            table_data.append(table_row)
        table = DoubleTable(table_data)
        table.inner_heading_row_border = True
        #table.inner_row_border = True
        table.justify_columns[0] = 'center'
        table.justify_columns[1] = 'center'
        table.justify_columns[2] = 'center'
        table.justify_columns[3] = 'center'
        table.justify_columns[4] = 'center'
        print table.table
        return ""

    def __set__log_dir(self,log_dir):
        
        self.__log_dir = log_dir


    def __set__log_files(self,log_dir):
        
        log_files = os.walk(log_dir+"/.")
        log_files = next(log_files)[2]
        log_files = sorted(log_files)
        log_files = [s for s in log_files if "main.log" in s]
        log_files.append(log_files.pop(0))
        self.__log_files = log_files


    def __set__log_dates(self,log_files):

        cur_date = os.stat(self.__log_dir+'/main.log')
        cur_date = cur_date.st_ctime
        cur_date = str(datetime.fromtimestamp(cur_date))[0:10]

        log_dates = [s.replace('main.log','') for s in log_files]
        log_dates = [s.replace('.','') for s in log_dates]
        log_dates = list(filter(None,log_dates))
        log_dates.append(cur_date)
        self.__log_dates = log_dates

    def __set__log_paths(self):
        log_paths = [self.__log_dir+'/'+s for s in self.__log_files]
        self.__log_paths = log_paths

    def __set__log_stats(self):

        stats = []
        cmp_run = 'RECEIVER RUNNING!'
        cmp_fail = 'RECEIVER FAILED!'
        cmp_stop = 'RECEIVER STOPPED!'
        cmp_start = 'RECEIVER STARTED!'
        for i,log_path in enumerate(self.__log_paths):
            stats.append([self.__log_dates[i]])
            cnt_run = 0
            cnt_fail = 0
            cnt_stop = 0
            cnt_start = 0
            with open(log_path,'r') as file:
                for line in file:
                    if cmp_run in line: cnt_run += 1
                    if cmp_fail in line: cnt_fail += 1                    
                    if cmp_stop in line: cnt_stop += 1            
                    if cmp_start in line: cnt_start += 1
            stats[-1].append(
                {
                    'Run': cnt_run,
                    'Fail': cnt_fail,
                    'Stop': cnt_stop,
                    'Start': cnt_start,                    
                }
            )
        self.__stats = stats



if __name__ == "__main__":


    log_error = LoggingStats("/home/radar/.simone-rx/log")
    print log_error

    sys.exit()