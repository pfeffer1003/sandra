#!/usr/bin/env python



import os
import sys
import time
import xmlrpclib


if __name__ == "__main__":

	rpc_host = 'localhost'
	rpc_port = 6999
	rpc_addr = (rpc_host,rpc_port)
	rpc_client = xmlrpclib.Server('http://%s:%s'%rpc_addr)

	while True:
		print rpc_client.get_latitude()
		print rpc_client.get_longitude()
		print rpc_client.get_altitude()
		print rpc_client.get_timestamp()
		time.sleep(1.0)

	sys.exit()