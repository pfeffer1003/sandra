SYSTEM DETAILS:
---------------

	-	10 channel receiver (2x 5 antennas)
	-	with the MIMO approach => 50 channels in total


PARTS NEEDED:
-------------

	-	1x 	Schroff COMPTEC Rack
	-	 
	-	5x 	USRP N200
	-	5x	Basic-RX Daughterboard
	-	5x 	Basic-TX Daughterboard
	-	1x	Octoclock
	-	1x	8-port 1Gb Ethernet Switch
	-	1x 	Trimble Thunderbolt E GPS Receiver 
	-	1x 	EATON UPS (using with "nut")
	-	1x	2HE Rackserver
				-	32GB RAM
				-	2x 1Gb Ethernet interfaces
				-	4x USB Interface
				-	Quadcore CPU (Intel)
				-	HDD storage up to 4TB (at least)
	-	10x	Mini-Circuits Amplifier
	-	10x	Mini-Circuits Amplifier
	-	10x	Mini-Circuits Highpass
	-	10x	Mini-Circuits Lowpass
	-	10x	Mini-Circuits Bandstop


MODULAR RACKUNITS:
------------------

	-	1HE to 2HE Rackserver
	-	1HE UPS unit
	