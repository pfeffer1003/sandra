#!/usr/bin/env python

"""
Usage:
    main-scope <channel>
"""

import os

activate_this = "%s/.simone-rx/bin/activate_this.py"%os.getenv('HOME')
execfile(activate_this, dict(__file__=activate_this))

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt5 import Qt
from PyQt5 import Qt, QtCore
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import zeromq
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
from docopt import docopt
import numpy
import sip
import sys
import simone_rx
from gnuradio import qtgui



args = docopt(__doc__,options_first=True)



class main_scope(gr.top_block, Qt.QWidget):

    def __init__(self,channel=0):
        gr.top_block.__init__(self, "Main Scope")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Main Scope")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "main_scope")

        if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
            self.restoreGeometry(self.settings.value("geometry").toByteArray())
        else:
            self.restoreGeometry(self.settings.value("geometry", type=QtCore.QByteArray))

        self._cfg = simone_rx.Config()
        self._cfg.load("%s/.simone-rx/cfg/main.cfg" % os.getenv("HOME"))

        ##################################################
        # Parameters
        ##################################################
        self.channel = channel

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = self._cfg.get_acq_rate()
        self.gui_bit_range = gui_bit_range = 3

        ##################################################
        # Blocks
        ##################################################
        self.gui_tab = Qt.QTabWidget()
        self.gui_tab_widget_0 = Qt.QWidget()
        self.gui_tab_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.gui_tab_widget_0)
        self.gui_tab_grid_layout_0 = Qt.QGridLayout()
        self.gui_tab_layout_0.addLayout(self.gui_tab_grid_layout_0)
        self.gui_tab.addTab(self.gui_tab_widget_0, 'Channel Comparison')
        self.gui_tab_widget_1 = Qt.QWidget()
        self.gui_tab_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.gui_tab_widget_1)
        self.gui_tab_grid_layout_1 = Qt.QGridLayout()
        self.gui_tab_layout_1.addLayout(self.gui_tab_grid_layout_1)
        self.gui_tab.addTab(self.gui_tab_widget_1, 'Channel Constellation')
        self.top_layout.addWidget(self.gui_tab)
        self._gui_bit_range_range = Range(1, 15, 1, 6, 200)
        self._gui_bit_range_win = RangeWidget(self._gui_bit_range_range, self.set_gui_bit_range, 'Set Scale', "counter_slider", float)
        self.top_layout.addWidget(self._gui_bit_range_win)
        self.zeromq_sub_source_0_0 = zeromq.sub_source(gr.sizeof_gr_complex, 1, 'tcp://localhost:%' % self._cfg.get_stream_port(self.channel), 100, True, -1)
        self.zeromq_sub_source_0 = zeromq.sub_source(gr.sizeof_gr_complex, 1, 'tcp://localhost:%' % self._cfg.get_stream_port(0), 100, True, -1)
        self.qtgui_vector_sink_f_0_0 = qtgui.vector_sink_f(
            2048,
            -samp_rate/2.0,
            +samp_rate/2048.0,
            "Frequency / Hz",
            "Dynamic Range / Bits",
            "Channel %s" % self.channel,
            2 # Number of inputs
        )
        self.qtgui_vector_sink_f_0_0.set_update_time(1.0)
        self.qtgui_vector_sink_f_0_0.set_y_axis(-5, 15)
        self.qtgui_vector_sink_f_0_0.enable_autoscale(False)
        self.qtgui_vector_sink_f_0_0.enable_grid(True)
        self.qtgui_vector_sink_f_0_0.set_x_axis_units("")
        self.qtgui_vector_sink_f_0_0.set_y_axis_units("")
        self.qtgui_vector_sink_f_0_0.set_ref_level(0)

        labels = ['Signal', 'Noise', '', '', '',
                  '', '', '', '', '']
        widths = [2, 2, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["dark blue", "black", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_vector_sink_f_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_vector_sink_f_0_0.set_line_label(i, labels[i])
            self.qtgui_vector_sink_f_0_0.set_line_width(i, widths[i])
            self.qtgui_vector_sink_f_0_0.set_line_color(i, colors[i])
            self.qtgui_vector_sink_f_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_vector_sink_f_0_0_win = sip.wrapinstance(self.qtgui_vector_sink_f_0_0.pyqwidget(), Qt.QWidget)
        self.gui_tab_grid_layout_0.addWidget(self._qtgui_vector_sink_f_0_0_win, 7, 6, 8, 3)
        [self.gui_tab_grid_layout_0.setRowStretch(r,1) for r in range(7,15)]
        [self.gui_tab_grid_layout_0.setColumnStretch(c,1) for c in range(6,9)]
        self.qtgui_vector_sink_f_0 = qtgui.vector_sink_f(
            2048,
            -samp_rate/2.0,
            +samp_rate/2048.0,
            "Frequency / Hz",
            "Dynamic Range / Bits",
            "Channel 0",
            2 # Number of inputs
        )
        self.qtgui_vector_sink_f_0.set_update_time(1.0)
        self.qtgui_vector_sink_f_0.set_y_axis(-5, 15)
        self.qtgui_vector_sink_f_0.enable_autoscale(False)
        self.qtgui_vector_sink_f_0.enable_grid(True)
        self.qtgui_vector_sink_f_0.set_x_axis_units("")
        self.qtgui_vector_sink_f_0.set_y_axis_units("")
        self.qtgui_vector_sink_f_0.set_ref_level(0)

        labels = ['Noise', 'Signal', '', '', '',
                  '', '', '', '', '']
        widths = [2, 2, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["black", "dark blue", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_vector_sink_f_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_vector_sink_f_0.set_line_label(i, labels[i])
            self.qtgui_vector_sink_f_0.set_line_width(i, widths[i])
            self.qtgui_vector_sink_f_0.set_line_color(i, colors[i])
            self.qtgui_vector_sink_f_0.set_line_alpha(i, alphas[i])

        self._qtgui_vector_sink_f_0_win = sip.wrapinstance(self.qtgui_vector_sink_f_0.pyqwidget(), Qt.QWidget)
        self.gui_tab_grid_layout_0.addWidget(self._qtgui_vector_sink_f_0_win, 1, 6, 6, 3)
        [self.gui_tab_grid_layout_0.setRowStretch(r,1) for r in range(1,7)]
        [self.gui_tab_grid_layout_0.setColumnStretch(c,1) for c in range(6,9)]
        self.qtgui_time_sink_x_0_0_0 = qtgui.time_sink_f(
        	1000, #size
        	samp_rate, #samp_rate
        	"Channel %s" % self.channel, #name
        	2 #number of inputs
        )
        self.qtgui_time_sink_x_0_0_0.set_update_time(1)
        self.qtgui_time_sink_x_0_0_0.set_y_axis(-2**gui_bit_range, +2**gui_bit_range)

        self.qtgui_time_sink_x_0_0_0.set_y_label('Amplitude', 'a.u.')

        self.qtgui_time_sink_x_0_0_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_0_0.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "Trigger")
        self.qtgui_time_sink_x_0_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0_0.enable_grid(True)
        self.qtgui_time_sink_x_0_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_0_0.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_0_0.disable_legend()

        labels = ['I', 'Q', 'ch001 : I', 'ch001 : Q', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["black", "Dark Blue", "black", "dark red", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 0.2, 0.2, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0_0.pyqwidget(), Qt.QWidget)
        self.gui_tab_grid_layout_0.addWidget(self._qtgui_time_sink_x_0_0_0_win, 7, 0, 8, 3)
        [self.gui_tab_grid_layout_0.setRowStretch(r,1) for r in range(7,15)]
        [self.gui_tab_grid_layout_0.setColumnStretch(c,1) for c in range(0,3)]
        self.qtgui_time_sink_x_0_0 = qtgui.time_sink_f(
        	1000, #size
        	samp_rate, #samp_rate
        	"Channel 0", #name
        	2 #number of inputs
        )
        self.qtgui_time_sink_x_0_0.set_update_time(1)
        self.qtgui_time_sink_x_0_0.set_y_axis(-2**gui_bit_range, +2**gui_bit_range)

        self.qtgui_time_sink_x_0_0.set_y_label('Amplitude', "a.u.")

        self.qtgui_time_sink_x_0_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "Trigger")
        self.qtgui_time_sink_x_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0.enable_grid(True)
        self.qtgui_time_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_0.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_0.disable_legend()

        labels = ['I', 'Q', 'ch001 : I', 'ch001 : Q', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["black", "Dark Blue", "black", "dark red", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 0.2, 0.2, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.gui_tab_grid_layout_0.addWidget(self._qtgui_time_sink_x_0_0_win, 1, 0, 6, 3)
        [self.gui_tab_grid_layout_0.setRowStretch(r,1) for r in range(1,7)]
        [self.gui_tab_grid_layout_0.setColumnStretch(c,1) for c in range(0,3)]
        self.qtgui_number_sink_0_0 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_VERT,
            1
        )
        self.qtgui_number_sink_0_0.set_update_time(1.0)
        self.qtgui_number_sink_0_0.set_title('Phase Difference')

        labels = ['Channel 0 %s' % self.channel, '', '', '', '',
                  '', '', '', '', '']
        units = ['deg', '', '', '', '',
                 '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(1):
            self.qtgui_number_sink_0_0.set_min(i, -180.0)
            self.qtgui_number_sink_0_0.set_max(i, +180.0)
            self.qtgui_number_sink_0_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0_0.set_label(i, labels[i])
            self.qtgui_number_sink_0_0.set_unit(i, units[i])
            self.qtgui_number_sink_0_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0_0.enable_autoscale(False)
        self._qtgui_number_sink_0_0_win = sip.wrapinstance(self.qtgui_number_sink_0_0.pyqwidget(), Qt.QWidget)
        self.gui_tab_grid_layout_0.addWidget(self._qtgui_number_sink_0_0_win, 7, 9, 8, 1)
        [self.gui_tab_grid_layout_0.setRowStretch(r,1) for r in range(7,15)]
        [self.gui_tab_grid_layout_0.setColumnStretch(c,1) for c in range(9,10)]
        self.qtgui_histogram_sink_x_0_0_0_0 = qtgui.histogram_sink_f(
        	samp_rate,
        	100,
                -2**gui_bit_range,
                +2**gui_bit_range,
        	'Channel %s' % self.channel,
        	2
        )

        self.qtgui_histogram_sink_x_0_0_0_0.set_update_time(1.0)
        self.qtgui_histogram_sink_x_0_0_0_0.enable_autoscale(True)
        self.qtgui_histogram_sink_x_0_0_0_0.enable_accumulate(False)
        self.qtgui_histogram_sink_x_0_0_0_0.enable_grid(True)
        self.qtgui_histogram_sink_x_0_0_0_0.enable_axis_labels(True)

        if not True:
          self.qtgui_histogram_sink_x_0_0_0_0.disable_legend()

        labels = ["I", "Q", '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["black", "dark blue", "black", "dark red", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 0.2, 0.2, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_histogram_sink_x_0_0_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_histogram_sink_x_0_0_0_0.set_line_label(i, labels[i])
            self.qtgui_histogram_sink_x_0_0_0_0.set_line_width(i, widths[i])
            self.qtgui_histogram_sink_x_0_0_0_0.set_line_color(i, colors[i])
            self.qtgui_histogram_sink_x_0_0_0_0.set_line_style(i, styles[i])
            self.qtgui_histogram_sink_x_0_0_0_0.set_line_marker(i, markers[i])
            self.qtgui_histogram_sink_x_0_0_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_histogram_sink_x_0_0_0_0_win = sip.wrapinstance(self.qtgui_histogram_sink_x_0_0_0_0.pyqwidget(), Qt.QWidget)
        self.gui_tab_grid_layout_0.addWidget(self._qtgui_histogram_sink_x_0_0_0_0_win, 7, 3, 8, 3)
        [self.gui_tab_grid_layout_0.setRowStretch(r,1) for r in range(7,15)]
        [self.gui_tab_grid_layout_0.setColumnStretch(c,1) for c in range(3,6)]
        self.qtgui_histogram_sink_x_0_0_0 = qtgui.histogram_sink_f(
        	samp_rate,
        	100,
                -2**gui_bit_range,
                +2**gui_bit_range,
        	'Channel 0',
        	2
        )

        self.qtgui_histogram_sink_x_0_0_0.set_update_time(1.0)
        self.qtgui_histogram_sink_x_0_0_0.enable_autoscale(True)
        self.qtgui_histogram_sink_x_0_0_0.enable_accumulate(False)
        self.qtgui_histogram_sink_x_0_0_0.enable_grid(True)
        self.qtgui_histogram_sink_x_0_0_0.enable_axis_labels(True)

        if not True:
          self.qtgui_histogram_sink_x_0_0_0.disable_legend()

        labels = ["I", "Q", '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["black", "dark blue", "black", "dark red", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 0.2, 0.2, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_histogram_sink_x_0_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_histogram_sink_x_0_0_0.set_line_label(i, labels[i])
            self.qtgui_histogram_sink_x_0_0_0.set_line_width(i, widths[i])
            self.qtgui_histogram_sink_x_0_0_0.set_line_color(i, colors[i])
            self.qtgui_histogram_sink_x_0_0_0.set_line_style(i, styles[i])
            self.qtgui_histogram_sink_x_0_0_0.set_line_marker(i, markers[i])
            self.qtgui_histogram_sink_x_0_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_histogram_sink_x_0_0_0_win = sip.wrapinstance(self.qtgui_histogram_sink_x_0_0_0.pyqwidget(), Qt.QWidget)
        self.gui_tab_grid_layout_0.addWidget(self._qtgui_histogram_sink_x_0_0_0_win, 1, 3, 6, 3)
        [self.gui_tab_grid_layout_0.setRowStretch(r,1) for r in range(1,7)]
        [self.gui_tab_grid_layout_0.setColumnStretch(c,1) for c in range(3,6)]
        self.qtgui_const_sink_x_0 = qtgui.const_sink_c(
        	1, #size
        	"", #name
        	2 #number of inputs
        )
        self.qtgui_const_sink_x_0.set_update_time(1.0)
        self.qtgui_const_sink_x_0.set_y_axis(-2**gui_bit_range, +2**gui_bit_range)
        self.qtgui_const_sink_x_0.set_x_axis(-2**gui_bit_range, +2**gui_bit_range)
        self.qtgui_const_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, "")
        self.qtgui_const_sink_x_0.enable_autoscale(False)
        self.qtgui_const_sink_x_0.enable_grid(True)
        self.qtgui_const_sink_x_0.enable_axis_labels(True)

        if not True:
          self.qtgui_const_sink_x_0.disable_legend()

        labels = ['Channel 0 : I+jQ', 'Channel %s : I+jQ' % self.channel, '', '', '',
                  '', '', '', '', '']
        widths = [30, 30, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["black", "Dark Blue", "red", "red", "red",
                  "red", "red", "red", "red", "red"]
        styles = [1, 1, 0, 0, 0,
                  0, 0, 0, 0, 0]
        markers = [0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(2):
            if len(labels[i]) == 0:
                self.qtgui_const_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_const_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_const_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_const_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_const_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_const_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_const_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_const_sink_x_0_win = sip.wrapinstance(self.qtgui_const_sink_x_0.pyqwidget(), Qt.QWidget)
        self.gui_tab_layout_1.addWidget(self._qtgui_const_sink_x_0_win)
        self.fft_vxx_0_0 = fft.fft_vcc(2048, True, (window.blackmanharris(2048)), True, 1)
        self.fft_vxx_0 = fft.fft_vcc(2048, True, (window.blackmanharris(2048)), True, 1)
        self.blocks_vector_to_stream_0_0 = blocks.vector_to_stream(gr.sizeof_float*1, 2048)
        self.blocks_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_float*1, 2048)
        self.blocks_stream_to_vector_1_1_0 = blocks.stream_to_vector(gr.sizeof_float*1, 2048)
        self.blocks_stream_to_vector_1_1 = blocks.stream_to_vector(gr.sizeof_float*1, 2048)
        self.blocks_stream_to_vector_1_0 = blocks.stream_to_vector(gr.sizeof_float*1, 2048)
        self.blocks_stream_to_vector_1 = blocks.stream_to_vector(gr.sizeof_float*1, 2048)
        self.blocks_stream_to_vector_0_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, 2048)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, 2048)
        self.blocks_repeat_0_0 = blocks.repeat(gr.sizeof_float*1, 2048)
        self.blocks_repeat_0 = blocks.repeat(gr.sizeof_float*1, 2048)
        self.blocks_nlog10_ff_0_0 = blocks.nlog10_ff(10.0, 2048, -32.0-6.0)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(10.0, 2048, -32.0-6.0)
        self.blocks_multiply_const_vxx_1_1 = blocks.multiply_const_vff((180.0/numpy.pi, ))
        self.blocks_multiply_const_vxx_1_0_0 = blocks.multiply_const_vcc((1.0/float(10000), ))
        self.blocks_multiply_const_vxx_1_0 = blocks.multiply_const_vcc((1.0/float(10000), ))
        self.blocks_multiply_const_vxx_0_1_1_0 = blocks.multiply_const_vff((1.0/2048.0, ))
        self.blocks_multiply_const_vxx_0_1_1 = blocks.multiply_const_vff((1.0/2048.0, ))
        self.blocks_multiply_const_vxx_0_1_0 = blocks.multiply_const_vff((1.0/6.0, ))
        self.blocks_multiply_const_vxx_0_1 = blocks.multiply_const_vff((1.0/6.0, ))
        self.blocks_multiply_conjugate_cc_0_0 = blocks.multiply_conjugate_cc(1)
        self.blocks_max_xx_0_0 = blocks.max_ff(2048,2048)
        self.blocks_max_xx_0 = blocks.max_ff(2048,2048)
        self.blocks_integrate_xx_2_0_0 = blocks.integrate_cc(10000, 1)
        self.blocks_integrate_xx_2_0 = blocks.integrate_cc(10000, 1)
        self.blocks_integrate_xx_1_0 = blocks.integrate_ff(2048, 1)
        self.blocks_integrate_xx_1 = blocks.integrate_ff(2048, 1)
        self.blocks_integrate_xx_0_0 = blocks.integrate_ff(10, 2048)
        self.blocks_integrate_xx_0 = blocks.integrate_ff(10, 2048)
        self.blocks_complex_to_mag_squared_0_0 = blocks.complex_to_mag_squared(2048)
        self.blocks_complex_to_mag_squared_0 = blocks.complex_to_mag_squared(2048)
        self.blocks_complex_to_float_0_0 = blocks.complex_to_float(1)
        self.blocks_complex_to_float_0 = blocks.complex_to_float(1)
        self.blocks_complex_to_arg_1_0 = blocks.complex_to_arg(1)
        self.blocks_add_xx_1_0 = blocks.add_vff(1)
        self.blocks_add_xx_1 = blocks.add_vff(1)
        self.blocks_add_const_vxx_0_0 = blocks.add_const_vff((numpy.repeat([-5.0],2048)))
        self.blocks_add_const_vxx_0 = blocks.add_const_vff((numpy.repeat([-5.0],2048)))
        self.analog_noise_source_x_1_0 = analog.noise_source_f(analog.GR_GAUSSIAN, 0.1, 0)
        self.analog_noise_source_x_1 = analog.noise_source_f(analog.GR_GAUSSIAN, 0.1, 0)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_noise_source_x_1, 0), (self.blocks_add_xx_1, 0))
        self.connect((self.analog_noise_source_x_1_0, 0), (self.blocks_add_xx_1_0, 1))
        self.connect((self.blocks_add_const_vxx_0, 0), (self.blocks_max_xx_0, 1))
        self.connect((self.blocks_add_const_vxx_0, 0), (self.qtgui_vector_sink_f_0, 1))
        self.connect((self.blocks_add_const_vxx_0_0, 0), (self.blocks_max_xx_0_0, 0))
        self.connect((self.blocks_add_const_vxx_0_0, 0), (self.qtgui_vector_sink_f_0_0, 0))
        self.connect((self.blocks_add_xx_1, 0), (self.blocks_stream_to_vector_1, 0))
        self.connect((self.blocks_add_xx_1_0, 0), (self.blocks_stream_to_vector_1_0, 0))
        self.connect((self.blocks_complex_to_arg_1_0, 0), (self.blocks_multiply_const_vxx_1_1, 0))
        self.connect((self.blocks_complex_to_float_0, 1), (self.qtgui_histogram_sink_x_0_0_0, 1))
        self.connect((self.blocks_complex_to_float_0, 0), (self.qtgui_histogram_sink_x_0_0_0, 0))
        self.connect((self.blocks_complex_to_float_0, 1), (self.qtgui_time_sink_x_0_0, 1))
        self.connect((self.blocks_complex_to_float_0, 0), (self.qtgui_time_sink_x_0_0, 0))
        self.connect((self.blocks_complex_to_float_0_0, 1), (self.qtgui_histogram_sink_x_0_0_0_0, 1))
        self.connect((self.blocks_complex_to_float_0_0, 0), (self.qtgui_histogram_sink_x_0_0_0_0, 0))
        self.connect((self.blocks_complex_to_float_0_0, 1), (self.qtgui_time_sink_x_0_0_0, 1))
        self.connect((self.blocks_complex_to_float_0_0, 0), (self.qtgui_time_sink_x_0_0_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_0, 0), (self.blocks_integrate_xx_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_0, 0), (self.blocks_integrate_xx_0_0, 0))
        self.connect((self.blocks_integrate_xx_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_integrate_xx_0_0, 0), (self.blocks_nlog10_ff_0_0, 0))
        self.connect((self.blocks_integrate_xx_1, 0), (self.blocks_repeat_0, 0))
        self.connect((self.blocks_integrate_xx_1_0, 0), (self.blocks_repeat_0_0, 0))
        self.connect((self.blocks_integrate_xx_2_0, 0), (self.blocks_multiply_const_vxx_1_0, 0))
        self.connect((self.blocks_integrate_xx_2_0_0, 0), (self.blocks_multiply_const_vxx_1_0_0, 0))
        self.connect((self.blocks_max_xx_0, 0), (self.qtgui_vector_sink_f_0, 0))
        self.connect((self.blocks_max_xx_0_0, 0), (self.qtgui_vector_sink_f_0_0, 1))
        self.connect((self.blocks_multiply_conjugate_cc_0_0, 0), (self.blocks_complex_to_arg_1_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_1, 0), (self.blocks_integrate_xx_1, 0))
        self.connect((self.blocks_multiply_const_vxx_0_1, 0), (self.blocks_stream_to_vector_1_1, 0))
        self.connect((self.blocks_multiply_const_vxx_0_1_0, 0), (self.blocks_integrate_xx_1_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_1_0, 0), (self.blocks_stream_to_vector_1_1_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_1_1, 0), (self.blocks_add_xx_1, 1))
        self.connect((self.blocks_multiply_const_vxx_0_1_1_0, 0), (self.blocks_add_xx_1_0, 0))
        self.connect((self.blocks_multiply_const_vxx_1_0, 0), (self.blocks_multiply_conjugate_cc_0_0, 1))
        self.connect((self.blocks_multiply_const_vxx_1_0, 0), (self.qtgui_const_sink_x_0, 0))
        self.connect((self.blocks_multiply_const_vxx_1_0_0, 0), (self.blocks_multiply_conjugate_cc_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_1_0_0, 0), (self.qtgui_const_sink_x_0, 1))
        self.connect((self.blocks_multiply_const_vxx_1_1, 0), (self.qtgui_number_sink_0_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.blocks_vector_to_stream_0, 0))
        self.connect((self.blocks_nlog10_ff_0_0, 0), (self.blocks_vector_to_stream_0_0, 0))
        self.connect((self.blocks_repeat_0, 0), (self.blocks_multiply_const_vxx_0_1_1, 0))
        self.connect((self.blocks_repeat_0_0, 0), (self.blocks_multiply_const_vxx_0_1_1_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.blocks_stream_to_vector_0_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.blocks_stream_to_vector_1, 0), (self.blocks_max_xx_0, 0))
        self.connect((self.blocks_stream_to_vector_1_0, 0), (self.blocks_max_xx_0_0, 1))
        self.connect((self.blocks_stream_to_vector_1_1, 0), (self.blocks_add_const_vxx_0, 0))
        self.connect((self.blocks_stream_to_vector_1_1_0, 0), (self.blocks_add_const_vxx_0_0, 0))
        self.connect((self.blocks_vector_to_stream_0, 0), (self.blocks_multiply_const_vxx_0_1, 0))
        self.connect((self.blocks_vector_to_stream_0_0, 0), (self.blocks_multiply_const_vxx_0_1_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.blocks_complex_to_mag_squared_0, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.blocks_complex_to_mag_squared_0_0, 0))
        self.connect((self.zeromq_sub_source_0, 0), (self.blocks_complex_to_float_0, 0))
        self.connect((self.zeromq_sub_source_0, 0), (self.blocks_integrate_xx_2_0, 0))
        self.connect((self.zeromq_sub_source_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.zeromq_sub_source_0_0, 0), (self.blocks_complex_to_float_0_0, 0))
        self.connect((self.zeromq_sub_source_0_0, 0), (self.blocks_integrate_xx_2_0_0, 0))
        self.connect((self.zeromq_sub_source_0_0, 0), (self.blocks_stream_to_vector_0_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "main_scope")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_vector_sink_f_0_0.set_x_axis(-self.samp_rate/2.0, +self.samp_rate/2048.0)
        self.qtgui_vector_sink_f_0.set_x_axis(-self.samp_rate/2.0, +self.samp_rate/2048.0)
        self.qtgui_time_sink_x_0_0_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_0_0.set_samp_rate(self.samp_rate)

    def get_gui_bit_range(self):
        return self.gui_bit_range

    def set_gui_bit_range(self, gui_bit_range):
        self.gui_bit_range = gui_bit_range
        self.qtgui_time_sink_x_0_0_0.set_y_axis(-2**self.gui_bit_range, +2**self.gui_bit_range)
        self.qtgui_time_sink_x_0_0.set_y_axis(-2**self.gui_bit_range, +2**self.gui_bit_range)
        self.qtgui_histogram_sink_x_0_0_0_0.set_x_axis(-2**self.gui_bit_range, +2**self.gui_bit_range)
        self.qtgui_histogram_sink_x_0_0_0.set_x_axis(-2**self.gui_bit_range, +2**self.gui_bit_range)
        self.qtgui_const_sink_x_0.set_y_axis(-2**self.gui_bit_range, +2**self.gui_bit_range)
        self.qtgui_const_sink_x_0.set_x_axis(-2**self.gui_bit_range, +2**self.gui_bit_range)


def main(top_block_cls=main_scope, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(channel=int(args['<channel>']))
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
