#!/usr/bin/env python
# @Author: Matthias Clahsen
# @Date:   2019-04-12 14:30:21
# @Last Modified by:   Matthias Clahsen
# @Last Modified time: 2019-05-15 09:52:08

from __future__ import division
from math import ceil


import time
import numpy
import digital_rf



class DigitalRfReaderOnline(digital_rf.DigitalRFReader):
    """Reader for digital_rf data."""

    def __init__(
            self,
            top_level_directory_arg,
            chunk_seconds,
            start_time=None,
            stop_time=None,
            pad_before=0,
            pad_after=0,
            timeout=None,
    ):
        '''Initialise the digital_rf reader.

        Parameters:
        -----------

        top_level_directory_arg : string
           Either a single top level directory containing Digital RF channel
           directories, or a list of such. A directory can be a file system
           path or a url, where the url points to a top level directory. Each
           must be a local path, or start with 'http://'', 'file://'', or
           'ftp://''.
        chunk_seconds : int,
            The number seconds to read per chunk.
        start_time : int64,
            Unix time to start the reading at.
            If None (default) the start of available data is used.
            start_time is forced to be a multiple of chunk time!
        stop_time : int64,
            Unix time to stop the reading at.
            If None (default) the reader waits for new data until timeout.
            Otherwise the reader stops if stop_time is reached.
        pad_before : int,
            Number of samples to include before start of regular chunk.
            Default is 0, has to be positive.
        pad_after : int,
            Number of samples to include after end of regular chunk.
            Default is 0, has to be positive.
        timeout : int,
            Number of seconds to wait for new data.
            If None (default) no timeout occurs.
        '''
        super(DigitalRfReaderOnline, self).__init__(top_level_directory_arg)
        self.__chunk_seconds = chunk_seconds
        self.__start_time = start_time
        self.__stop_time = stop_time
        self.__pad_before = pad_before
        self.__pad_after = pad_after
        self.__timeout = timeout
        self.__chunks_iter = None

    @property
    def chunk_seconds(self):
        return self.__chunk_seconds

    @property
    def start_time(self):
        return self.__start_time

    @property
    def stop_time(self):
        return self.__stop_time

    @property
    def pad_before(self):
        return self.__pad_before

    @property
    def pad_after(self):
        return self.__pad_after

    @property
    def timeout(self):
        return self.__timeout

    @property
    def channels(self):
        '''The names of all channels found.'''
        return sorted(self.get_channels())

    @property
    def properties(self):
        '''The properties of the first channel.

        It is assumed, that the properties are the same for all channels.

        '''
        return self.get_properties(self.channels[0])

    @property
    def sampling_rate(self):
        '''The sampling rate in samples per second.'''
        return int(self.properties['samples_per_second'])

    @property
    def bounds(self):
        '''Get bounds over all channels in samples.'''
        bounds = [self.get_bounds(c) for c in self.channels]
        start = max([b[0] for b in bounds])
        end = min([b[1] for b in bounds])
        return (start, end)

    def __poll_data(
            self,
            sample_index,
            timeout=None,
            poll_interval=1.0,
    ):
        '''waiting for new data by polling the data bounds.

        Parameters:
        -----------

        sample_index : int,
            index that have to be smaller than upper bound of data.
        '''
        if timeout is None:
            while True:
                time.sleep(poll_interval)
                if self.bounds[1] >= sample_index:
                    return True
        else:
            for _ in range(int(timeout // poll_interval)):
                time.sleep(poll_interval)
                if self.bounds[1] >= sample_index:
                    return True
        return False

    def chunks(self):
        '''Generator, that yields rawdata chunks.

        Yields:
        -------

        voltages : numpy.ndarray, complex64, shape(channels, times)
            The read rawdata voltages.
        time_start : int
            The unixtime of the regular! start of the chunk.
            The pad_before parameter is not included here!
        '''

        # Force start_time to be a multiple of chunk_time
        if self.start_time is not None:
            start_sample = (ceil(self.start_time / self.chunk_seconds) *
                            self.chunk_seconds * self.sampling_rate)

        if self.start_time is None or (start_sample - self.pad_before <
                                       self.bounds[0]):
            start_sample = ceil(
                (self.bounds[0] + self.pad_before) / self.sampling_rate /
                self.chunk_seconds) * self.sampling_rate * self.chunk_seconds

        if self.stop_time is None:
            stop_sample = None
        else:
            stop_sample = ceil(self.stop_time / self.chunk_seconds
                               ) * self.sampling_rate * self.chunk_seconds

        # read the data successively
        while True:
            if (stop_sample is not None) and (start_sample >= stop_sample):
                break
            read_len = (
                self.pad_before + self.chunk_seconds * self.sampling_rate +
                self.pad_after)

            # The last sample that need to be read for current chunk
            end_sample = start_sample + read_len - self.pad_before - 1

            if end_sample > self.bounds[1]:
                print(
                    'Waiting for new data (timeout: %.2fs)...' % self.timeout)
                if not self.__poll_data(
                        end_sample,
                        self.timeout,
                ):
                    print('Timeout, no more data found.')
                    break
            try:
                out_array = numpy.array([
                    self.read_vector(
                        start_sample=start_sample - self.pad_before,
                        vector_length=read_len,
                        channel_name=c,
                    ) for c in self.channels
                ])
            except IOError:
                # Data gap found
                print("Gap found in data. Continue.")
                continue
            finally:
                start_sample += self.chunk_seconds * self.sampling_rate
            yield (
                out_array,
                start_sample / self.sampling_rate - self.chunk_seconds,
            )

    def read_next_chunk(self):
        '''Returns the next chunk of data.'''
        if self.__chunks_iter is None:
            self.__chunks_iter = self.chunks()
        return next(self.__chunks_iter)



if __name__ == '__main__':


    FILE_PATH = "/home/radar/.simone-rx-raw"
    READER = DigitalRfReaderOnline(
        FILE_PATH,
        chunk_seconds=60,
        timeout=180.0,
        start_time=1541138520,
        # stop_time=1541138700,
        pad_before=0,
        pad_after=0,
    )
    print(READER.properties)
    print(READER.channels)

    ITER = READER.chunks()

    while True:
        try:
            data, t_start = READER.read_next_chunk()
        except StopIteration:
            # No more data available...
            break
        #print(helpers.unix2utc_str(t_start), t_start)
        print(data.shape)
        print(data.dtype)

    print('#' * 80)

    for data, t_start in ITER:
        # print(READER.start_time)
        # print(READER.sampling_rate)
        # print(READER.bounds[1])
        # print(READER.bounds[1] / READER.sampling_rate)
        #print(helpers.unix2utc_str(t_start), t_start)
        print(data.shape)
        print(data.dtype)

    READER.close()

