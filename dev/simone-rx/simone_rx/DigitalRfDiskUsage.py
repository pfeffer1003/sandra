#!/usr/bin/env python


import os

activate_this = "%s/.simone-rx/bin/activate_this.py"%os.getenv('HOME')
execfile(activate_this, dict(__file__=activate_this))

import sys
import time
import glob
import numpy
import engfmt
import simone_rx
import terminaltables
import digital_rf.digital_rf_hdf5 as drf


from engfmt import quant_to_eng
from datetime import datetime
from colorclass import Color
from collections import OrderedDict
from terminaltables import DoubleTable






class DigitalRfDiskUsage(object):

    # ----------------------------------------------------------------------- #

    def __init__(self,config_path):
        """
        """
        self._config = simone_rx.Config()
        self._config.load(config_path)

        self._acq_path = str(self._config.get_acq_path())
        self._acq_disk_space_limit = long(self._config.get_acq_size())*long(1e009)

        self._acq_reader = self.__get__reader(self._acq_path)
        self._acq_channels = self.__get__channels(self._acq_reader)
        self._acq_properties = self.__get__properties(self._acq_reader,self._acq_channels)
        self._acq_sample_rates = self.__get__sample_rates(self._acq_properties)
        self._acq_sample_sizes = self.__get__sample_sizes(self._acq_properties)
        self._acq_bounds = self.__get__bounds(self._acq_reader,self._acq_channels)
        self._acq_blocks_sample = self.__get__blocks_sample(self._acq_reader,self._acq_channels,self._acq_bounds)
        self._acq_blocks_unix = self.__get__blocks_sample2unix(self._acq_blocks_sample,self._acq_sample_rates)
        self._acq_blocks_date = self.__get__blocks_unix2date(self._acq_blocks_unix)
        self._acq_blocks_size = self.__get__blocks_size(self._acq_blocks_unix,self._acq_sample_rates,self._acq_sample_sizes)
        self._acq_blocks_avail = self.__get__blocks_avail(self._acq_blocks_unix)
        self._acq_bounds_size = self.__get__bounds_size(self._acq_blocks_size)
        self._acq_disk_space_used = self.__get__disk_space_used(self._acq_bounds_size)

        '''
        self._arc_path = str(self._config.get_archive_path())
        self._arc_disk_space_limit = long(self._config.get_archive_usage())*long(1e009)

        self._arc_reader = self.__get__reader(self._arc_path)
        self._arc_channels = self.__get__channels(self._arc_reader)
        self._arc_properties = self.__get__properties(self._arc_reader,self._arc_channels)
        self._arc_sample_rates = self.__get__sample_rates(self._arc_properties)
        self._arc_sample_sizes = self.__get__sample_sizes(self._arc_properties)
        self._arc_bounds = self.__get__bounds(self._arc_reader,self._arc_channels)
        self._arc_blocks_sample = self.__get__blocks_sample(self._arc_reader,self._arc_channels,self._arc_bounds)
        self._arc_blocks_unix = self.__get__blocks_sample2unix(self._arc_blocks_sample,self._arc_sample_rates)
        self._arc_blocks_date = self.__get__blocks_unix2date(self._arc_blocks_unix)
        self._arc_blocks_size = self.__get__blocks_size(self._arc_blocks_unix,self._arc_sample_rates,self._arc_sample_sizes)
        self._arc_blocks_avail = self.__get__blocks_avail(self._arc_blocks_unix)
        self._arc_bounds_size = self.__get__bounds_size(self._arc_blocks_size)
        self._arc_disk_space_used = self.__get__disk_space_used(self._arc_bounds_size)
        '''

    # ----------------------------------------------------------------------- #

    def __str__(self):

        acq_table_data = []
        acq_table_data.append(
            [
                'Channel',
                'Blocks',
                'Avail ==\ndd HH:MM:SS',
                'Oldest >=\nyyyy-mm-dd HH:MM:SS',
                'Latest <=\nyyyy-mm-dd HH:MM:SS',
                'Used',
                'Expected'
            ],
        )
        for idx_channel,acq_channel \
        in enumerate(self._acq_channels):
            acq_table_row = []
            acq_table_row.append(Color("\033[1m%s\033[0m" % acq_channel))
            acq_table_row_blk_idx = ""
            acq_table_row_blk_dur = ""
            acq_table_row_blk_beg = ""
            acq_table_row_blk_end = ""
            acq_table_row_blk_use = ""
            len_block = len(self._acq_blocks_date[idx_channel])
            for idx_block,blocks_date_item,blocks_size_item \
            in zip(range(len_block),self._acq_blocks_date[idx_channel],self._acq_blocks_size[idx_channel]):
                if len_block==1:
                    acq_table_row_blk_idx += Color("\033[1m%s\033[0m\n" % (idx_block+1))
                    acq_table_row_blk_dur += Color("\033[1m%s\033[0m\n" % blocks_date_item[1])
                    acq_table_row_blk_beg += Color("\033[1m%s\033[0m\n" % blocks_date_item[0])
                    acq_table_row_blk_end += Color("\033[1m%s\033[0m\n" % blocks_date_item[2])
                    acq_table_row_blk_use += Color("\033[1m%4.3fGB\033[0m\n" % (float(blocks_size_item)/1e009))
                else:
                    acq_table_row_blk_idx += "%s\n" % (idx_block+1)
                    acq_table_row_blk_dur += "%s\n" % blocks_date_item[1]
                    acq_table_row_blk_beg += "%s\n" % blocks_date_item[0]
                    acq_table_row_blk_end += "%s\n" % blocks_date_item[2]
                    acq_table_row_blk_use += "%4.3fGB\n" % (float(blocks_size_item)/1e009)
            if len_block>1:
                acq_table_row_blk_idx = '-'* 6+'\n' + acq_table_row_blk_idx
                acq_table_row_blk_dur = '-'*11+'\n' + acq_table_row_blk_dur
                acq_table_row_blk_beg = '-'*19+'\n' + acq_table_row_blk_beg
                acq_table_row_blk_end = '-'*19+'\n' + acq_table_row_blk_end
                acq_table_row_blk_use = '-'*10+'\n' + acq_table_row_blk_use
                acq_table_row_blk_idx = Color("\033[1m%s\033[0m\n" % len_block) + acq_table_row_blk_idx
                acq_table_row_blk_dur = Color("\033[1m%s\033[0m\n" % self._acq_blocks_avail[idx_channel]) + acq_table_row_blk_dur
                acq_table_row_blk_beg = Color("\033[1m%s\033[0m\n" % self._acq_blocks_date[idx_channel][0][0]) + acq_table_row_blk_beg
                acq_table_row_blk_end = Color("\033[1m%s\033[0m\n" % self._acq_blocks_date[idx_channel][-1][2]) + acq_table_row_blk_end
                acq_table_row_blk_use = Color("\033[1m%4.3fGB\033[0m\n" % (float(self._acq_bounds_size[idx_channel])/1e009)) + acq_table_row_blk_use
            acq_table_row.append(acq_table_row_blk_idx.rstrip())
            acq_table_row.append(acq_table_row_blk_dur.rstrip())
            acq_table_row.append(acq_table_row_blk_beg.rstrip())
            acq_table_row.append(acq_table_row_blk_end.rstrip())
            acq_table_row.append(acq_table_row_blk_use.rstrip())
            acq_table_row.append(Color("\033[1m{autoblue}%4.3fGB{/autoblue}\033[0m" % (float(self._acq_disk_space_limit)/1.0e009/len(self._acq_channels))))
            acq_table_data.append(acq_table_row)
        acq_table_data.append(
            [
                '-','-','-','-',
                Color("\033[1m{autoblue}%s{/autoblue}\033[0m" % self.__get__unix2date(time.time())),
                Color("\033[1m{autoblue}%4.3fGB{/autoblue}\033[0m" % (float(sum(self._acq_bounds_size))/1.0e009)),
                Color("\033[1m{autoblue}%4.3fGB{/autoblue}\033[0m" % (float(self._acq_disk_space_limit)/1.0e009)),
            ]
        )
        acq_table = DoubleTable(acq_table_data,Color("\033[1mACQUISITION\033[0m"))
        acq_table.inner_heading_row_border = True
        acq_table.inner_row_border = True
        acq_table.justify_columns[0] = 'center'
        acq_table.justify_columns[1] = 'right'
        acq_table.justify_columns[2] = 'center'
        acq_table.justify_columns[3] = 'center'
        acq_table.justify_columns[4] = 'center'
        acq_table.justify_columns[5] = 'right'
        acq_table.justify_columns[6] = 'right'
        print
        print acq_table.table

        '''
        arc_table_data = []
        arc_table_data.append(
            [
                'Channel',
                'Blocks',
                'Avail ==\ndd HH:MM:SS',
                'Oldest >=\nyyyy-mm-dd HH:MM:SS',
                'Latest <=\nyyyy-mm-dd HH:MM:SS',
                'Used',
                'Expected'
            ],
        )
        for idx_channel,arc_channel \
        in enumerate(self._arc_channels):
            arc_table_row = []
            arc_table_row.append(Color("\033[1m%s\033[0m" % arc_channel))
            arc_table_row_blk_idx = ""
            arc_table_row_blk_dur = ""
            arc_table_row_blk_beg = ""
            arc_table_row_blk_end = ""
            arc_table_row_blk_use = ""
            len_block = len(self._arc_blocks_date[idx_channel])
            for idx_block,blocks_date_item,blocks_size_item \
            in zip(range(len_block),self._arc_blocks_date[idx_channel],self._arc_blocks_size[idx_channel]):
                if len_block==1:
                    arc_table_row_blk_idx += Color("\033[1m%s\033[0m\n" % (idx_block+1))
                    arc_table_row_blk_dur += Color("\033[1m%s\033[0m\n" % blocks_date_item[1])
                    arc_table_row_blk_beg += Color("\033[1m%s\033[0m\n" % blocks_date_item[0])
                    arc_table_row_blk_end += Color("\033[1m%s\033[0m\n" % blocks_date_item[2])
                    arc_table_row_blk_use += Color("\033[1m%4.3fGB\033[0m\n" % (float(blocks_size_item)/1e009))
                else:
                    arc_table_row_blk_idx += "%s\n" % (idx_block+1)
                    arc_table_row_blk_dur += "%s\n" % blocks_date_item[1]
                    arc_table_row_blk_beg += "%s\n" % blocks_date_item[0]
                    arc_table_row_blk_end += "%s\n" % blocks_date_item[2]
                    arc_table_row_blk_use += "%4.3fGB\n" % (float(blocks_size_item)/1e009)
            if len_block>1:
                arc_table_row_blk_idx = '-'* 6+'\n' + arc_table_row_blk_idx
                arc_table_row_blk_dur = '-'*11+'\n' + arc_table_row_blk_dur
                arc_table_row_blk_beg = '-'*19+'\n' + arc_table_row_blk_beg
                arc_table_row_blk_end = '-'*19+'\n' + arc_table_row_blk_end
                arc_table_row_blk_use = '-'*10+'\n' + arc_table_row_blk_use
                arc_table_row_blk_idx = Color("\033[1m%s\033[0m\n" % len_block) + arc_table_row_blk_idx
                arc_table_row_blk_dur = Color("\033[1m%s\033[0m\n" % self._arc_blocks_avail[idx_channel]) + arc_table_row_blk_dur
                arc_table_row_blk_beg = Color("\033[1m%s\033[0m\n" % self._arc_blocks_date[idx_channel][0][0]) + arc_table_row_blk_beg
                arc_table_row_blk_end = Color("\033[1m%s\033[0m\n" % self._arc_blocks_date[idx_channel][-1][2]) + arc_table_row_blk_end
                arc_table_row_blk_use = Color("\033[1m%4.3fGB\033[0m\n" % (float(self._arc_bounds_size[idx_channel])/1e009)) + arc_table_row_blk_use
            arc_table_row.append(arc_table_row_blk_idx.rstrip())
            arc_table_row.append(arc_table_row_blk_dur.rstrip())
            arc_table_row.append(arc_table_row_blk_beg.rstrip())
            arc_table_row.append(arc_table_row_blk_end.rstrip())
            arc_table_row.append(arc_table_row_blk_use.rstrip())
            arc_table_row.append(Color("\033[1m{autoblue}%4.3fGB{/autoblue}\033[0m" % (float(self._arc_disk_space_limit)/1.0e009/len(self._arc_channels))))
            arc_table_data.append(arc_table_row)
        arc_table_data.append(
            [
                '-','-','-','-',
                Color("\033[1m{autoblue}%s{/autoblue}\033[0m" % self.__get__unix2date(time.time())),
                Color("\033[1m{autoblue}%4.3fGB{/autoblue}\033[0m" % (float(sum(self._arc_bounds_size))/1.0e009)),
                Color("\033[1m{autoblue}%4.3fGB{/autoblue}\033[0m" % (float(self._arc_disk_space_limit)/1.0e009)),
            ]
        )
        arc_table = DoubleTable(arc_table_data,Color("\033[1mARCHIVE\033[0m"))
        arc_table.inner_heading_row_border = True
        arc_table.inner_row_border = True
        arc_table.justify_columns[0] = 'center'
        arc_table.justify_columns[1] = 'right'
        arc_table.justify_columns[2] = 'center'
        arc_table.justify_columns[3] = 'center'
        arc_table.justify_columns[4] = 'center'
        arc_table.justify_columns[5] = 'right'
        arc_table.justify_columns[6] = 'right'
        print
        print arc_table.table
        '''


        return ''

    # ----------------------------------------------------------------------- #

    def __get__disk_space_used(self,bounds_size):
        return sum(bounds_size)

    # ----------------------------------------------------------------------- #

    def __get__sample_sizes(self,properties):

        sample_sizes = []
        for properties_item in properties:
            if properties_item['is_complex']:
                sample_sizes_item = properties_item['H5Tget_size']*2
            else:
                sample_sizes_item = properties_item['H5Tget_size']
            sample_sizes.append(sample_sizes_item)
        
        return sample_sizes

    # ----------------------------------------------------------------------- #

    def __get__sample_rates(self,properties):
        
        sample_rates = []
        for properties_item in properties:
            sample_rates_item = properties_item['samples_per_second']
            sample_rates.append(sample_rates_item)
        
        return sample_rates

    # ----------------------------------------------------------------------- #

    def __get__channels(self,reader):
        return reader.get_channels()

    # ----------------------------------------------------------------------- #

    def __get__reader(self,path):
        return drf.DigitalRFReader(path)

    # ----------------------------------------------------------------------- #

    def __get__bounds(self,reader,channels):
        
        bounds = []
        for channels_item in channels:
            bounds_item = reader.get_bounds(channels_item)
            bounds_item = list(bounds_item)
            bounds_item[1] += 1
            bounds.append(bounds_item)
        
        return bounds

    # ----------------------------------------------------------------------- #

    def __get__bounds_size(self,blocks_size):

        bounds_size = []
        for blocks_size_item in blocks_size:
            bounds_size_item = sum(blocks_size_item)
            bounds_size.append(bounds_size_item)
        
        return bounds_size

    # ----------------------------------------------------------------------- #

    def __get__blocks_avail(self,blocks_unix):

        blocks_avail = []
        for blocks_unix_item in blocks_unix:
            blocks_avail_item = 0
            for block_unix_item in blocks_unix_item:
                blocks_avail_item += block_unix_item[1]
            blocks_avail.append(self.__get__seconds2daytime(blocks_avail_item))

        return blocks_avail

    # ----------------------------------------------------------------------- #

    def __get__blocks_sample(self,reader,channels,bounds):
        
        blocks_sample = []
        for bounds_item,channels_item \
        in zip(bounds,channels):
            try:
                blocks_sample_item = reader.get_continuous_blocks(
                    bounds_item[0],
                    bounds_item[1],
                    channels_item,
                )
                blocks_sample_item = blocks_sample_item.items()
                blocks_sample_item = [
                    list(block_sample_item) 
                    for block_sample_item in blocks_sample_item
                ]
                for block_sample_item in blocks_sample_item:
                    block_sample_item.append(
                        block_sample_item[0]+\
                        block_sample_item[1]
                    )
                blocks_sample.append(blocks_sample_item)
            except:
                continue
        
        return blocks_sample

    # ----------------------------------------------------------------------- #

    def __get__blocks_sample2unix(self,blocks_sample,sample_rates):

        blocks_unix = []
        for blocks_sample_item,sample_rate \
        in zip(blocks_sample,sample_rates):
            blocks_unix_item = []
            for block_sample_item in blocks_sample_item:
                block_unix_item = [
                    long(item/sample_rate) 
                    for item in block_sample_item
                ]
                blocks_unix_item.append(block_unix_item)
            blocks_unix.append(blocks_unix_item)
        
        return blocks_unix

    # ----------------------------------------------------------------------- #

    def __get__blocks_unix2date(self,blocks_unix):

        blocks_date = []
        for blocks_unix_item in blocks_unix:
            blocks_date_item = []
            for block_unix_item in blocks_unix_item:
                block_date_item = [
                    self.__get__unix2date(block_unix_item[0]),
                    self.__get__seconds2daytime(block_unix_item[1]),
                    self.__get__unix2date(block_unix_item[2]),
                ]
                blocks_date_item.append(block_date_item)
            blocks_date.append(blocks_date_item)

        return blocks_date

    # ----------------------------------------------------------------------- #

    def __get__blocks_size(self,blocks_unix,sample_rates,sample_sizes):

        blocks_size = []
        for blocks_unix_item,sample_rate,sample_size \
        in zip(blocks_unix,sample_rates,sample_sizes):
            blocks_size_item = []
            for block_unix_item in blocks_unix_item:
                blocks_size_item.append(
                    long(block_unix_item[1])* \
                    long(sample_rate)* \
                    long(sample_size)
                )
            blocks_size.append(blocks_size_item)
        
        return blocks_size

    # ----------------------------------------------------------------------- #

    def __get__properties(self,reader,channels):

        properties = []
        for channels_item in channels:
            properties_item = reader.get_properties(channels_item)
            properties.append(properties_item)
        
        return properties

    # ----------------------------------------------------------------------- #

    def __get__seconds2daytime(self,seconds):
    
        (m,s) = divmod(seconds,60)
        (h,m) = divmod(m,60)
        (d,h) = divmod(h,24)
        daytime = "%02d %02d:%02d:%02d" % (d,h,m,s)
    
        return daytime

    # ----------------------------------------------------------------------- #

    def __get__unix2date(self,unix):

        date = int(unix)
        date = datetime.utcfromtimestamp(date)
        date = date.strftime('%Y-%m-%d %H:%M:%S')        
        
        return date

    # ----------------------------------------------------------------------- #
