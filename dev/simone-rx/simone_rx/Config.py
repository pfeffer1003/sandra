#!/usr/bin/env python

###############################################################################

import os
import sys
import numpy
import config
import xmlrpclib
import scipy.special
import matplotlib.pyplot as plt

from Verify import Verify
from config import Config as ConfigInit
from config import Mapping as ConfigDict
from config import Sequence as ConfigList

###############################################################################

class Config(object):

    # ======================================================================= #

    __USRP_IFACES = next(os.walk('/sys/class/net'))[1]
    __USRP_IFACES = filter(lambda x:'e' in x,__USRP_IFACES) 

    __PATH_HOME = os.getenv('HOME')
    __PATH_ACQUIRE = '%s/.simone-rx-raw' % __PATH_HOME
    __PATH_ARCHIVE = '%s/.simone-rx-archive' % __PATH_HOME

    # ----------------------------------------------------------------------- #

    ACQ_SYNC_SECS = [
        1  , 2  , 3  ,
        4  , 5  , 6  ,
        10 , 12 , 15 ,
        20 , 30 , 60 ,
    ]
    ACQ_DELAYS = {
        5000    : 4  , 8000    : 4  , 10000   : 4  ,
        12500   : 4  , 20000   : 5  , 25000   : 5  ,
        40000   : 5  , 50000   : 7  , 100000  : 9  ,
        200000  : 10 , 250000  : 11 , 500000  : 11 ,
        625000  : 11 , 1000000 : 11 , 1250000 : 12 ,
        2500000 : 12 , 5000000 : 13 ,
    }
    ACQ_RATES = sorted(ACQ_DELAYS.keys())

    # ======================================================================= #

    def __init__(self):

        self._Data = ConfigInit()

    # ======================================================================= #
    # ATTRIBUTES GLOBAL POSITION
    # ======================================================================= #

    def get_gps_lat(self): return self._Data.GlobalPosition.Latitude
    def get_gps_lon(self): return self._Data.GlobalPosition.Longitude
    def get_gps_alt(self): return self._Data.GlobalPosition.Altitude

    # ----------------------------------------------------------------------- #

    def set_gps_lat(self,val): self._Data.GlobalPosition.Latitude = val
    def set_gps_lon(self,val): self._Data.GlobalPosition.Longitude = val
    def set_gps_alt(self,val): self._Data.GlobalPosition.Altitude = val

    # ----------------------------------------------------------------------- #

    def __add__gps_lat(self,obj,val): self.__add__dict(obj,val,'Latitude')
    def __add__gps_lon(self,obj,val): self.__add__dict(obj,val,'Longitude')
    def __add__gps_alt(self,obj,val): self.__add__dict(obj,val,'Altitude')

    # ======================================================================= #
    # ATTRIBUTES ACQUISITION
    # ======================================================================= #

    def get_ACQ_RATES(self): return self.ACQ_RATES
    def get_ACQ_DELAYS(self): return self.ACQ_DELAYS
    def get_ACQ_SYNC_SECS(self): return self.ACQ_SYNC_SECS

    def get_acq_taps(self): return self._acq_taps
    def get_acq_decim(self): return self._acq_decim
    def get_acq_delay_init(self): return self._acq_delay_init

    def get_acq_rate(self): return self._Data.Acquisition.SampleRate
    def get_acq_path(self): return self._Data.Acquisition.Path
    def get_acq_size(self): return self._Data.Acquisition.Size
    def get_acq_delay(self): return self._Data.Acquisition.Delay
    def get_acq_dtype(self): return self._Data.Acquisition.DataType
    def get_acq_carrier(self): return self._Data.Acquisition.Carrier
    def get_acq_sync_sec(self): return self._Data.Acquisition.SyncSecond

    # ----------------------------------------------------------------------- #

    def set_acq_rate(self,val): self._Data.Acquisition.SampleRate = val 
    def set_acq_path(self,val): self._Data.Acquisition.Path = val  
    def set_acq_size(self,val): self._Data.Acquisition.Size = val 
    def set_acq_delay(self,val): self._Data.Acquisition.Delay = val 
    def set_acq_dtype(self,val): self._Data.Acquisition.DataType = val 
    def set_acq_carrier(self,val): self._Data.Acquisition.Carrier = val
    def set_acq_sync_sec(self,val): self._Data.Acquisition.SyncSecond = val 

    def __set__acq_taps(self,acq_rate,acq_decim):
        """
        """
        if acq_rate >= 200000:
            taps = [1.0]
        else:
            rolloff = 0.518
            radix = int(numpy.ceil(numpy.log2(7.0*float(acq_decim)+1.0)))
            sigma = rolloff/float(acq_decim)/numpy.pi
            f = numpy.arange(-0.5,+0.5,1.0/2.0**radix,numpy.float32)
            f_pos = (numpy.copy(f)+0.5/float(acq_decim))/sigma/numpy.sqrt(2.0)
            f_neg = (numpy.copy(f)-0.5/float(acq_decim))/sigma/numpy.sqrt(2.0)
            idx_low = int(0.5*len(f))-int(3.5*float(acq_decim))
            idx_upp = int(0.5*len(f))+int(3.5*float(acq_decim))+1
            taps  = scipy.special.erf(f_pos)-scipy.special.erf(f_neg)
            taps  = numpy.fft.fftshift(numpy.fft.ifft(numpy.fft.fftshift(taps)))
            taps  = taps.real[idx_low:idx_upp]
            taps /= numpy.sum(taps)
        taps = tuple(numpy.float64(taps).tolist())
        taps = numpy.float64(taps)
        self._acq_taps = taps

    def __set__acq_decim(self,acq_rate,sample_rate):
        self._acq_decim  = sample_rate
        self._acq_decim /= acq_rate  

    def __set__acq_delay_init(self,acq_rate):
        self._acq_delay_init = self.ACQ_DELAYS[acq_rate]

    # ----------------------------------------------------------------------- #

    def __add__acq_rate(self,obj,val): self.__add__dict(obj,val,'SampleRate')
    def __add__acq_path(self,obj,val): self.__add__dict(obj,val,'Path')
    def __add__acq_size(self,obj,val): self.__add__dict(obj,val,'Size')
    def __add__acq_delay(self,obj,val): self.__add__dict(obj,val,'Delay')
    def __add__acq_dtype(self,obj,val): self.__add__dict(obj,val,'DataType')
    def __add__acq_carrier(self,obj,val): self.__add__dict(obj,val,'Carrier')
    def __add__acq_sync_sec(self,obj,val): self.__add__dict(obj,val,'SyncSecond')

    # ======================================================================= #
    # ATTRIBUTES ARCHIVE
    # ======================================================================= #

    def get_archive_path(self): return self._Data.Archive.Path
    def get_archive_size(self): return self._Data.Archive.Size

    # ----------------------------------------------------------------------- #

    def set_archive_path(self,val): self._Data.Archive.Path = val 
    def set_archive_size(self,val): self._Data.Archive.Size = val 

    # ----------------------------------------------------------------------- #

    def __add__archive_path(self,obj,val): self.__add__dict(obj,val,'Path')
    def __add__archive_size(self,obj,val): self.__add__dict(obj,val,'Size')

    # ======================================================================= #
    # ATTRIBUTES CONTROL
    # ======================================================================= #

    def get_ctrl_mode(self): return self._Data.Control.Mode
    def get_ctrl_host(self): return self._Data.Control.Host
    def get_ctrl_port(self): return self._Data.Control.Port

    # ----------------------------------------------------------------------- #

    def set_ctrl_mode(self,val): self._Data.Control.Mode = val
    def set_ctrl_host(self,val): self._Data.Control.XmlRpcHost = val
    def set_ctrl_port(self,val): self._Data.Control.XmlRpcPort = val

    # ----------------------------------------------------------------------- #

    def __add__ctrl_mode(self,obj,val): self.__add__dict(obj,val,'Mode')
    def __add__ctrl_host(self,obj,val): self.__add__dict(obj,val,'Host')
    def __add__ctrl_port(self,obj,val): self.__add__dict(obj,val,'Port')

    # ======================================================================= #
    # ATTRIBUTES SIMULATION
    # ======================================================================= #

    def get_sim_noise_bits(self): return self._Data.Simulate.NoiseBits
    def get_sim_signal_bits(self): return self._Data.Simulate.SignalBits
    def get_sim_signal_doppler(self): return self._Data.Simulate.SignalDoppler

    # ----------------------------------------------------------------------- #

    def set_noise_bits(self,val): self._Data.Simulate.NoiseBits = val
    def set_signal_bits(self,val): self._Data.Simulate.SignalBits = val
    def set_signal_doppler(self,val): self._Data.Simulate.SignalDoppler = val

    # ----------------------------------------------------------------------- #

    def __add__sim_noise_bits(self,obj,val): self.__add__dict(obj,val,'NoiseBits')
    def __add__sim_signal_bits(self,obj,val): self.__add__dict(obj,val,'SignalBits')
    def __add__sim_signal_doppler(self,obj,val): self.__add__dict(obj,val,'SignalDoppler')

    # ======================================================================= #
    # ATTRIBUTES USRP
    # ======================================================================= #

    def get_usrp_rate(self): return self._usrp_rate
    def get_usrp_mboards(self): return self._usrp_mboards
    def get_usrp_channels(self): return self._usrp_channels

    def get_usrp_cpus(self): return [self.get_usrp_cpu(mb) for mb in xrange(self._usrp_mboards)]
    def get_usrp_routes(self): return [self.get_usrp_route(mb) for mb in xrange(self._usrp_mboards)]
    def get_usrp_addresses(self): return [self.get_usrp_address(mb) for mb in xrange(self._usrp_mboards)]
    def get_usrp_interfaces(self): return [self.get_usrp_interface(mb) for mb in xrange(self._usrp_mboards)]
    def get_usrp_time_sources(self): return [self.get_usrp_time_source(mb) for mb in xrange(self._usrp_mboards)]
    def get_usrp_clock_sources(self): return [self.get_usrp_clock_source(mb) for mb in xrange(self._usrp_mboards)]

    def get_usrp_cpu(self,mb): return self._Data['Usrp'][mb]['Cpu']
    def get_usrp_route(self,mb): return self._Data['Usrp'][mb]['Route']
    def get_usrp_address(self,mb): return self._Data['Usrp'][mb]['Address']
    def get_usrp_interface(self,mb): return self._Data['Usrp'][mb]['Interface']
    def get_usrp_time_source(self,mb): return self._Data['Usrp'][mb]['TimeSource']
    def get_usrp_clock_source(self,mb): return self._Data['Usrp'][mb]['ClockSource']

    # ----------------------------------------------------------------------- #

    def set_usrp_route(self,val,mb): self._Data['Usrp'][mb]['Route'] = val
    def set_usrp_address(self,val,mb): self._Data['Usrp'][mb]['Address'] = val
    def set_usrp_interface(self,val,mb): self._Data['Usrp'][mb]['Interface'] = val
    def set_usrp_time_source(self,val,mb): self._Data['Usrp'][mb]['TimeSource'] = val
    def set_usrp_clock_source(self,val,mb): self._Data['Usrp'][mb]['ClockSource'] = val

    def __set__usrp_rate(self,acq_rate):
        if acq_rate < 200000:
            self._usrp_rate = 200000
        else:
            self._usrp_rate = acq_rate

    def __set__usrp_channels(self):
        self._usrp_channels = len(self._Data.Channel)

    def __set__usrp_mboards(self,channels):
        self._usrp_mboards = len(self._Data.Usrp)
        #self._usrp_mboards = int(numpy.ceil(float(channels)/2.0))
        #if not self._usrp_mboards == len(self._Data.Usrp):
        #    raise LookupError(
        #        "\n\tUSRP devices 'count=%s', but must be 'count=%s' )." % (
        #            len(self._Data.Usrp),
        #            self._usrp_mboards,
        #        )
        #    )

    # ----------------------------------------------------------------------- #

    def __add__usrp_cpu(self,obj,val): self.__add__dict(obj,val,'Cpu')
    def __add__usrp_route(self,obj,val): self.__add__dict(obj,val,'Route')
    def __add__usrp_address(self,obj,val): self.__add__dict(obj,val,'Address')
    def __add__usrp_interface(self,obj,val): self.__add__dict(obj,val,'Interface')
    def __add__usrp_time_source(self,obj,val): self.__add__dict(obj,val,'TimeSource')
    def __add__usrp_clock_source(self,obj,val): self.__add__dict(obj,val,'ClockSource')

    # ======================================================================= #
    # ATTRIBUTES CHANNEL
    # ======================================================================= #

    def get_chan_phasors(self): return self._chan_phasors
    def get_chan_phasor(self,ch): return self._chan_phasors[ch]

    def get_chan_ports(self): return [self.get_chan_port(ch) for ch in xrange(self._usrp_channels)]
    def get_chan_phases(self): return [self.get_chan_phase(ch) for ch in xrange(self._usrp_channels)]
    def get_chan_antennas(self): return [self.get_chan_antenna(ch) for ch in xrange(self._usrp_channels)]

    def get_chan_port(self,ch): return self._Data['Channel'][ch]['Port']
    def get_chan_phase(self,ch): return self._Data['Channel'][ch]['Phase']
    def get_chan_antenna(self,ch): return list(self._Data['Channel'][ch]['Antenna'])

    # ----------------------------------------------------------------------- #

    def set_chan_port(self,val,ch): self._Data['Channel'][ch]['Port'] = val
    def set_chan_phase(self,val,ch): self._Data['Channel'][ch]['Phase'] = val
    def set_chan_antenna(self,val,ch): self._Data['Channel'][ch]['Antenna'] = val

    def __set__chan_phasors(self):
        chan_phasors = []
        for ch in xrange(self.get_usrp_channels()):
            chan_phasor  = self.get_chan_phase(ch)*1.0j*numpy.pi/180.0
            chan_phasor  = numpy.exp(chan_phasor)
            chan_phasor *= 2.0**15 # factor
            chan_phasor  = numpy.complex64(chan_phasor)
            chan_phasors.append(chan_phasor)
        self._chan_phasors = chan_phasors

    # ----------------------------------------------------------------------- #

    def __add__chan_port(self,obj,val): self.__add__dict(obj,val,'Port')
    def __add__chan_phase(self,obj,val): self.__add__dict(obj,val,'Phase')
    def __add__chan_antenna(self,obj,val): self.__add__dict(obj,val,'Antenna')

    # ======================================================================= #

    def __add__item(self,obj,val): obj.append(val,'')
    def __add__dict(self,obj,val,name): obj.addMapping(name,val,'',False)

    # ======================================================================= #

    def load(self,filename):
        self._Data = config.Config(file(filename,'r'))
        self.verify()

    # ----------------------------------------------------------------------- #

    def save(self,filename):

        self.verify()

        Data = ConfigInit()
        Data.GlobalPosition = ConfigDict()
        Data.Acquisition = ConfigDict()
        Data.Archive = ConfigDict()
        Data.Control = ConfigDict()
        Data.Simulate = ConfigDict()
        Data.Usrp = ConfigList()
        Data.Channel = ConfigList()

        self.__add__gps_lat(Data.GlobalPosition,self.get_gps_lat())
        self.__add__gps_lon(Data.GlobalPosition,self.get_gps_lon())
        self.__add__gps_alt(Data.GlobalPosition,self.get_gps_alt())

        self.__add__acq_rate(Data.Acquisition,self.get_acq_rate())
        self.__add__acq_path(Data.Acquisition,self.get_acq_path())
        self.__add__acq_size(Data.Acquisition,self.get_acq_size())
        self.__add__acq_delay(Data.Acquisition,self.get_acq_delay())
        self.__add__acq_dtype(Data.Acquisition,self.get_acq_dtype())
        self.__add__acq_carrier(Data.Acquisition,self.get_acq_carrier())
        self.__add__acq_sync_sec(Data.Acquisition,self.get_acq_sync_sec())

        self.__add__archive_path(Data.Archive,self.get_archive_path())
        self.__add__archive_size(Data.Archive,self.get_archive_size())

        self.__add__ctrl_mode(Data.Control,self.get_ctrl_mode())
        self.__add__ctrl_host(Data.Control,self.get_ctrl_host())
        self.__add__ctrl_port(Data.Control,self.get_ctrl_port())

        self.__add__sim_noise_bits(Data.Simulate,self.get_sim_noise_bits())
        self.__add__sim_signal_bits(Data.Simulate,self.get_sim_signal_bits())
        self.__add__sim_signal_doppler(Data.Simulate,self.get_sim_signal_doppler())

        for mb in xrange(self._usrp_mboards):
            tmp = ConfigDict()
            self.__add__usrp_cpu(tmp,self.get_usrp_cpu(mb))
            self.__add__usrp_route(tmp,self.get_usrp_route(mb))
            self.__add__usrp_address(tmp,self.get_usrp_address(mb))
            self.__add__usrp_interface(tmp,self.get_usrp_interface(mb))
            self.__add__usrp_time_source(tmp,self.get_usrp_time_source(mb))
            self.__add__usrp_clock_source(tmp,self.get_usrp_clock_source(mb))
            self.__add__item(Data.Usrp,dict(tmp))

        for ch in xrange(self._usrp_channels):
            tmp = ConfigDict()
            self.__add__chan_port(tmp,self.get_chan_port(ch))
            self.__add__chan_phase(tmp,self.get_chan_phase(ch))
            self.__add__chan_antenna(tmp,self.get_chan_antenna(ch))
            self.__add__item(Data.Channel,dict(tmp))

        Data.save(file(filename,'w'))

    # ----------------------------------------------------------------------- #

    def create(self,filename,usrp_channels):

        usrp_mboards = int(numpy.ceil(float(usrp_channels)/2.0))
        usrp_routes = ['A:A A:B' for m in xrange(usrp_mboards)]
        if bool(usrp_channels%2):
            usrp_routes[-1] = 'A:A'

        self._Data = ConfigInit()
        self._Data.GlobalPosition = ConfigDict()
        self._Data.Acquisition = ConfigDict()
        self._Data.Archive = ConfigDict()
        self._Data.Control = ConfigDict()
        self._Data.Simulate = ConfigDict()
        self._Data.Usrp = ConfigList()
        self._Data.Channel = ConfigList()

        self.__add__gps_lat(self._Data.GlobalPosition,0.0)
        self.__add__gps_lon(self._Data.GlobalPosition,0.0)
        self.__add__gps_alt(self._Data.GlobalPosition,0.0)

        self.__add__acq_rate(self._Data.Acquisition,100000)
        self.__add__acq_path(self._Data.Acquisition,self.__PATH_ACQUIRE)
        self.__add__acq_size(self._Data.Acquisition,1)
        self.__add__acq_delay(self._Data.Acquisition,0)
        self.__add__acq_dtype(self._Data.Acquisition,'float')
        self.__add__acq_carrier(self._Data.Acquisition,32550000.0)
        self.__add__acq_sync_sec(self._Data.Acquisition,1)

        self.__add__archive_size(self._Data.Archive,2)
        self.__add__archive_path(self._Data.Archive,self.__PATH_ARCHIVE)

        self.__add__ctrl_mode(self._Data.Control,'operate')
        self.__add__ctrl_host(self._Data.Control,'localhost')
        self.__add__ctrl_port(self._Data.Control,7000)

        self.__add__sim_noise_bits(self._Data.Simulate,3)
        self.__add__sim_signal_bits(self._Data.Simulate,3)
        self.__add__sim_signal_doppler(self._Data.Simulate,0.0)

        for mb in xrange(usrp_mboards):
            tmp = ConfigDict()
            self.__add__usrp_cpu(tmp,0)
            self.__add__usrp_route(tmp,usrp_routes[mb])
            self.__add__usrp_address(tmp,'192.168.10.%s'%(mb+2))
            self.__add__usrp_interface(tmp,self.__USRP_IFACES[-1])
            self.__add__usrp_time_source(tmp,'external')
            self.__add__usrp_clock_source(tmp,'external')
            self.__add__item(self._Data.Usrp,dict(tmp))

        for ch in xrange(usrp_channels):
            tmp = ConfigDict()
            self.__add__chan_port(tmp,7001+ch)
            self.__add__chan_phase(tmp,0.0)
            self.__add__chan_antenna(tmp,[0.0,0.0,0.0])
            self.__add__item(self._Data.Channel,dict(tmp))

        self.save(filename)

    # ----------------------------------------------------------------------- #

    @staticmethod
    def edit(filename):
        """
        """
        os.system("nano %s" % filename)

    # ----------------------------------------------------------------------- #

    def verify(self):

        Verify.float("GlobalPosition['Altitude']",self.get_gps_alt(),"(","-inf","+inf",")")
        Verify.float("GlobalPosition['Latitude']",self.get_gps_lat(),"[",-90.0,+90.0,"]")
        Verify.float("GlobalPosition['Longitude']",self.get_gps_lon(),"[",-180.0,+180.0,"]")

        Verify.str  ("Acquisition['Path']",self.get_acq_path())
        Verify.int  ("Acquisition['Rate']",self.get_acq_rate(),"[",200,25000000,"]",self.ACQ_RATES)
        Verify.int  ("Acquisition['Size']",self.get_acq_size(),"[",1,'+inf',")")
        Verify.int  ("Acquisition['Delay']",self.get_acq_delay(),"(",'-inf','+inf',")")
        Verify.float("Acquisition['Carrier']",self.get_acq_carrier(),"[",0.0,"+inf",")")
        Verify.str  ("Acquisition['DataType']",self.get_acq_dtype(),['float','int'])
        Verify.int  ("Acquisition['SyncSecond']",self.get_acq_sync_sec(),"[",1,60,"]",self.ACQ_SYNC_SECS)

        Verify.str  ("Archive['Path']",self.get_archive_path())
        Verify.int  ("Archive['Size']",self.get_archive_size(),"[",2,'+inf',")")

        self.__set__usrp_channels()
        self.__set__usrp_mboards(self.get_usrp_channels())
        self.__set__usrp_rate(self.get_acq_rate())

        self.__set__acq_decim(self.get_acq_rate(),self.get_usrp_rate())
        self.__set__acq_taps(self.get_acq_rate(),self.get_acq_decim())
        self.__set__acq_delay_init(self.get_acq_rate())

        Verify.ipv4 ("Control['Host']",self.get_ctrl_host())
        Verify.int  ("Control['Port']",self.get_ctrl_port(),"[",1024,32768,")")
        Verify.str  ("Control['Mode']",self.get_ctrl_mode(),['simulate','operate'])

        Verify.int  ("Simulation['NoiseBits']",self.get_sim_noise_bits(),"[",2**0,2**15,")")
        Verify.int  ("Simulation['SignalBits']",self.get_sim_signal_bits(),"[",2**0,2**15,")")
        Verify.float("Simulation['SignalDoppler']",self.get_sim_signal_doppler(),"[",-float(self.get_usrp_rate())/10.0,+float(self.get_usrp_rate())/10.0 ,"]")

        for mb in xrange(self._usrp_mboards):
            Verify.int ("Usrp['Cpu'][%s]"%(mb),self.get_usrp_cpu(mb),"[",0,24,"]")
            Verify.str ("Usrp['Route'][%s]"%(mb),self.get_usrp_route(mb),['A:A','A:A A:B'])
            Verify.ipv4("Usrp['Address'][%s]"%(mb),self.get_usrp_address(mb))
            Verify.str ("Usrp['Interface'][%s]"%(mb),self.get_usrp_interface(mb),self.__USRP_IFACES)
            Verify.str ("Usrp['ClockSource'][%s]"%(mb),self.get_usrp_clock_source(mb),['internal','external','mimo'])
            if self.get_usrp_clock_source(mb) == 'internal':
                Verify.str("Usrp['TimeSource'][%s]"%(mb),self.get_usrp_time_source(mb),['none'])
            else:
                Verify.str("Usrp['TimeSource'][%s]"%(mb),self.get_usrp_time_source(mb),[self.get_usrp_clock_source(mb)])

        for ch in xrange(self._usrp_channels):
            Verify.int  ("Channel['Port'][%s]"%(ch),self.get_chan_port(ch),"[",1024,32768,")")
            Verify.float("Channel['Phase'][%s]"%(ch),self.get_chan_phase(ch),"[",-180.0,+180.0,"]")
            Verify.list ("Channel['Antenna'][%s]"%(ch),self.get_chan_antenna(ch),3)
            for i,item in enumerate(self.get_chan_antenna(ch)):
                Verify.float("Channel['Antenna'][%s][%s]"%(ch,i),item,"(","-inf","+inf",")")

        self.__set__chan_phasors()

    # ======================================================================= #

    def show_rx_chain(self): pass
    def show_antenna_psf(self): pass
    def show_antenna_array(self): pass

    # ======================================================================= #

if __name__ == "__main__":

    filename = '/home/radar/Schreibtisch/rx_test.cfg'

    cfg = Config()
    #cfg.create(filename,10)

    cfg.load(filename)
    #cfg.set_chan_antenna([10.0,2.0,-3.0],20)
    #cfg.save(filename)

    print cfg.get_chan_antennas()

    sys.exit()
