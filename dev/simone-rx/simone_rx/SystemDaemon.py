#!/usr/bin/env python



import os
import sys
from Logging import Logging


def start_log():
    log = Logging.file("%s/.simone-rx/log/main.log" % os.getenv("HOME"))
    log.info("")
    log.info("#################")
    log.info("RECEIVER STARTED!")
    log.info("#################")

def stop_log():
    log = Logging.file("%s/.simone-rx/log/main.log" % os.getenv("HOME"))
    log.info("")
    log.info("#################")
    log.info("RECEIVER STOPPED!")
    log.info("#################")


class SystemDaemon(object):

    # ----------------------------------------------------------------------- #

    def __init__(self):
        pass

    # ----------------------------------------------------------------------- #

    @staticmethod
    def start():
        """
        """
        os.system('systemctl --user daemon-reload')
        os.system('systemctl --user start simone-rx')
        start_log()

    # ----------------------------------------------------------------------- #

    @staticmethod
    def stop():
        """
        """
        stop_log()
        os.system('systemctl --user daemon-reload')
        os.system('systemctl --user stop simone-rx')

    # ----------------------------------------------------------------------- #

    @staticmethod
    def status():
        """
        """
        os.system('systemctl --user daemon-reload')
        os.system('systemctl --user status simone-rx')

    # ----------------------------------------------------------------------- #

    @staticmethod
    def restart():
        """
        """
        stop_log()
        os.system('systemctl --user daemon-reload')
        os.system('systemctl --user restart simone-rx')
        start_log()

    # ----------------------------------------------------------------------- #

    @staticmethod
    def enable():
        """
        """
        stop_log()
        os.system('systemctl --user daemon-reload')
        os.system('systemctl --user stop simone-rx')
        os.system('systemctl --user enable simone-rx')
        os.system('systemctl --user daemon-reload')

    # ----------------------------------------------------------------------- #

    @staticmethod
    def disable():
        """
        """
        stop_log()
        os.system('systemctl --user daemon-reload')
        os.system('systemctl --user stop simone-rx')
        os.system('systemctl --user disable simone-rx')
        os.system('systemctl --user daemon-reload')

    # ----------------------------------------------------------------------- #



if __name__ == '__main__':


    sys.exit()