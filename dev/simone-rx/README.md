# SIMONE RECEIVER


## IMPORTANT NOTICE
**Sometimes it can happen that using USRP-N200 with images flashed with other 
versions of UHD they are not working properly or with undefined performances. 
It is always recommended to use the FPGA images for specific version of UHD. 
This means the user has somehow to deal with updating FPGA images and maybe boot 
the USRP into safe-mode when something is really strange. Therefore it the following 
link should be the recommended way to go:**

<https://files.ettus.com/manual/page_devices.html>

## Related Topics
- *MF/HF/VHF Radar*
- *Coherent Interferometric Receiver*
- *Atmospheric Research (Meteors/Mesosphere/Ionosphere)*
- ...

## Hardware Related Parts
- *USRP N200/210*
- *Trimble Thunderbolt E*
- ...


## Tested Operating System
Currently we tested the software on the following operating systems:
- *Ubuntu 18.04LTS*
- ...


## Basic Concept
This software is mostly written in ***Python 2.7*** and therefore we are creating
a virtual environment located at:
```
$ ~/.simone-rx
```
Inside we install all the dependencies without manipulating any other installed 
python libraries the user has already installed system wide. Also we have take
advantage of the *Linux* tools called ***SystemD*** 


## Dependencies already meet
When installing the software it will install additional libraries used for
the softwares functionality. The main parts are:
- *GnuRadio / UHD / DgitialRF*
- *Python / Intel-Numpy / Intel-Scipy*
- ...


## Pre-Requirements
We are assuming that first installation in combination with a new USRP N200 is 
the case for the default settings, which for the USRP N200 is a default *IP-Address* 
of ***192.168.10.2*** and therefore the user doesn't need to change major 
configuration parameters. For more detailed information please read the manuals 
and instructions from *Ettus-Research* located here:

<https://files.ettus.com/manual/page_usrp2.html>

This documentation covers all detailed hardware related things related to the 
USRP N200. We are also assuming that the user attached the USRP already with the 
comupter and the device could be seen with the following command:
```
$ uhd_find_devices
```
```
linux; GNU C++ version 7.3.0; Boost_106501; UHD_003.010.003.000-0-unknown

--------------------------------------------------
-- UHD Device 0
--------------------------------------------------
Device Address:
    type: usrp2
    addr: 192.168.10.2
    name: 
    serial: A1B2C3

```
Then make sure your ***USRP N200*** is *external* connected with a GPS device (e.g. 
***Trimble Thunderbolt E***) to the *1PPS* and *10MHz* reference *SMA* connectors.


## Download and Installation
You can clone the repository somewhere on your operating system. Normally we
creating a structure like this for our purposes:
```
$ mkdir ~/.GitLab
$ cd ~/.GitLab
$ git clone https://gitlab.com/pfeffer1003/simone-rx.git
$ cd simone-rx
$ ./install
$ source ~/.bashrc
$ simone-rx create 2
```
When this is finally executed, commandline tools are provided on your operating
system and these needs to be used for the first steps on using the software. 


## Default Settings
When the installation is done, the receiver configuration using default the 
following major default parameter configurations:

***Parameter*** | ***Value*** | ***Unit***
--- | --- | ---
`Latitude` | 0.0 | °
`Longitude` | 0.0 | °
`Altitude` | 0.0 | m
`Sample Rate` | 100000 | Hz
`Rawdata Path` | ~/.simone-rx-raw | -
`Rawdata Disk Usage` | 1 | GB
`Archive Path` | ~/.simone-rx-archive | -
`Archive Disk Usage` | 2 | GB

You can also look at the configuration file by using the following command:
```
$ simone-rx configure
```
```
GlobalPosition :
{
  Latitude : 0.0
  Longitude : 0.0
  Altitude : 0.0
}
Acquisition :
{
  Path : '/home/radar/.simone-rx-raw'
  Delay : 0
  Carrier : 32550000.0
  SampleRate : 100000
  DataType : 'float'
  DiskUsage : 1
  SyncSecond : 1
}
Archive :
{
  Path : '/home/radar/.simone-rx-archive'
  DiskUsage : 2
}
Control :
{
  Mode : 'operate'
  XmlRpcHost : 'localhost'
  XmlRpcPort : 7000
  CpuCores : [0]
}
Simulate :
{
  NoiseBits : 4
  SignalBits : 1
  SignalDoppler : 0.0
}
Usrp :
[
  {'ClockSource': 'external', 'Route': 'A:A A:B', 'TimeSource': 'external', 'Address': '192.168.10.2'}
]
Channel :
[
  {'Phase': 0.0, 'StreamPort': 7001, 'Antenna': [0.0, 0.0, 0.0]}
  {'Phase': 0.0, 'StreamPort': 7002, 'Antenna': [0.0, 0.0, 0.0]}
]

```


## First Running Example
First open a terminal on your machine and use the following command:
```
$ simone-rx start
```
This will actually start the current configured application and to check if it 
is running or not just type the following commands:
```
$ simone-rx status
```
```
● simone-rx.service - Deamon Script for SIMONe Receiver
   Loaded: loaded (/home/$USER/.config/systemd/user/simone-rx.service; enabled; vendor preset: enabled)
   Active: active (running) since Day YYY-MM-DD hh:mm:ss CEST; some time ago ago
 Main PID: 12345 (python)
   CGroup: /user.slice/user-1000.slice/user@1000.service/simone-rx.service
           ├─<1'st PID> <1'st spawned process here>
           ├─<2'nd PID> <2'nd spawned process here>
           └─<3'rd PID> <3'rd spawned process here>

```
also the user is able to check the current logging informations with the following command:
```
$ simone-rx log
```
```
2019-08-07 09:48:06,337 INFO     Initializing Receiver
2019-08-07 09:48:06,417 INFO     Configuration loaded
2019-08-07 09:48:06,424 INFO     Pinning Process to cores 0
2019-08-07 09:48:06,425 INFO     Starting RPC Server
2019-08-07 09:48:06,425 INFO     Radar parameter calculated
2019-08-07 09:48:07,013 INFO     Creating Timestamps for Start
2019-08-07 09:48:07,013 INFO     Creating Flowgraph
2019-08-07 09:48:07,071 INFO     Buffer Acquisition PID=25851
2019-08-07 09:48:07,072 INFO     Buffer Archive PID=25852
2019-08-07 09:48:07,072 INFO     Mirror Rawdata PID=25853
2019-08-07 09:48:17,004 INFO     Starting Receiver
2019-08-07 09:48:18,005 INFO     Setting delay 0
2019-08-07 09:48:18,027 INFO     Setting phase 0.0 for channel 0
2019-08-07 09:48:18,028 INFO     Setting phase 0.0 for channel 1
```
Additionally the user has the following basic commands provided on the terminal 
for controlling the application:
```
$ simone-rx stop
$ simone-rx restart
```
Another nice tool is the following command for listing the current status of the 
rawdata and archived data buffers:
```
$ simone-rx diskusage
```
```
SIMONe Receiver Buffer Status:

	RAWDATA:

		Current Size: 1G
		Latest  Size: 966M	/home/radar/.simone-rx-raw

		Current Time: 2019-08-07 13:24:20
		Latest  Time: 2019-08-07 13:23:59 for ch000
		Latest  Time: 2019-08-07 13:23:59 for ch001

	ARCHIVE:

		Current Size: 2G
		Latest  Size: 1,9G	/home/radar/.simone-rx-archive

		Current Time: 2019-08-07 13:24:20
		---------------------------------
		Latest  Time: 2019-08-07 13:24:01 for ch000
		Latest  Time: 2019-08-07 13:24:01 for ch001
```
Also there are more advanced command line tools for the user provided, but they 
will be explained more detailed in an other part of the documentation. 


## Using External Harddrives
To be on the safe side, the user has to mount the harddrive by adding an entry 
to the ***/etc/fstab*** to be able archiving the rawdata after a reboot or power 
failure. For gettings the major informations we use ***gparted*** for covering 
them, but be careful with this tools. Quckly, things can go wrong by using wrong 
partitions and accidently modify something.
```
$ sudo nano /etc/fstab
```
```
UUID=<your uuid> <your mount path> <your filesystem> defaults 0 2
```
```
$ sudo reboot
$ simone-rx configure
```
```
...
Archive :
{
  Path : <your mount path>/<rawdata dir>
  DiskUsage : <disk space in GB>
}
...
```
```
$ simone-rx restart
$ simone-rx status
$ simone-rx log
```

**If the user want to use big external hard-disk-drives for archiving the sampled 
rawdata it is necessary to take special parameters into account. We have not yet 
automated the calculation for the inotify parameters for the specific operating sysems. 
This means, it could happen that the one of the specific inotify parameters reach one 
of the following limits:**

- Maximum of User Instances     ```max_user_instances```
- Maximum of User Watches       ```max_user_watches```
- Maximum of Queued Events      ```max_queued_events```

The reason for this is, that the rawdata is first sampled to the **internal harddrive** of 
the computer and then **mirrored** to the path of the **archived** rawdata. Also these 
directories will be **buffered** on the **acquisition path** and the **archived path** 
thus it depends on the **size of files**, **count of directories** and **count of channels**. 
If errors occur during the buffer and mirror processes, there will be raised an error by these 
child processes and they will report this. There it is maybe necessary to increase the number 
of the maximum limits for inotify. First the user can easily find out his specific default 
limits of the operating system parameters:

```
$ cat /proc/sys/fs/intoify/max_user_instances
$ cat /proc/sys/fs/inotify/max_user_watches
$ cat /proc/sys/fs/inotify/max_queued_events
```

These could be for example:

***Variable*** | ***Value***
--- | ---
`max_user_instances` | 128
`max_user_watches` | 8192
`max_queued_events` | 16384



## Activate the Daemon on Reboot
Since using **systemd** there is also an easy way to force the execution of scripts 
after reboot or power failure automatically this can be achieved by typing this 
command:
```
$ simone-rx enable
```
If the user decides not use this feature anymore, it can also be deactivated by 
by the following command:
```
$ simone-rx disable
```


# ADVANCED TOOLS

For some specific reasons the software does not only depending on already owned 
hardware like USRP or GPS devices. Therefore the user can use the following 
commands to switch easily between the type of operation mode:
```
$ simone-rx mode operate
$ simone-rx restart
```
```
$ simone-rx mode simulate
$ simone-rx restart
```

## Operation Commands
```
$ simone-rx set delay VALUE 
$ simone-rx set phase VALUE <channel>
```


## Simulation Commands
```
$ simone-rx set delay VALUE 
$ simone-rx set phase VALUE <channel>
$ simone-rx set doppler VALUE
$ simone-rx set noise VALUE
$ simone-rx set signal VALUE
```