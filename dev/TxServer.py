#!/usr/bin/env python



import os
import sys
import time








class _Center(object):
    def __get__(self, instance, owner):
        return instance.x + instance.width/2.0, instance.y + instance.height/2.0
    def __set__(self, instance, value):
        instance.x = value[0] - instance.width/2.0
        instance.y = value[1] - instance.height/2.0
 
 
class Rect(object):
    center = _Center()
 
    def __init__(self,x,y,width,height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
 
    def __repr__(self):
        return "Rect({x}, {y}, {width}, {height})".format(**vars(self))





rect = Rect(2,3,4,5)
print rect.center
rect.width = 5
print rect.center
rect.width = 6
print rect.center




class _DutyCycle(object):

    def __get__(self, instance, owner):
        return instance.x + instance.width/2.0, instance.y + instance.height/2.0
    def __set__(self, instance, value):
        instance.x = value[0] - instance.width/2.0
        instance.y = value[1] - instance.height/2.0

class Pulse(object):

	dutycycle = _DutyCycle()

	def __init__(self):
		self.__length = int(length)
		self.__period = int(length)




class TxServer(object):


	__dutycycle = _DutyCycle()
	__symbolrate = __SymbolRate()

	def __str__(self): pass
	def __init__(self): pass


	##########################
	# INDEPENDENT PARAMETERS #
	##########################

	def get_length(self,): return self.__length
	def get_codetype(self,): return self.__codetype
	def get_symbolrate(self,): return self.__symbolrate
	
	def set_length(self,length): self.__length = int(value)
	def set_codetype(self,codetype): self.__codetype = str(codetype)
	def set_symbolrate(self,value): self.__symbolrate = float(value)


	########################
	# DEPENDENT PARAMETERS #
	########################

	def get_period(self,): return self.__period
	def get_dutycycle(self,): return self.__dutycycle
	
	def __set_period(self,period): self.__period = int(value)
	def __set_dutycycle(self,length,period): self.__dutycycle = float()
