#!/usr/bin/env python



import os
import sys
import numpy
import scipy.signal
import gnuradio.blocks
import matplotlib.pyplot as plt

class RootRaisedGaussian(object):

    def __init__(self,
            rolloff=0.518,
            interpolation=1,
        ):
        
        self.__rolloff = float(rolloff)
        self.__interpolation = int(interpolation)

        self.__set__radix(self.__interpolation)
        self.__set__sigma(self.__interpolation,self.__rolloff)
        self.__set__freqs(self.__radix)
        self.__set__taps(self.__interpolation,self.__freqs,self.__sigma)


    def get_rolloff(self): return self.__rolloff
    def get_interpolation(self): return self.__interpolation


    def get_radix(self):
        return self.__radix
    def __set__radix(self,interpolation):
        radix = 7.0*float(interpolation)+1.0
        radix = numpy.ceil(numpy.log2(radix))
        self.__radix = int(radix)

    def get_sigma(self):
        return self.__sigma
    def __set__sigma(self,interpolation,rolloff):
        sigma  = float(rolloff)
        sigma /= float(interpolation)
        sigma /= numpy.pi
        self.__sigma = float(sigma)

    def get_freqs(self):
        return self.__freqs
    def __set__freqs(self,radix):
        freqs = numpy.arange(
            start=-0.5,
            stop=+0.5,
            step=1.0/2.0**radix,
            dtype=numpy.float32)
        self.__freqs = freqs

    def get_taps(self):
        return self.__taps
    def __set__taps(self,interpolation,freqs,sigma):
        freqs_pos = (numpy.copy(freqs)+0.5/float(interpolation))/sigma/numpy.sqrt(2.0)
        freqs_neg = (numpy.copy(freqs)-0.5/float(interpolation))/sigma/numpy.sqrt(2.0)
        idx_low = int(0.5*len(freqs))-int(3.5*float(interpolation))
        idx_upp = int(0.5*len(freqs))+int(3.5*float(interpolation))+1
        taps = scipy.special.erf(freqs_pos)-scipy.special.erf(freqs_neg)
        taps = numpy.fft.fftshift(numpy.fft.ifft(numpy.fft.fftshift(taps)))
        taps = taps.real[idx_low:idx_upp]*float(interpolation)/2.0
        self.__taps = taps




class TxWaveform(object):


    def __str__(self):
        string  = "=========\n"
        string += "Waveform:\n"
        string += "=========\n\n"
        string += ""
        return string


    def __init__(self,
            cpu=None,
            seed=0,
            length=1,
            period=100,
            rolloff=0.518,
            codetype='cw',
            symbolrate=100000.0,
            interpolation=2,
            amplitude_max = 1.0,
            dutycycle_max = 100.0,
    ):

        self.__delay = 0
        self.__phase = 0.0
        self.__amplitude = 0.0

        self.__cpu = cpu
        self.__seed = int(seed)
        self.__length = int(length)
        self.__period = int(period)
        self.__rolloff 0 float(rolloff)
        self.__codetype = str(codetype)
        self.__symbolrate = float(symbolrate)
        self.__interpolation = int(interpolation)
        self.__amplitude_max = float(amplitude_max)
        self.__dutycycle_max = float(dutycycle_max)

        self.__set__fir(self.__interpolation)
        self.__set__ipp(self.__period,self.__symbolrate)
        self.__set__prf(self.__period,self.__symbolrate)
        self.__set__code(self.__codetype,self.__length)
        self.__set__phasor(self.__amplitude,self.__phase)
        self.__set__dutycycle(self.__length,self.__period)
        self.__set__samplerate(self.__symbolrate,self.__interpolation)

    # ======================================================================= +

    def get_cpu(self): return self.__cpu
    def get_seed(self): return self.__seed
    def get_length(self): return self.__length
    def get_period(self): return self.__period
    def get_codetype(self): return self.__codetype
    def get_symbolrate(self): return self.__symbolrate
    def get_interpolation(self): return self.__interpolation
    def get_amplitude_max(self): return self.__amplitude_max
    def get_dutycycle_max(self): return self.__dutycycle_max

    #def get_waveform(self): return self.__waveform

    # ======================================================================= +

    def get_ipp(self):
        return self.__ipp
    def __set__ipp(self,period,symbolrate):
        ipp  = float(period)
        ipp /= float(symbolrate)
        self.__ipp = ipp

    def get_prf(self):
        return self.__prf
    def __set__prf(self,period,symbolrate):
        prf  = float(symbolrate)
        prf /= float(period)
        self.__prf = prf

    def get_fir(self):
        return self.__fir
    def __set__fir(self,interpolation):
        self.__fir = RootRaisedGaussian(interpolation)
        #rolloff = 0.518
        #radix = int(numpy.ceil(numpy.log2(7.0*float(interpolation)+1.0)))
        #sigma = rolloff/float(interpolation)/numpy.pi
        #freqs = numpy.arange(-0.5,+0.5,1.0/2.0**radix,numpy.float32)
        #freqs_pos = (numpy.copy(freqs)+0.5/float(interpolation))/sigma/numpy.sqrt(2.0)
        #freqs_neg = (numpy.copy(freqs)-0.5/float(interpolation))/sigma/numpy.sqrt(2.0)
        #idx_low = int(0.5*len(freqs))-int(3.5*float(interpolation))
        #idx_upp = int(0.5*len(freqs))+int(3.5*float(interpolation))+1
        #taps = scipy.special.erf(freqs_pos)-scipy.special.erf(freqs_neg)
        #taps = numpy.fft.fftshift(numpy.fft.ifft(numpy.fft.fftshift(taps)))
        #taps = taps.real[idx_low:idx_upp]*float(interpolation)/2.0

    def get_code(self):
        return self.__code
    def __set__code(self,codetype,length):
        if codetype == "cw":
            code = {"A":[]}
        if codetype == "barker":
            code = {"A":[]}
        if codetype == "pseudo":
            code = {"A":[]}
        if codetype == "complementary":
            code = {
                "A":[],
                "B":[],
            }
        self.__code = code

    def get_phasor(self):
        return self.__phasor
    def __set__phasor(self,amplitude,phase):
        phasor  = amplitude
        phasor *= numpy.exp(1j*phase)
        self.__phasor = complex(phasor)

    def get_dutycycle(self):
        return self.__dutycycle
    def __set__dutycycle(self,length,period):
        dutycycle  = float(length)
        dutycycle /= float(period)
        dutycycle *= 100.0
        self.__dutycycle = float(dutycycle)

    def get_samplerate(self):
        return self.__samplerate
    def __set__samplerate(self,symbolrate,interpolation):
        samplerate  = float(symbolrate)
        samplerate *= float(interpolation)
        self.__samplerate = float(samplerate)

    # ======================================================================= +

    def get_phase(self):
        return self.__phase
    def set_phase(self,phase):
        self.__phase = float(phase)

    def get_delay(self):
        return self.__delay
    def set_delay(self,delay):
        self.__delay = int(delay)

    def get_amplitude(self):
        return self.__amplitude
    def set_amplitude(self,amplitude):
        if amplitude < 0.0                 : amplitude = 0.0
        if amplitude > self.__amplitude_max: amplitude = self.__amplitude_max
        self.__amplitude = float(amplitude)

    # ======================================================================= +


if __name__ == "__main__":

    wf = TxWaveform()
    print wf

    sys.exit()
