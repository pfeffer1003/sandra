import os
import sys
import warnings



class Validator(object):

    # ----------------------------------------------------------------------- #

    __DTYPES = [int,float,long,bool,str]

    # ----------------------------------------------------------------------- #

    def __init__(self,value,name,dtype,unit=None,lower=None,upper=None,choices=[],threshold=None,log=None):

        """
        """

        self.__init__name(name=name)
        self.__init__dtype(dtype=dtype)
        self.__init__unit(unit=unit)
        self.__init__log(log=log)

        self.set_lower(lower=lower)
        self.set_upper(upper=upper)
        self.set_choices(choices=choices)
        self.set_threshold(threshold=threshold)
        self.set_value(value=value)

    # ----------------------------------------------------------------------- #

    def __str__(self):
        """
        """
        if self.__lower is None: lower = "-inf"
        else:                    lower = self.__lower 
        if self.__upper is None: upper = "+inf"
        else:                    upper = self.__upper
        string  = "Quantity:\n"
        string += "---------\n\n"
        string += "\tName\t\t:\t %s\n" % self.__name
        string += "\tValue\t\t:\t %s\n" % self.__value
        string += "\tUnit\t\t:\t %s\n" % self.__unit
        string += "\tThreshold\t:\t %s\n" % self.__threshold
        string += "\tDataType\t:\t %s\n" % self.__dtype
        string += "\tInterval\t:\t [%s,%s]\n" % (lower,upper)
        string += "\tChoices\t\t:\t %s\n" % self.__choices
        return string

    # ----------------------------------------------------------------------- #

    def get_DTYPES(self):
        """
        """
        return self.__DTYPES

    DTYPES = property(get_DTYPES)

    # ----------------------------------------------------------------------- #

    def __init__name(self, name):
        """
        """
        if not isinstance(name,str):
            raise TypeError()
        if name == "":
            raise ValueError()
        self.__name = name

    def get_name(self):
        """
        """
        return self.__name

    name = property(get_name)

    # ----------------------------------------------------------------------- #

    def __init__dtype(self, dtype):
        """
        """
        if not dtype in self.__DTYPES:
            raise TypeError()
        self.__dtype = dtype

    def get_dtype(self):
        """
        """
        return self.__dtype

    dtype = property(get_dtype)

    # ----------------------------------------------------------------------- #

    def __init__unit(self, unit):
        """
        """
        if self.__dtype is str:  unit = None
        if self.__dtype is bool: unit = None
        if not unit is None:
            if not isinstance(unit,str):
                raise TypeError()
            if unit == "":
                raise ValueError()
        self.__unit = unit

    def get_unit(self):
        """
        """
        return self.__unit

    unit = property(get_unit)

    # ----------------------------------------------------------------------- #

    def __init__log(self,log):
        """
        TODO!!! and check the update function also!!!
        """
        self.__log = log

    # ----------------------------------------------------------------------- #

    def get_lower(self):
        """
        """
        return self.__lower

    def set_lower(self, lower):
        """
        """
        if self.__dtype is str:  lower = None
        if self.__dtype is bool: lower = None
        if not lower is None:
            if not isinstance(lower,self.__dtype):
                raise TypeError()
            try:
                if not self.__upper is None:
                    if lower > self.__upper:
                        raise ValueError()
            except AttributeError:
                pass
        self.__lower = lower

    lower = property(get_lower,set_lower)

    # ----------------------------------------------------------------------- #

    def get_upper(self):
        """
        """
        return self.__upper

    def set_upper(self, upper):
        """
        """
        if self.__dtype is str:  upper = None
        if self.__dtype is bool: upper = None
        if not upper is None:
            if not isinstance(upper,self.__dtype):
                raise TypeError()
            if not self.__lower is None:
                if upper < self.__lower:
                    raise ValueError()
        self.__upper = upper

    upper = property(get_upper,set_upper)

    # ----------------------------------------------------------------------- #

    def get_choices(self):
        """
        """
        return self.__choices

    def set_choices(self, choices):
        """
        """
        if not isinstance(choices,list):
            raise TypeError()
        if choices != []:
            if not self.__dtype is bool:
                for i,choice in enumerate(choices):
                    if not isinstance(choice,self.__dtype):
                        raise TypeError()
                    if not self.__lower is None:
                        if choice < self.__lower:
                            raise ValueError()
                    if not self.__upper is None:
                        if choice > self.__upper:
                            raise ValueError()
            else:
                choices = [True,False]
        self.__choices = choices

    choices = property(get_choices,set_choices)

    # ----------------------------------------------------------------------- #

    def get_threshold(self):
        """
        """
        return self.__threshold

    def set_threshold(self,threshold):
        """
        """
        if self.__dtype is str:  threshold = None
        if self.__dtype is bool: threshold = None
        if not threshold is None:
            if not isinstance(threshold,self.__dtype):
                raise TypeError()
            if not self.__lower is None:
                if threshold < self.__lower:
                    raise ValueError()
            if not self.__upper is None:
                if threshold > self.__upper:
                    raise ValueError()
        self.__threshold = threshold

    threshold = property(get_threshold,set_threshold)

    # ----------------------------------------------------------------------- #

    def get_value(self):
        """
        """
        return self.__value

    def set_value(self, value):
        """
        """
        if not isinstance(value,self.__dtype):
            raise TypeError()
        if not self.__lower is None:
            if value < self.__lower:
                raise ValueError()
        if not self.__upper is None:
            if value > self.__upper:
                raise ValueError("MyError")
        if self.__choices != []:
            if not value in self.__choices:
                raise ValueError()
        if not self.__threshold is None:
            if value > self.__threshold:
                raise ValueError()
        self.__value = value

    value = property(get_value)

    # ----------------------------------------------------------------------- #

    def update(self,value):
        """
        """
        try:
            self.set_value(value)
        except TypeError as err:
            warnings.warn(str(err),stacklevel=3)
        except ValueError as err:
            warnings.warn(str(err),stacklevel=3)

    # ----------------------------------------------------------------------- #





if __name__ == "__main__":

    phase = Validator(value=0.0,name="Phase",dtype=float,unit="deg",choices=[],lower=-180.0,upper=+180.0,threshold=90.0)
    phase.update(90.0); print phase.value
    phase.update(89.0); print phase.value
    phase.update(300.0); print phase.value

    sys.exit()