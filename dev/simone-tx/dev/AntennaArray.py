#!/usr/bin/env python






import os
import sys
import numpy
import matplotlib.pyplot as plt





class CoordinateTransform(object):

	@staticmethod
	def car2cyl(self,car): pass
	@staticmethod
	def car2sph(self,car): pass

	@staticmethod
	def cyl2car(self,cyl): pass
	@staticmethod
	def cyl2sph(self,cyl): pass

	@staticmethod
	def sph2car(self,sph): pass
	@staticmethod
	def sph2cyl(self,sph): pass




class Positions(object):

	def __init__(self,antennas):
		self._antennas = int(antennas)
		self._positions = numpy.zeros((3,antennas),dtype=numpy.float32)


	def get_positions(self): return self._positions
	def set_position(self,position,index): pass
	positions = property(get_positions)



class Cartesian(Positions):

	def __init__(self,antennas):
		Positions.__init__(self,antennas)

	def get_x(self): return self._positions[0,:]
	def get_y(self): return self._positions[1,:]
	def get_z(self): return self._positions[2,:]

	def set_x(self,value,index): self._positions[0,index] = value
	def set_y(self,value,index): self._positions[1,index] = value
	def set_z(self,value,index): self._positions[2,index] = value

	x = property(get_x)
	y = property(get_y)
	z = property(get_z)





class Cylindric(Positions):

	def __init__(self,antennas):
		Positions.__init__(self,antennas)

	def get_radius(self): return self._radius
	def get_azimut(self): return self._azimut
	def get_height(self): return self._height

	def set_radius(self,value,index): pass
	def set_azimut(self,value,index): pass
	def set_height(self,value,index): pass



class Spherical(Positions):

	def __init__(self,antennas):
		Positions.__init__(self,antennas)

	def __init__(self,antennas):
		Positions.__init__(self,antennas)

	def get_radius(self): return self._radius
	def get_azimut(self): return self._azimut
	def get_(self): return self._

	def set_radius(self,value,index): pass
	def set_azimut(self,value,index): pass
	def set_(self,value,index): pass



class CircularArray(object):

	def __init__(self,
			center=True,
			radius=25.0,
			antennas=6,
		):

		self._center = bool(center)
		self._radius = float(radius)
		self._antennas = int(antennas)

		self._cartesian = Cartesian(antennas)
		self._cylindric = Cylindric(antennas)
		self._spherical = Spherical(antennas)

		print self._cartesian.positions

		if     self._center: self._angle = 360.0/float(self._antennas-1)
		if not self._center: self._angle = 360.0/float(self._antennas-0)

		if     self._center: self._rad = numpy.float32([self._radius]*(self._antennas-1)+[0.0])
		if not self._center: self._rad = numpy.float32([self._radius]*(self._antennas-0))
		
		if     self._center: self._phi = numpy.float32([self._angle*i for i in range(self._antennas-1)]+[0.0])
		if not self._center: self._phi = numpy.float32([self._angle*i for i in range(self._antennas-0)])

		#self._z = numpy.float32([0.0]*self._antennas)

	#def get_x(self): return self._x
	#def get_y(self): return self._y
	#def get_z(self): return self._z
	#def get_rad(self): return self._rad
	#def get_phi(self): return self._phi
	def get_angle(self): return self._angle
	def get_center(self): return self._center
	def get_radius(self): return self._radius
	def get_antennas(self): return self._antennas




	def shift(self,x0,y0): pass
	def rotate(self,phi0): pass




if __name__ == "__main__":

	array = CircularArray()




	#print array.get_rad()
	#print array.get_phi()
	#print array.get_z()







	#print array.center
	#print array.radius
	#print array.antennas
	#print array.angle

	#print array.cartesian
	#print array.cylindric
	#print array.spherical


	sys.exit()