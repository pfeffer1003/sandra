#!/usr/bin/env python


import os
import sys
import numpy



RAMP_MINIMUM = 0.0
RAMP_MAXIMUMS = [0.35,0.34,0.33,0.34,0.1,0.0]

RAMP_VALUES = []
for i in xrange(len(RAMP_MAXIMUMS)):
	try:
		RAMP_VALUES.append(list(numpy.arange(0.0,RAMP_MAXIMUMS[i]+RAMP_MAXIMUMS[i]/10.0,RAMP_MAXIMUMS[i]/10.0)))
	except ZeroDivisionError:
		RAMP_VALUES.append(list(numpy.zeros(10)))
	print RAMP_VALUES[-1]
