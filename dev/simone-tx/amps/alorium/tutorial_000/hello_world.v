`timescale 1ns / 1ns



module f_add(a,b,ci,sum,co);
    
    input   a;
    input   b;
    input   ci;

    output  sum;
    output  co;

    wire    a;
    wire    b;
    wire    ci;
    wire    co;
    wire    sum;
    wire    n1;
    wire    n2;
    wire    n3;

    xor (n1,a,b);
    xor (sum,n1,ci);
    and (n2,a,b);
    and (n3,n1,ci);
    or  (co,n2,n3);

endmodule