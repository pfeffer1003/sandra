`timescale 1ns/1ns


module testbench;

    reg     a;
    reg     b;
    reg     ci;

    wire    sum;
    wire    co;

    f_add i1 (a,b,ci,sum,co);

    initial begin
        
        a=0;
        b=0;
        ci=0;

        #10     a=1;
        #10     b=1;
        #10     ci=1;

        //#10     $stop;
        $finish;

    end

    initial begin

        $dumpfile("simple.vcd");
        $dumpvars(0, i1);
        $monitor(
            "Bei %0d: \t a=%b b=%b ci=%b sum=%b co=%b",
            $time,a,b,ci,sum,co
        );

    end
endmodule