module my_module
genvar


 
integer
real 


reg
wire


if
else
while 
for 
case 


assign


or
nor
xor
and
nand
not 


begin
end


always
posedge 
negedge
edge 


buf
input
output


time 



// ---------- //
// HPA-40-500 //
// ---------- //

input gps_clock_10mhz
input gps_strober_1pps

input amp_status_swr
input amp_status_temp
input amp_status_input

output strober_amplifier

output radar_strober_prf
output radar_strober_sync
output radar_strober_symbolrate
