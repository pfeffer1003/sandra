// this module simply forwards 
// the fetched 1pps signal from 
// the connected gps receiver



module amplifier_ctrl(
        in_1pps,
        in_10mhz,
        in_reset,
        enable_tx,
        enable_cw,
        enable_pulse,
        enable_rf_vox,
        N_PRF,
        N_DUTY,
        N_SYNC,
        N_DELAY,
        out_1pps,
        out_10mhz,
        out_gate,
        );

    // ##################################################################### //
    // DECLARE INPUTS

    input       in_1pps;
    input       in_10mhz;
    input       in_reset;

    input       enable_tx;
    input       enable_cw;
    input       enable_pulse;
    input       enable_rf_vox;

    input[15:0] N_PRF;
    input[15:0] N_DUTY;
    input[15:0] N_SYNC;
    input[15:0] N_DELAY;

    // ##################################################################### //
    // DECLARE OUTPUTS

    output      out_1pps;
    output      out_10mhz;
    output      out_sync;
    output      out_prf;
    output      out_gate;

    // ##################################################################### //
    // DECLARE REGISTERS

    reg[15:0]   n_prf;
    reg[15:0]   n_duty;
    reg[15:0]   n_sync;
    reg[15:0]   n_delay;

    // ##################################################################### //
    // DECLARE WIRES

    wire        in_1pps;
    wire        in_10mhz;

    wire        enable_tx;
    wire        enable_cw;
    wire        enable_pulse;
    wire        enable_rf_vox;

    wire        N_PRF;
    wire        N_DUTY;
    wire        N_SYNC;
    wire        N_DELAY;

    // ##################################################################### //

    always @ (posedge in_clk)
    begin
        if (in_1pps=='1' && enable_tx='1')

        else
            
    end

    // ##################################################################### //
    // ##################################################################### //
    // ##################################################################### //
    // ##################################################################### //
    // ##################################################################### //



endmodule