// this module simply forwards 
// the fetched 1pps signal from 
// the connected gps receiver



module strobe_generate_prf(
		i_strobe_sync,
		o_strobe_prf);

    input   i_strobe_sync;
    output  o_strobe_prf;

    wire    i_strobe_sync;
    wire    o_strobe_prf;

endmodule