// this module simply forwards 
// the fetched 1pps signal from 
// the connected gps receiver



module strobe_generate_sync(
		i_strobe_1pps,
		i_strobe_cnt,
		o_strobe_sync);

    input   	i_strobe_1pps;
    input[7:0]	i_strobe_cnt;
    output  	o_strobe_sync;

    wire    i_strobe_1pps;
    wire    o_strobe_sync;

endmodule