// this module simply forwards 
// the fetched 1pps signal from 
// the connected gps receiver



module strobe_generate_1pps(
        i_strobe_1pps,
        o_strobe_1pps);

    input   i_strobe_1pps;
    output  o_strobe_1pps; 

    wire    i_strobe_1pps;
    wire    o_strobe_1pps;

endmodule