# SIMONE TRANSMITTER


## IMPORTANT NOTICE
**Sometimes it can happen that using USRP-N200 with images flashed with other 
versions of UHD they are not working properly or with undefined performances. 
It is always recommended to use the FPGA images for specific version of UHD. 
This means the user has somehow to deal with updating FPGA images and maybe boot 
the USRP into safe-mode when something is really strange. Therefore it the following 
link should be the recommended way to go:**

<https://files.ettus.com/manual/page_devices.html>

## Related Topics
- *MF/HF/VHF Radar*
- *Coherent Interferometric Transmitter*
- *Atmospheric Research (Meteors/Mesosphere/Ionosphere)*
- ...

## Hardware Related Parts
- *USRP N200/210*
- *Trimble Thunderbolt E*
- ...


## Tested Operating System
Currently we tested the software on the following operating systems:
- *Ubuntu 18.04LTS*
- ...


## Basic Concept
This software is mostly written in ***Python 2.7*** and therefore we are creating
a virtual environment located at:
```
$ ~/.simone-tx
```
Inside we install all the dependencies without manipulating any other installed 
python libraries the user has already installed system wide. Also we have take
advantage of the *Linux* tools called ***SystemD*** 


## Dependencies already meet
When installing the software it will install additional libraries used for
the softwares functionality. The main parts are:
- *GnuRadio / UHD / DgitialRF*
- *Python / Intel-Numpy / Intel-Scipy*
- ...


## Pre-Requirements
We are assuming that first installation in combination with a new USRP N200 is 
the case for the default settings, which for the USRP N200 is a default *IP-Address* 
of ***192.168.10.2*** and therefore the user doesn't need to change major 
configuration parameters. For more detailed information please read the manuals 
and instructions from *Ettus-Research* located here:

<https://files.ettus.com/manual/page_usrp2.html>

This documentation covers all detailed hardware related things related to the 
USRP N200. We are also assuming that the user attached the USRP already with the 
comupter and the device could be seen with the following command:
```
$ uhd_find_devices
```
```
linux; GNU C++ version 7.3.0; Boost_106501; UHD_003.010.003.000-0-unknown

--------------------------------------------------
-- UHD Device 0
--------------------------------------------------
Device Address:
    type: usrp2
    addr: 192.168.10.2
    name: 
    serial: A1B2C3

```
Then make sure your ***USRP N200*** is *external* connected with a GPS device (e.g. 
***Trimble Thunderbolt E***) to the *1PPS* and *10MHz* reference *SMA* connectors.


## Download and Installation
You can clone the repository somewhere on your operating system. Normally we
creating a structure like this for our purposes:
```
$ mkdir ~/.GitLab
$ cd ~/.GitLab
$ git clone https://gitlab.com/pfeffer1003/simone-tx.git
$ cd simone-tx
$ ./install
$ source ~/.bashrc
$ simone-tx create 1
```
When this is finally executed, commandline tools are provided on your operating
system and these needs to be used for the first steps on using the software. 


## Default Settings
When the installation is done, the transmitter configuration using default the 
following major default parameter configurations:

***Parameter*** | ***Value*** | ***Unit***
--- | --- | ---
`Latitude` | 0.0 | °
`Longitude` | 0.0 | °
`Altitude` | 0.0 | m
`Symbol Rate` | 100000 | Hz


You can also look at the configuration file by using the following command:
```
$ simone-tx configure
```
```
GlobalPosition :
{
  Latitude : 0.0
  Longitude : 0.0
  Altitude : 0.0
}
Code :
{
  Type : 'cw'
  Length : 1
  Period : 100
}
Symbol :
{
  Rate : 100000
  Delay : 0
  Carrier : 32550000.0
  SyncSecond : 1
}
Control :
{
  Mode : 'operate'
  XmlRpcHost : 'localhost'
  XmlRpcPort : 6000
  CpuCores : [0]
}
Usrp :
[
  {'ClockSource': 'external', 'Route': 'A:A', 'TimeSource': 'external', 'Address': '192.168.10.2'}
]
Channel :
[
  {'Phase': 0.0, 'Seed': 0, 'Antenna': [0.0, 0.0, 0.0], 'Amplitude': 0.0, 'StreamPort': 6001}
]


```


## First Running Example
First open a terminal on your machine and use the following command:
```
$ simone-tx start
```
This will actually start the current configured application and to check if it 
is running or not just type the following commands:
```
$ simone-tx status
```
```
● simone-tx.service - Deamon Script for SIMONe Transmitter
   Loaded: loaded (/home/radar/.config/systemd/user/simone-tx.service; disabled; vendor preset: enabled)
   Active: active (running) since Sat 2019-08-10 17:32:35 CEST; 2s ago
 Main PID: 13148 (python)
   CGroup: /user.slice/user-1000.slice/user@1000.service/simone-tx.service
           └─13148 python /home/radar/.simone-tx/bin/main

Aug 10 17:32:35 lappfeffer systemd[1221]: Started Deamon Script for SIMONe Transmitter.
Aug 10 17:32:35 lappfeffer main[13148]: linux; GNU C++ version 7.3.0; Boost_106501; UHD_003.010.003.000-0-unknown
Aug 10 17:32:36 lappfeffer main[13148]: -- Opening a USRP2/N-Series device...
Aug 10 17:32:36 lappfeffer main[13148]: -- Current recv frame size: 1472 bytes
Aug 10 17:32:36 lappfeffer main[13148]: -- Current send frame size: 1472 bytes
```
also the user is able to check the current logging informations with the following command:
```
$ simone-tx log
```
```
2019-08-10 17:32:35,594 INFO     Initializing Transmitter
2019-08-10 17:32:35,622 INFO     Configuration loaded
2019-08-10 17:32:35,622 INFO     Starting RPC Server
2019-08-10 17:32:36,013 INFO     Creating Flowgraph
2019-08-10 17:32:37,133 INFO     Starting Transmitter
2019-08-10 17:32:38,134 INFO     Setting delay 0
2019-08-10 17:32:38,134 INFO     Setting phase 0.0 for channel 0
2019-08-10 17:32:38,134 INFO     Setting amplitude 0.0 for channel 0
```
Additionally the user has the following basic commands provided on the terminal 
for controlling the application:
```
$ simone-tx stop
$ simone-tx restart
```

Also there are more advanced command line tools for the user provided, but they 
will be explained more detailed in an other part of the documentation. 


## Activate the Daemon on Reboot
Since using **systemd** there is also an easy way to force the execution of scripts 
after reboot or power failure automatically this can be achieved by typing this 
command:
```
$ simone-tx enable
```
If the user decides not use this feature anymore, it can also be deactivated by 
by the following command:
```
$ simone-tx disable
```


# ADVANCED TOOLS

For some specific reasons the software does not only depending on already owned 
hardware like USRP or GPS devices. Therefore the user can use the following 
commands to switch easily between the type of operation mode:
```
$ simone-tx mode operate
$ simone-tx restart
```
```
$ simone-tx mode simulate
$ simone-tx restart
```
```
$ simone-tx mode calibrate
$ simone-tx restart
```

## Settings Commands
```
$ simone-tx set delay VALUE 
$ simone-tx set phase VALUE <channel>
```