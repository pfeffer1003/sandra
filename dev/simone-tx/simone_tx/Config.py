#!/usr/bin/env python

###############################################################################

import os
import sys
import numpy
import config
import xmlrpclib
import scipy.special
import matplotlib.pyplot as plt

###############################################################################

from Verify import Verify

###############################################################################

class Config(object):

    __USRP_IFACES = next(os.walk('/sys/class/net'))[1]
    __USRP_IFACES = filter(lambda x:'e' in x,__USRP_IFACES) 

    # ======================================================================= #

    __data_gps = config.Mapping()
    __data_gps.addMapping(key='Latitude',value=0.0,comment='')
    __data_gps.addMapping(key='Longitude',value=0.0,comment='')
    __data_gps.addMapping(key='Altitude',value=0.0,comment='')

    __data_code = config.Mapping()
    __data_code.addMapping(key='Type',value='cw',comment='')
    __data_code.addMapping(key='Length',value=1,comment='')
    __data_code.addMapping(key='Period',value=100,comment='')

    __data_limits = config.Mapping()
    __data_limits.addMapping(key='Amplitude',value=1.0,comment='')

    __data_symbol = config.Mapping()
    __data_symbol.addMapping(key='Rate',value=100000,comment='')
    __data_symbol.addMapping(key='Delay',value=0,comment='')
    __data_symbol.addMapping(key='Carrier',value=32550000.0,comment='')
    __data_symbol.addMapping(key='SyncSecond',value=1,comment='')

    __data_control = config.Mapping()
    __data_control.addMapping(key='Mode',value='operate',comment='')
    __data_control.addMapping(key='XmlRpcHost',value='localhost',comment='')
    __data_control.addMapping(key='XmlRpcPort',value=6000,comment='')
    __data_control.addMapping(key='CpuCores',value=[0],comment='')

    __data_channel = config.Mapping()
    __data_channel.addMapping(key='Seed',value=0,comment='')
    __data_channel.addMapping(key='Phase',value=0.0,comment='')
    __data_channel.addMapping(key='Amplitude',value=0.0,comment='')
    __data_channel.addMapping(key='Antenna',value=[0.0,0.0,0.0],comment='')
    __data_channel.addMapping(key='StreamPort',value=6001,comment='')

    # ----------------------------------------------------------------------- #

    _symbol_delays = {
    	1000 : 4,
        5000 : 4,
        8000 : 4,
        10000 : 4,
        12500 : 4,
        20000 : 4,
        25000 : 5,
        40000 : 6,
        50000 : 6,
        100000 : 10,
        200000 : 13,
        250000 : 13,
        500000 : 13,
        625000 : 13,
        1000000 : 13,
        1250000 : 13,
        2500000 : 14,
        5000000 : 16,
    }

    _symbol_rates = [
        250, 400, 500, 800,
        1000, 2000, 2500, 4000, 5000, 8000, 
        10000, 12500, 20000, 25000, 40000, 50000, 
        100000, 200000, 250000, 500000, 625000, 
        1000000, 1250000, 2500000, 5000000
    ]

    _symbol_sync_seconds = [
        1, 2, 3, 4, 5, 6,
        10, 12, 15, 20, 30, 60,
    ]

    # ======================================================================= #

    def __init__(self):

        self._data = config.Config()

    # ======================================================================= #

    def get_gps_latitude(self): return self._data.GlobalPosition.Latitude
    def get_gps_longitude(self): return self._data.GlobalPosition.Longitude
    def get_gps_altitude(self): return self._data.GlobalPosition.Altitude

    def get_code_periods(self): return self._code_periods

    def get_code_type(self): return self._data.Code.Type
    def get_code_length(self): return self._data.Code.Length    
    def get_code_period(self): return self._data.Code.Period
    
    def get_symbol_taps(self): return self._symbol_taps
    def get_symbol_rates(self): return self._symbol_rates
    def get_symbol_delays(self): return self._symbol_delays
    def get_symbol_sync_seconds(self): return self._symbol_sync_seconds
    def get_symbol_interpolation(self): return self._symbol_interpolation
    
    def get_symbol_rate(self): return self._data.Symbol.Rate
    def get_symbol_delay(self): return self._data.Symbol.Delay
    def get_symbol_carrier(self): return self._data.Symbol.Carrier
    def get_symbol_sync_second(self): return self._data.Symbol.SyncSecond

    def get_limit_amplitude(self): return self._data['Limits']['Amplitude']

    def get_control_mode(self): return self._data.Control.Mode
    def get_control_rpc_host(self): return self._data.Control.XmlRpcHost
    def get_control_rpc_port(self): return self._data.Control.XmlRpcPort    
    def get_control_cpu_cores(self): return list(self._data.Control.CpuCores)

    def get_usrp_channels(self): return self._usrp_channels
    def get_usrp_sample_rate(self): return self._usrp_sample_rate
    def get_usrp_motherboards(self): return self._usrp_motherboards

    def get_usrp_routes(self): return [self.get_usrp_route(mb) for mb in xrange(self._usrp_motherboards)]
    def get_usrp_addresses(self): return [self.get_usrp_address(mb) for mb in xrange(self._usrp_motherboards)]
    def get_usrp_interfaces(self): return [self.get_usrp_interface(mb) for mb in xrange(self._usrp_motherboards)]
    def get_usrp_time_sources(self): return [self.get_usrp_time_source(mb) for mb in xrange(self._usrp_motherboards)]
    def get_usrp_clock_sources(self): return [self.get_usrp_clock_source(mb) for mb in xrange(self._usrp_motherboards)]

    def get_channel_codes(self): return self._channel_codes
    def get_channel_phasors(self): return self._channel_phasors
    def get_channel_patterns(self): return self._channel_patterns

    def get_channel_seeds(self): return [self.get_channel_seed(ch) for ch in xrange(self.get_usrp_channels())]
    def get_channel_phases(self): return [self.get_channel_phase(ch) for ch in xrange(self.get_usrp_channels())]
    def get_channel_antennas(self): return [self.get_channel_antenna(ch) for ch in xrange(self.get_usrp_channels())]
    def get_channel_amplitudes(self): return [self.get_channel_amplitude(ch) for ch in xrange(self.get_usrp_channels())]
    def get_channel_stream_ports(self): return [self.get_channel_stream_port(ch) for ch in xrange(self.get_usrp_channels())]

    # ----------------------------------------------------------------------- #

    def get_usrp_route(self,mb): return self._data['Usrp'][mb]['Route']
    def get_usrp_address(self,mb): return self._data['Usrp'][mb]['Address']
    def get_usrp_interface(self,mb): return self._data['Usrp'][mb]['Interface']
    def get_usrp_time_source(self,mb): return self._data['Usrp'][mb]['TimeSource']
    def get_usrp_clock_source(self,mb): return self._data['Usrp'][mb]['ClockSource']

    # ----------------------------------------------------------------------- #

    def get_channel_code(self,ch): return self._channel_codes[ch]
    def get_channel_phasor(self,ch): return self._channel_phasors[ch]
    def get_channel_pattern(self,ch): return self._channel_patterns[ch]

    def get_channel_seed(self,ch): return self._data['Channel'][ch]['Seed']
    def get_channel_phase(self,ch): return self._data['Channel'][ch]['Phase']
    def get_channel_antenna(self,ch): return self._data['Channel'][ch]['Antenna']
    def get_channel_amplitude(self,ch): return self._data['Channel'][ch]['Amplitude']
    def get_channel_stream_port(self,ch): return self._data['Channel'][ch]['StreamPort']

    # ======================================================================= #

    def set_gps_latitude(self,value): self._data.GlobalPosition.Latitude = value
    def set_gps_longitude(self,value): self._data.GlobalPosition.Longitude = value
    def set_gps_altitude(self,value): self._data.GlobalPosition.Altitude = value

    def set_code_type(self,value): self._data.Code.Type = value
    def set_code_length(self,value): self._data.Code.Length = value    
    def set_code_period(self,value): self._data.Code.Period = value
    
    def set_symbol_rate(self,value): self._data.Symbol.Rate = value
    def set_symbol_delay(self,value): self._data.Symbol.Delay = value
    def set_symbol_carrier(self,value): self._data.Symbol.Carrier = value
    def set_symbol_sync_second(self,value): self._data.Symbol.SyncSecond = value  

    def set_limit_amplitude(self,value): self._data['Limits']['Amplitude'] = value

    def set_control_mode(self,value): self._data.Control.Mode = value
    def set_control_rpc_host(self,value): self._data.Control.XmlRpcHost = value
    def set_control_rpc_port(self,value): self._data.Control.XmlRpcPort = value

    # ----------------------------------------------------------------------- #

    def set_usrp_route(self,value,mb): self._data['Usrp'][mb]['Route'] = value
    def set_usrp_address(self,value,mb): self._data['Usrp'][mb]['Address'] = value
    def set_usrp_interface(self,value,mb): self._data['Usrp'][mb]['Interface'] = value
    def set_usrp_time_source(self,value,mb): self._data['Usrp'][mb]['TimeSource'] = value
    def set_usrp_clock_source(self,value,mb): self._data['Usrp'][mb]['ClockSource'] = value

    # ----------------------------------------------------------------------- #

    def set_channel_seed(self,value,ch): self._data['Channel'][ch]['Seed'] = value
    def set_channel_phase(self,value,ch): self._data['Channel'][ch]['Phase'] = value
    def set_channel_antenna(self,value,ch): self._data['Channel'][ch]['Antenna'] = value
    def set_channel_amplitude(self,value,ch): self._data['Channel'][ch]['Amplitude'] = value
    def set_channel_stream_port(self,value,ch): self._data['Channel'][ch]['StreamPort'] = value

    # ======================================================================= #

    def _set_code_periods(self, sync_second, symbol_rate, code_type, code_length):
        """
        """
        value  = sync_second
        value *= symbol_rate
        code_periods = set()
        for i in range(1,int(numpy.sqrt(value))+1):
            div,mod = divmod(value,i)
            if mod == 0:
                code_periods |= {i,div}
        code_periods = sorted(code_periods)
        if code_type == 'complementary':
            code_periods = [code_period for code_period in code_periods if code_period%2 == 0]
        code_periods = numpy.int64(code_periods)
        code_periods = code_periods[code_periods>=code_length]
        code_periods = list(code_periods)
        self._code_periods = code_periods

    # ----------------------------------------------------------------------- #

    def get_symbol_delay_init(self):
        """
        """
        return self._symbol_delay_init

    def _set_symbol_delay_init(self, symbol_rate):
        """
        """
        self._symbol_delay_init = -self._symbol_delays[symbol_rate]

    # ----------------------------------------------------------------------- #

    def _set_symbol_taps(self, symbol_rate, interpolation):
        """
        """
        if symbol_rate >= 200000:
            taps = [1.0]
        else:
            rolloff = 0.518
            radix = int(numpy.ceil(numpy.log2(7.0*float(interpolation)+1.0)))
            sigma = rolloff/float(interpolation)/numpy.pi
            freqs = numpy.arange(-0.5,+0.5,1.0/2.0**radix,numpy.float32)
            freqs_pos = (numpy.copy(freqs)+0.5/float(interpolation))/sigma/numpy.sqrt(2.0)
            freqs_neg = (numpy.copy(freqs)-0.5/float(interpolation))/sigma/numpy.sqrt(2.0)
            idx_low = int(0.5*len(freqs))-int(3.5*float(interpolation))
            idx_upp = int(0.5*len(freqs))+int(3.5*float(interpolation))+1
            taps = scipy.special.erf(freqs_pos)-scipy.special.erf(freqs_neg)
            taps = numpy.fft.fftshift(numpy.fft.ifft(numpy.fft.fftshift(taps)))
            taps = taps.real[idx_low:idx_upp]*float(interpolation)/2.0
        taps = tuple(numpy.float64(taps).tolist())
        taps = numpy.float64(taps)
        self._symbol_taps = taps

    # ----------------------------------------------------------------------- #

    def _set_symbol_interpolation(self, symbol_rate, output_rate):
        """
        """
        self._symbol_interpolation  = output_rate
        self._symbol_interpolation /= symbol_rate  

    # ----------------------------------------------------------------------- #

    def _set_usrp_motherboards(self):
        """
        """
        self._usrp_motherboards = len(self._data.Usrp)

    # ----------------------------------------------------------------------- #
    
    def _set_usrp_sample_rate(self, symbol_rate):
        """
        """
        if symbol_rate < 200000:
            self._usrp_sample_rate = 200000
        else:
            self._usrp_sample_rate = symbol_rate
    
    # ----------------------------------------------------------------------- #

    def _set_usrp_channels(self, motherboards):
        """
        """
        channels = len(self._data.Channel)
        if not channels == motherboards:
            raise LookupError(
                "\n\tChannels 'count=%s' must be equal to USRP devices 'count=%s')." % (
                    channels,
                    motherboards,
                )
            )
        self._usrp_channels = channels

    # ----------------------------------------------------------------------- #

    def _set_channel_phasors(self, amplitudes, phases, channels):
        """
        """
        phasors = []
        for ch in xrange(channels):
            phasor  = phases[ch]*1.0j*numpy.pi/180.0
            phasor  = numpy.exp(phasor)
            phasor *= amplitudes[ch]
            phasor *= 1.0 # factor
            phasor  = numpy.complex64(phasor)
            phasors.append(phasor)
        self._channel_phasors = phasors

    # ----------------------------------------------------------------------- #

    def _set_channel_codes(self, channels, ctype, length, seeds):
        """
        """
        codes = []
        for c in xrange(channels):
            if ctype == 'cw':
                code = numpy.ones(length)
            if ctype == 'barker':
                if length == 2: code = numpy.float32([+1,-1])
                if length == 3: code = numpy.float32([+1,+1,-1])
                if length == 4: code = numpy.float32([+1,-1,+1,+1])
                if length == 5: code = numpy.float32([+1,+1,+1,-1,+1])
                if length == 7: code = numpy.float32([+1,+1,+1,-1,-1,+1,-1])
                if length == 11: code = numpy.float32([+1,+1,+1,-1,-1,-1,+1,-1,-1,+1,-1])
                if length == 13: code = numpy.float32([+1,+1,+1,+1,+1,-1,-1,+1,+1,-1,+1,-1,+1])
            if ctype == 'complementary':
                if length == 4:
                    code = numpy.float32([
                        [+1,+1,+1,-1],
                        [+1,+1,-1,+1],
                    ])
                if length == 8:
                    code = numpy.float32([
                        [+1,+1,+1,-1,+1,+1,-1,+1],
                        [+1,+1,+1,-1,-1,-1,+1,-1],
                    ])
                if length == 16:
                    code = numpy.float32([
                        [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1],
                        [+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
                    ])
                if length == 32:
                    code = numpy.float32([
                        [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
                        [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,-1,-1,-1,+1,-1,-1,+1,-1,+1,+1,+1,-1,-1,-1,+1,-1],
                    ])
            if ctype == 'pseudo':
                numpy.random.seed(seeds[c])
                code = numpy.random.random(length)
                code = numpy.exp(2.0*numpy.pi*1.0j*code)
                code = numpy.angle(code)
                code = -1.0*numpy.sign(code)
                code = code.real
                code = numpy.float32(code)
            codes.append(code)
        self._channel_codes = codes

    # ----------------------------------------------------------------------- #

    def _set_channel_patterns(self, channels, ctype, length, period, codes):
        """
        """
        patterns = []
        for c in xrange(channels):
            if ctype == 'complementary':
                pattern_a = numpy.zeros(period)
                pattern_b = numpy.zeros(period)
                pattern_a[0:length] = codes[c][0,:]
                pattern_b[0:length] = codes[c][1,:]
                pattern = numpy.append(
                    pattern_a,
                    pattern_b,
                )
            else:
                pattern = numpy.zeros(period)
                pattern[0:length] = codes[c]
            patterns.append(numpy.float32(pattern))
        self._channel_patterns = patterns
    
    # ======================================================================= #

    def load(self, filename):
        """
        """
        self._data = config.Config(file(filename,'r'))
        self.verify()

    # ----------------------------------------------------------------------- #

    def save(self, filename):
        """
        """
        self.verify()
        data = config.Config()
        data.GlobalPosition = config.Mapping()
        data.GlobalPosition.addMapping(key='Latitude',value=self.get_gps_latitude(),comment='')
        data.GlobalPosition.addMapping(key='Longitude',value=self.get_gps_longitude(),comment='')
        data.GlobalPosition.addMapping(key='Altitude',value=self.get_gps_altitude(),comment='')
        data.Code = config.Mapping()
        data.Code.addMapping(key='Type',value=self.get_code_type(),comment='')
        data.Code.addMapping(key='Length',value=self.get_code_length(),comment='')
        data.Code.addMapping(key='Period',value=self.get_code_period(),comment='')
        data.Limits = config.Mapping()
        data.Limits.addMapping(key='Amplitude',value=self.get_limit_amplitude(),comment='')
        data.Symbol = config.Mapping()
        data.Symbol.addMapping(key='Rate',value=self.get_symbol_rate(),comment='')
        data.Symbol.addMapping(key='Delay',value=self.get_symbol_delay(),comment='')
        data.Symbol.addMapping(key='Carrier',value=self.get_symbol_carrier(),comment='')
        data.Symbol.addMapping(key='SyncSecond',value=self.get_symbol_sync_second(),comment='')
        data.Control = config.Mapping()
        data.Control.addMapping(key='Mode',value=self.get_control_mode(),comment='')
        data.Control.addMapping(key='XmlRpcHost',value=self.get_control_rpc_host(),comment='')
        data.Control.addMapping(key='XmlRpcPort',value=self.get_control_rpc_port(),comment='')
        data.Control.addMapping(key='CpuCores',value=list(self.get_control_cpu_cores()),comment='')
        data.Usrp = config.Sequence()
        data.Channel = config.Sequence()
        for mb in xrange(self.get_usrp_motherboards()):
            data_usrp = config.Mapping()
            data_usrp.addMapping(key='Address',value=self.get_usrp_address(mb),comment='')
            data_usrp.addMapping(key='Interface',value=self.get_usrp_interface(mb),comment='')
            data_usrp.addMapping(key='ClockSource',value=self.get_usrp_clock_source(mb),comment='')
            data_usrp.addMapping(key='TimeSource',value=self.get_usrp_time_source(mb),comment='')    
            data_usrp.addMapping(key='Route',value=self.get_usrp_route(mb),comment='')
            data.Usrp.append(item=dict(data_usrp),comment='')
        for ch in xrange(self.get_usrp_channels()):
            data_channel = config.Mapping()
            data_channel.addMapping(key='Phase',value=self.get_channel_phase(ch),comment='')
            data_channel.addMapping(key='Seed',value=self.get_channel_seed(ch),comment='')
            data_channel.addMapping(key='Amplitude',value=self.get_channel_amplitude(ch),comment='')
            data_channel.addMapping(key='StreamPort',value=self.get_channel_stream_port(ch),comment='')
            data_channel.addMapping(key='Antenna',value=list(self.get_channel_antenna(ch)),comment='')
            data.Channel.append(item=dict(data_channel),comment='')            
        data.save(file(name=filename,mode='w'))

    # ----------------------------------------------------------------------- #
    
    def create(self, filename, channels):
        """
        """
        self._data = config.Config()
        self._data.GlobalPosition = self.__data_gps
        self._data.Code = self.__data_code
        self._data.Limits = self.__data_limits
        self._data.Symbol = self.__data_symbol
        self._data.Control = self.__data_control
        self._data.Usrp = config.Sequence()
        self._data.Channel = config.Sequence()
        for c in xrange(channels):
            data_usrp = config.Mapping()
            data_usrp.addMapping(key='Address',value='192.168.10.%s' % (c+2),comment='')
            data_usrp.addMapping(key='Interface',value=self.__USRP_IFACES[-1],comment='')
            data_usrp.addMapping(key='ClockSource',value='external',comment='')
            data_usrp.addMapping(key='TimeSource',value='external',comment='')    
            data_usrp.addMapping(key='Route',value='A:A',comment='')
            data_channel = config.Mapping()
            data_channel.addMapping(key='Seed',value=0,comment='')
            data_channel.addMapping(key='Phase',value=0.0,comment='')
            data_channel.addMapping(key='Amplitude',value=0.0,comment='')
            data_channel.addMapping(key='Antenna',value=[0.0,0.0,0.0],comment='')
            data_channel.addMapping(key='StreamPort',value=6000+c+1,comment='')
            self._data.Usrp.append(item=dict(data_usrp),comment='')
            self._data.Channel.append(item=dict(data_channel),comment='')
        self.save(filename)

    # ----------------------------------------------------------------------- #

    @staticmethod
    def edit(filename):
        """
        """
        os.system("nano %s" % filename)

    # ----------------------------------------------------------------------- #
    
    def verify(self):
        """
        """

        Verify.number(self.get_limit_amplitude(),'LimitAmplitude',float,"(",0.0,1.0,"]",[])

        Verify.number(self.get_gps_latitude(),'Latitude',float,"[",-90.0,+90.0,"]",[])
        Verify.number(self.get_gps_longitude(),'Longitude',float,"[",-180.0,+180.0,"]",[])
        Verify.number(self.get_gps_altitude(),'Altitude',float,"(","-inf","+inf",")",[])
        Verify.string(self.get_code_type(),'CodeType',['cw','mono','barker','pseudo','complementary'])
        
        if self.get_code_type() == 'cw':
            Verify.number(self.get_code_length(),'CodeLength',int,"[",1,"+inf",")",[])
        if self.get_code_type() == 'barker':
            Verify.number(self.get_code_length(),'CodeLength',int,"[",2,13,"]",[2,3,4,5,7,11,13])
        if self.get_code_type() == 'complementary':
            Verify.number(self.get_code_length(),'CodeLength',int,"[",2,32,"]",[2,4,8,16,32])
        if self.get_code_type() == 'pseudo':
            Verify.number(self.get_code_length(),'CodeLength',int,"[",1,"+inf",")",[])

        Verify.number(self.get_symbol_rate(),'SymbolRate',int,"[",200,5000000,"]",self._symbol_rates)
        Verify.number(self.get_symbol_carrier(),'SymbolCarrier',float,"[",0.0,"+inf",")",[])
        Verify.number(self.get_symbol_sync_second(),'SyncSecond',int,"[",1,60,"]",self._symbol_sync_seconds)

        self._set_usrp_motherboards()
        self._set_usrp_sample_rate(self.get_symbol_rate())
        self._set_usrp_channels(self.get_usrp_motherboards())
        
        self._set_symbol_interpolation(self.get_symbol_rate(),self.get_usrp_sample_rate())
        self._set_symbol_taps(self.get_symbol_rate(),self.get_symbol_interpolation())
        self._set_symbol_delay_init(self.get_symbol_rate())

        self._set_code_periods(self.get_symbol_sync_second(),self.get_symbol_rate(),self.get_code_type(),self.get_code_length())

        Verify.number(self.get_code_period(),'CodePeriod',int,"[",self.get_code_length(),"+inf",")",self.get_code_periods())
        Verify.number(self.get_symbol_delay(),'SymbolDelay',int,"[",-self.get_code_period(),+self.get_code_period(),"]",[])
        Verify.string(self.get_control_mode(),'Mode',['calibrate','simulate','operate'])
        Verify.ip(self.get_control_rpc_host(),"XmlRpcHost")
        Verify.number(self.get_control_rpc_port(),"XmlRpcPort",int,"[",1024,32768,")",[])
        Verify.struct(list(self.get_control_cpu_cores()),"CpuCores",list,None)

        for mb in xrange(self._usrp_motherboards):
            
            Verify.ip(self.get_usrp_address(mb),"Address[%s]" % (mb+1))
            Verify.string(self.get_usrp_interface(mb),"Interface[%s]" % (mb+1),self.__USRP_IFACES)
            Verify.string(self.get_usrp_route(mb),"Route[%s]" % (mb+1),['A:A','A:B','A:AB','A:BA'])
            Verify.string(self.get_usrp_clock_source(mb),"ClockSource[%s]" % (mb+1),['internal','external','mimo'])
            
            if self.get_usrp_clock_source(mb) == 'internal':
                Verify.string(self.get_usrp_time_source(mb),"TimeSource[%s]" % (mb+1),['none'])
            else:
                Verify.string(self.get_usrp_time_source(mb),"TimeSource[%s]" % (mb+1),[self.get_usrp_clock_source(mb)])

        for c in xrange(self._usrp_channels):
            
            Verify.number(self.get_channel_seed(c),"Seed[%s]" % (c+1),int,"[",0,'+inf',")",[])
            Verify.number(self.get_channel_phase(c),"Phase[%s]" % (c+1),float,"[",-180.0,+180.0,"]",[])
            Verify.number(self.get_channel_amplitude(c),"Amplitude[%s]" % (c+1),float,"[",0.0,self.get_limit_amplitude(),"]",[])
            Verify.number(self.get_channel_stream_port(c),"StreamPort[%s]" % (c+1),int,"[",1024,32768,")",[])
            Verify.struct(list(self.get_channel_antenna(c)),"Antenna[%s]" % (c+1),list,3)
            
            for i,item in enumerate(list(self.get_channel_antenna(c))):
            
                Verify.number(item,"Antenna[%s][%s]" % (c+1,i+1),float,"(","-inf","+inf",")",[])               

        self._set_channel_codes(self.get_usrp_channels(),self.get_code_type(),self.get_code_length(),self.get_channel_seeds())
        self._set_channel_phasors(self.get_channel_amplitudes(),self.get_channel_phases(),self.get_usrp_channels())
        self._set_channel_patterns(self.get_usrp_channels(),self.get_code_type(),self.get_code_length(),self.get_code_period(),self.get_channel_codes())

    # ======================================================================= #



if __name__ == "__main__":
    
    cfg = Config()
    cfg.load(filename='/home/radar/.simone-tx/cfg/main.cfg')

    plt.figure()
    plt.plot(cfg.get_symbol_taps())
    plt.show()
    print cfg.get_channel_code(1)
    print cfg.get_channel_pattern(1)

    #cfg.create(filename='/home/radar/Schreibtisch/cfg_test.cfg',channels=6)

    sys.exit()
