#!/usr/bin/env python



import os

activate_this = "%s/.simone-tx/bin/activate_this.py"%os.getenv('HOME')
execfile(activate_this, dict(__file__=activate_this))

import sys
import time
import numpy
import calendar
import terminaltables
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from datetime import datetime, timedelta
from colorclass import Color
from collections import OrderedDict
from terminaltables import DoubleTable



class LoggingStats(object):

    def __init__(self,log_dir):

        self.__T_DAYS = 7

        self.__set__log_dir(log_dir)
        self.__set__log_files(self.__log_dir)
        self.__set__log_dates(self.__log_files)
        self.__set__log_paths()
        self.__set__log_stats()
        self.__set__log_events()

        self.__set__log_figure()

    def __str__(self):

        table_data = []
        table_data.append(
            [
                '',
                "\033[1m%s\033[0m"%'POWERON',              
                "\033[1m%s\033[0m"%'START',
                "\033[1m%s\033[0m"%'RUN',
                "\033[1m%s\033[0m"%'STOP',
                "\033[1m%s\033[0m"%'FAIL',
                "\033[1m%s\033[0m"%'POWEROFF',
            ],
        )
        for stat in self.__stats:
            table_row = []
            table_row.append(Color("\033[1m%s\033[0m" % stat[0]))
            table_row.append(stat[1]['PowerOn'])
            table_row.append(stat[1]['Start'])
            table_row.append(stat[1]['Run'])
            table_row.append(stat[1]['Stop'])
            table_row.append(stat[1]['Fail'])
            table_row.append(stat[1]['PowerOff'])
            table_data.append(table_row)
        table = DoubleTable(table_data)
        table.inner_heading_row_border = True
        table.justify_columns[0] = 'center'
        table.justify_columns[1] = 'center'
        table.justify_columns[2] = 'center'
        table.justify_columns[3] = 'center'
        table.justify_columns[4] = 'center'
        table.justify_columns[5] = 'center'
        table.justify_columns[6] = 'center'
        print table.table
        return ""

    def __set__log_dir(self,log_dir):
        
        self.__log_dir = log_dir


    def __set__log_files(self,log_dir):
        
        log_files = os.walk(log_dir+"/.")
        log_files = next(log_files)[2]
        log_files = sorted(log_files)
        log_files = [s for s in log_files if "main.log" in s]
        log_files.append(log_files.pop(0))
        self.__log_files = log_files


    def __set__log_dates(self,log_files):

        cur_date = os.stat(self.__log_dir+'/main.log')
        cur_date = cur_date.st_ctime
        cur_date = str(datetime.fromtimestamp(cur_date))[0:10]

        log_dates = [s.replace('main.log','') for s in log_files]
        log_dates = [s.replace('.','') for s in log_dates]
        log_dates = list(filter(None,log_dates))
        log_dates.append(cur_date)
        self.__log_dates = log_dates


    def __set__log_paths(self):
        
        log_paths = [self.__log_dir+'/'+s for s in self.__log_files]
        self.__log_paths = log_paths


    def __set__log_stats(self):

        stats = []

        cmp_on = 'TRANSMITTER POWER ON!'
        cmp_off = 'TRANSMITTER POWER OFF!'
        cmp_run = 'TRANSMITTER RUNNING!'
        cmp_fail = 'TRANSMITTER FAILED!'
        cmp_stop = 'TRANSMITTER STOPPED!'
        cmp_start = 'TRANSMITTER STARTED!'
        
        for i,log_path in enumerate(self.__log_paths):
            stats.append([self.__log_dates[i]])
            cnt_on = 0; cnt_off = 0
            cnt_run = 0; cnt_fail = 0
            cnt_stop = 0; cnt_start = 0
            with open(log_path,'r') as file:
                for line in file:
                    if cmp_on in line: cnt_on += 1
                    if cmp_off in line: cnt_off += 1
                    if cmp_run in line: cnt_run += 1
                    if cmp_fail in line: cnt_fail += 1                    
                    if cmp_stop in line: cnt_stop += 1            
                    if cmp_start in line: cnt_start += 1
            stats[-1].append(
                {
                    'Run': cnt_run,'Fail': cnt_fail,
                    'Stop': cnt_stop,'Start': cnt_start,
                    'PowerOn': cnt_on,'PowerOff': cnt_off,
                }
            )
        self.__stats = stats


    def __set__log_events(self):

        self.__t_now = int(time.time())

        event = None
        events = []

        cmp_on = 'TRANSMITTER POWER ON!'
        cmp_off = 'TRANSMITTER POWER OFF!'
        cmp_run = 'TRANSMITTER RUNNING!'
        cmp_fail = 'TRANSMITTER FAILED!'
        cmp_stop = 'TRANSMITTER STOPPED!'
        cmp_start = 'TRANSMITTER STARTED!'
        
        for i,log_path in enumerate(self.__log_paths):
            with open(log_path,'r') as file:
                for line in file:
                    
                    if cmp_on in line:
                        if event != 'on':
                            tmp = line.split()
                            tmp = [tmp[0]+' '+tmp[1].split(",")[0],'on']
                            tmp.insert(1,calendar.timegm(time.strptime(tmp[0],'%Y-%m-%d %H:%M:%S')))
                            events.append(tmp)
                        event = 'on'
                    
                    if cmp_off in line:
                        if event != 'off':
                            tmp = line.split()
                            tmp = [tmp[0]+' '+tmp[1].split(",")[0],'off']
                            tmp.insert(1,calendar.timegm(time.strptime(tmp[0],'%Y-%m-%d %H:%M:%S')))
                            events.append(tmp)
                        event = 'off'
                    
                    if cmp_run in line:
                        if event != 'run':
                            tmp = line.split()
                            tmp = [tmp[0]+' '+tmp[1].split(",")[0],'run']
                            tmp.insert(1,calendar.timegm(time.strptime(tmp[0],'%Y-%m-%d %H:%M:%S')))
                            events.append(tmp)
                        event = 'run'
                    
                    if cmp_fail in line:
                        if event != 'fail':
                            tmp = line.split()
                            tmp = [tmp[0]+' '+tmp[1].split(",")[0],'fail']
                            tmp.insert(1,calendar.timegm(time.strptime(tmp[0],'%Y-%m-%d %H:%M:%S')))
                            events.append(tmp)
                        event = 'fail'
                    
                    if cmp_stop in line:
                        if event != 'stop':
                            tmp = line.split()
                            tmp = [tmp[0]+' '+tmp[1].split(",")[0],'stop']
                            tmp.insert(1,calendar.timegm(time.strptime(tmp[0],'%Y-%m-%d %H:%M:%S')))
                            events.append(tmp)
                        event = 'stop'
                    
                    if cmp_start in line:
                        if event != 'start':
                            tmp = line.split()
                            tmp = [tmp[0]+' '+tmp[1].split(",")[0],'start']
                            tmp.insert(1,calendar.timegm(time.strptime(tmp[0],'%Y-%m-%d %H:%M:%S')))
                            events.append(tmp)
                        event = 'start'

        # ------------------------------------------------------------------- #

        for i in xrange(0,len(events)-1):
            t_diff = events[i+1][1]-events[i][1]
            events[i].insert(2,t_diff)
        events[-1].insert(2,self.__t_now-events[-2][1])

        # ------------------------------------------------------------------- #

        self.__t_start  = events[0][1]-events[0][1]%86400
        self.__t_offset = events[0][1]-self.__t_start
        self.__t_stop   = self.__t_now-self.__t_now%86400+86400
        self.__t_show   = self.__t_stop-self.__t_start
        self.__t_days   = self.__t_show/86400
        self.__t_dates  = [
            (datetime.now()-timedelta(-i)).strftime('%Y-%m-%d')
            for i in xrange(-self.__t_days+1,1)
        ]
        self.__T_DATES  = [
            (datetime.now()-timedelta(-i)).strftime('%Y-%m-%d')
            for i in xrange(-self.__T_DAYS+1,1)
        ]

        # ------------------------------------------------------------------- #

        for i in xrange(0,len(events)):
            events[i].insert(2,self.__t_offset)
            self.__t_offset += events[i][3]

        # ------------------------------------------------------------------- #

        events = self.__get__events_offsets_negedge(events,'on','off',self.__t_now)
        events = self.__get__events_offsets_negedge(events,'off','on',self.__t_now)
        events = self.__get__events_offsets_negedge(events,'start','stop',self.__t_now)
        events = self.__get__events_offsets_negedge(events,'stop','start',self.__t_now)

        # ------------------------------------------------------------------- #

        event_offsets = numpy.zeros((6,len(events)))
        event_widths = numpy.zeros((6,len(events)))

        for i in xrange(0,len(events)):
            if events[i][4]=='on':
                event_offsets[5,i] = events[i][2]
                event_widths[5,i] = events[i][3]
            if events[i][4]=='start':
                event_offsets[4,i] = events[i][2]
                event_widths[4,i] = events[i][3]
            if events[i][4]=='run':
                event_offsets[3,i] = events[i][2]
                event_widths[3,i] = events[i][3]
            if events[i][4]=='fail':
                event_offsets[2,i] = events[i][2]
                event_widths[2,i] = events[i][3]
            if events[i][4]=='stop':
                event_offsets[1,i] = events[i][2]
                event_widths[1,i] = events[i][3]
            if events[i][4]=='off':
                event_offsets[0,i] = events[i][2]
                event_widths[0,i] = events[i][3]

        # ------------------------------------------------------------------- #

        self.__on_logic = self.__get__logic_levels(event_offsets[5])
        self.__off_logic = self.__get__logic_levels(event_offsets[0])
        self.__run_logic = self.__get__logic_levels(event_offsets[3])
        self.__fail_logic = self.__get__logic_levels(event_offsets[2])
        self.__stop_logic = self.__get__logic_levels(event_offsets[1])
        self.__start_logic = self.__get__logic_levels(event_offsets[4])

        # ------------------------------------------------------------------- #

        self.__on_offset = self.__get__logic_offsets(event_offsets[5],event_widths[5])
        self.__off_offset = self.__get__logic_offsets(event_offsets[0],event_widths[0])
        self.__run_offset = self.__get__logic_offsets(event_offsets[3],event_widths[3])
        self.__fail_offset = self.__get__logic_offsets(event_offsets[2],event_widths[2])
        self.__stop_offset = self.__get__logic_offsets(event_offsets[1],event_widths[1])
        self.__start_offset = self.__get__logic_offsets(event_offsets[4],event_widths[4])

        # ------------------------------------------------------------------- #

        self.__on_counts = self.__get__logic_counts(self.__on_logic,self.__on_offset)
        self.__off_counts = self.__get__logic_counts(self.__off_logic,self.__off_offset)
        self.__run_counts = self.__get__logic_counts(self.__run_logic,self.__run_offset)
        self.__fail_counts = self.__get__logic_counts(self.__fail_logic,self.__fail_offset)
        self.__stop_counts = self.__get__logic_counts(self.__stop_logic,self.__stop_offset)
        self.__start_counts = self.__get__logic_counts(self.__start_logic,self.__start_offset)       

        # ------------------------------------------------------------------- #


    # ----------------------------------------------------------------------- #

    def __set__xticks(self):

        self.__xticks = numpy.arange(0,self.__t_days+1)*86400
        for i in xrange(1,self.__T_DAYS-self.__t_days+1):
            self.__xticks = numpy.insert(self.__xticks,0,-1*i*86400)
        self.__xticks = self.__xticks[-self.__T_DAYS-1::]
        if self.__T_DAYS==1:        
            self.__xticks = numpy.arange(self.__xticks[0],self.__xticks[-1]+1,86400/24)

    # ----------------------------------------------------------------------- #

    def __get__events_offsets_negedge(self,events,high,low,until):
        
        flag = False
        widths = []
        for i in xrange(0,len(events)):
            if events[i][4]==high:
                flag = True
                if flag:
                    offset = events[i][2]
            if events[i][4]==low:
                if flag:
                    widths.append(events[i][2]-offset)
                    offset = 0
        try:
            idx = 0
            for event in events:
                if event[4]==high:
                    event[3] = widths[idx]
                    idx += 1
        except IndexError:
            last_idx = 0
            last_utc = None
            for i,event in enumerate(events):
                if event[4]==high:
                    last_idx = i
                    last_utc = event[1]
            events[last_idx][3] = until-last_utc
        
        return events

    # ----------------------------------------------------------------------- #

    def __get__logic_levels(self,events):

        levels = numpy.copy(events)
        levels[levels!=0.0] = 1.0
        levels = levels[levels!=0]
        levels = numpy.insert(levels,range(1,levels.shape[0]+1),0.0)

        return levels

    # ----------------------------------------------------------------------- #

    def __get__logic_offsets(self,events,widths):

        idx = numpy.argwhere(events!=0.0)
        idx = idx.flatten()

        widths_tmp = numpy.copy(widths)
        widths_tmp = widths_tmp[idx]
        widths_tmp[widths_tmp==0] = 10

        offsets = numpy.copy(events)
        offsets = offsets[offsets!=0]
        offsets = numpy.insert(offsets,range(1,offsets.shape[0]+1),0.0)        
        offsets[1::2] = offsets[0::2]+widths_tmp   

        return offsets

    # ----------------------------------------------------------------------- #

    def __get__logic_counts(self,levels,offsets):

        counts = levels*offsets
        counts = counts[counts!=0]
        counts_date = numpy.zeros(self.__T_DAYS)
        for count in counts:
            idx =  int(count)/86400+self.__T_DAYS-self.__t_days
            if idx>=0:
                counts_date[idx] += 1
        return counts_date

    # ----------------------------------------------------------------------- #

    def __set__log_figure(self):

        c = [
            'firebrick','indianred','red',
            'lime','limegreen','green'
        ]

        #print self.__xticks
        self.__set__xticks()

        xticklabels = ['%02d:00'%i for i in xrange(0,24)]
        xticklabels.append(xticklabels[0])        



        plt.style.use('dark_background')
        fig = plt.figure(figsize=(20,4))
        fig.suptitle('Runtime Statistics - %s'%datetime.fromtimestamp(self.__t_now), fontsize=16,fontweight='bold')


        axs = plt.subplot2grid((6,6),(2,0),rowspan=4,colspan=6)
        axs.set_xlim((self.__xticks[0],self.__xticks[-1]))
        axs.set_xticks(self.__xticks)
        axs.set_xticklabels(xticklabels)
        if self.__T_DAYS>=2:
            axs.xaxis.set_major_formatter(ticker.NullFormatter())
            axs.xaxis.set_minor_locator(ticker.FixedLocator(self.__xticks+86400/2))
            axs.xaxis.set_minor_formatter(ticker.FixedFormatter(self.__T_DATES))
        axs.set_ylim((-0.2,7.2))
        axs.set_yticks(numpy.arange(0.5,7.7,1.2))
        axs.set_yticklabels(['OFF','STOP','FAIL','RUN','START','ON'],fontsize=15,fontweight='bold')
        axs.fill_between(self.__on_offset,self.__on_logic+6.0,6.0,step="post",alpha=0.5,color=c[5])
        axs.fill_between(self.__off_offset,self.__off_logic+0.0,0.0,step="post",alpha=0.5,color=c[0])
        axs.fill_between(self.__run_offset,self.__run_logic+3.6,3.6,step="post",alpha=0.5,color=c[3])
        axs.fill_between(self.__fail_offset,self.__fail_logic+2.4,2.4,step="post",alpha=0.5,color=c[2])
        axs.fill_between(self.__stop_offset,self.__stop_logic+1.2,1.2,step="post",alpha=0.5,color=c[1])
        axs.fill_between(self.__start_offset,self.__start_logic+4.8,4.8,step="post",alpha=0.5,color=c[4])
        axs.step(self.__on_offset,self.__on_logic+6.0,linewidth=2.0,where='post',color=c[5])
        axs.step(self.__off_offset,self.__off_logic+0.0,linewidth=2.0,where='post',color=c[0])
        axs.step(self.__run_offset,self.__run_logic+3.6,linewidth=2.0,where='post',color=c[3])
        axs.step(self.__fail_offset,self.__fail_logic+2.4,linewidth=2.0,where='post',color=c[2])
        axs.step(self.__stop_offset,self.__stop_logic+1.2,linewidth=2.0,where='post',color=c[1])
        axs.step(self.__start_offset,self.__start_logic+4.8,linewidth=2.0,where='post',color=c[4])

        axs.grid(axis='x')

        axh = plt.subplot2grid((6,6),(0,0),rowspan=2,colspan=6)

        bar_on = axh.bar(numpy.arange(0,6*self.__T_DAYS)[0::6]+0.5,self.__on_counts,align='center',color=c[5],width=1,label='ON')
        bar_start = axh.bar(numpy.arange(0,6*self.__T_DAYS)[1::6]+0.5,self.__start_counts,align='center',color=c[4],width=1,label='START')
        bar_run = axh.bar(numpy.arange(0,6*self.__T_DAYS)[2::6]+0.5,self.__run_counts,align='center',color=c[3],width=1,label='RUN')
        bar_fail = axh.bar(numpy.arange(0,6*self.__T_DAYS)[3::6]+0.5,self.__fail_counts,align='center',color=c[2],width=1,label='FAIL')
        bar_stop = axh.bar(numpy.arange(0,6*self.__T_DAYS)[4::6]+0.5,self.__stop_counts,align='center',color=c[1],width=1,label='STOP')
        bar_off = axh.bar(numpy.arange(0,6*self.__T_DAYS)[5::6]+0.5,self.__off_counts,align='center',color=c[0],width=1,label='OFF')        

        axh.set_xlim((0,6*self.__T_DAYS))
        axh.set_xticks(numpy.arange(0,6*self.__T_DAYS,6))
        axh.set_xticklabels([])

        axh.legend(loc='upper left',bbox_to_anchor=(0.0,1.2),fancybox=True,shadow=True,ncol=6)
        axh.grid(axis='x')

        plt.savefig("%s/main.log.png"%self.__log_dir,format='png')
        plt.close()
        

        #plt.show()

        # ------------------------------------------------------------------- #



if __name__ == "__main__":

    log_stats = LoggingStats("/home/radar/.simone-tx/log")
    #sys.exit()
