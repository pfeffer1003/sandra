###############################################################################
#                                                                             #
#   PROJECT     :   SanDRA                                                    #
#   FILE        :   Barker.py                                                 #
#   AUTHOR      :   M.Eng. Nico Pfeffer                                       #
#   INSTITUTION :   Leibniz-Institute of Atmospheric Physics                  #
#   DEPARTMENT  :   Radar Remote Sensing                                      #
#   GROUP       :   Radio Science Group                                       #
#                                                                             #
###############################################################################

import os
import sys
import numpy

###############################################################################
'''
class Barker(object):

    #-------------------------------------------------------------------------#

    __LENGTHS = [2,3,4,5,7,11,13]

    #-------------------------------------------------------------------------#

    def __init__(self,length):
        
        """
        """
        
        self.set_delay(delay=0)
        self.set_phase(phase=0.0)
        self.set_length(length=length)
        self.set_period(period=length)
        self.set_amplitude(amplitude=0.0)

        self.__set_duty_cycle__(
            length=self.__length,
            period=self.__period,
        )
        self.__set_array__(
            delay=self.__delay,
            phase=self.__phase,
            length=self.__length,
            period=self.__period,
            amplitude=self.__amplitude,
        )

    #-------------------------------------------------------------------------#

    def get_delay(self,):
        """
        """
        return self.__delay

    def set_delay(self,delay):
        """
        """
        if not isinstance(delay,int):
            raise TypeError(
                "Attribute <delay> must be %s!"%int
            )
        self.__delay = delay
        try:
            self.__set_array__(
                delay=self.__delay,
                phase=self.__phase,
                length=self.__length,
                period=self.__period,
                amplitude=self.__amplitude,
            )
        except AttributeError:
            pass        

    delay = property(get_delay,set_delay)

    #-------------------------------------------------------------------------#

    def get_phase(self,):
        """
        """
        return self.__phase

    def set_phase(self,phase):
        """
        """
        if not isinstance(phase,float):
            raise TypeError(
                "Attribute <phase> must be %s!"%float
            )
        self.__phase = phase
        try:
            self.__set_array__(
                delay=self.__delay,
                phase=self.__phase,
                length=self.__length,
                period=self.__period,
                amplitude=self.__amplitude,
            )
        except AttributeError:
            pass

    phase = property(get_phase,set_phase)

    #-------------------------------------------------------------------------#

    def get_length(self,):
        """
        """
        return self.__length

    def set_length(self,length):
        """
        """
        if not isinstance(length,int):
            raise TypeError(
                "Attribute <length> must be %s!"%int
            )
        if not length in self.__LENGTHS:
            raise ValueError(
                "Attribute <length> must be in %s!"%self.__LENGTHS
            )
        self.__length = length
        try:
            if self.__length > self.__period:
                self.__period = self.__length
            self.__set_duty_cycle__(
                length=self.__length,
                period=self.__period,
            )
            self.__set_array__(
                delay=self.__delay,
                phase=self.__phase,
                length=self.__length,
                period=self.__period,
                amplitude=self.__amplitude,
            )
        except AttributeError:
            pass

    length = property(get_length,set_length)

    #-------------------------------------------------------------------------#

    def get_period(self,):
        """
        """
        return self.__period

    def set_period(self,period):
        """
        """
        if not isinstance(period,int):
            raise TypeError(
                "Attribute <period> must be %s!"%int
            )
        if period < self.__length:
            raise ValueError(
                "Attribute <period> must be >= <length>!"
            )
        self.__period = period
        try:
            self.__set_duty_cycle__(
                length=self.__length,
                period=self.__period,
            )
            self.__set_array__(
                delay=self.__delay,
                phase=self.__phase,
                length=self.__length,
                period=self.__period,
                amplitude=self.__amplitude,
            )
        except AttributeError:
            pass

    period = property(get_period,set_period)

    #-------------------------------------------------------------------------#

    def get_amplitude(self,):
        """
        """
        return self.__amplitude

    def set_amplitude(self,amplitude):
        """
        """
        if not isinstance(amplitude,float):
            raise TypeError(
                "Attribute <amplitude> must be %s!"%float
            )
        self.__amplitude = amplitude
        try:
            self.__set_array__(
                delay=self.__delay,
                phase=self.__phase,
                length=self.__length,
                period=self.__period,
                amplitude=self.__amplitude,
            )
        except AttributeError:
            pass

    amplitude = property(get_amplitude,set_amplitude)

    #-------------------------------------------------------------------------#

    def get_duty_cycle(self,):
        """
        """
        return self.__duty_cycle

    def __set_duty_cycle__(self,length,period):
        """
        """
        duty_cycle  = float(length)
        duty_cycle /= float(period)
        duty_cycle *= 100.0
        self.__duty_cycle = duty_cycle

    duty_cycle = property(get_duty_cycle)

    #-------------------------------------------------------------------------#

    def get_array(self,):
        """
        """
        return self.__array

    def __set_array__(self,delay,phase,length,period,amplitude):
        """
        """
        if length==2:
            array = [+1,-1]
        if length==3:
            array = [+1,+1,-1]
        if length==4:
            array = [+1,-1,+1,+1]
        if length==5:
            array = [+1,+1,+1,-1,+1]
        if length==7:
            array = [+1,+1,+1,-1,-1,+1,-1]
        if length==11:
            array = [+1,+1,+1,-1,-1,-1,+1,-1,-1,+1,-1]
        if length==13:
            array = [+1,+1,+1,+1,+1,-1,-1,+1,+1,-1,+1,-1,+1]
        zeros  = numpy.zeros(period-length,numpy.complex64)
        array  = numpy.complex64(array)
        array  = numpy.append(array,zeros)
        array *= amplitude
        array *= numpy.exp(1j*numpy.deg2rad(phase))
        array  = numpy.roll(array,delay)
        self.__array = array

    array = property(get_array)

    #-------------------------------------------------------------------------#

    def revert(self,):
        """
        """
        self.__array = self.__array[::-1]

    #-------------------------------------------------------------------------#

    def reset(self,):
        """
        """
        self.__set_duty_cycle__(
            length=self.__length,
            period=self.__length,
        )
        self.__set_array__(
            delay=0,
            phase=0.0,
            length=self.__length,
            period=self.__length,
            amplitude=0.0,
        )

    #-------------------------------------------------------------------------#

    def real(self,):
        """
        """
        return self.__array.real

    def imag(self,):
        """
        """
        return self.__array.imag

    #-------------------------------------------------------------------------#

###############################################################################

class Pulse(object):

    def __str__(self,): pass
    def __init__(self,): pass

###############################################################################

if __name__ == '__main__':

    #code = Barker(length=5)
    #code.length = 7
    #code.amplitude = 1.0
    #code.phase = 0.0
    #code.period = 5
    #code.delay = 3
    #print code.array

    sys.exit()

###############################################################################