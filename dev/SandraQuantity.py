###############################################################################
#
#   Project     :   SanDRA - [S]oftware [D]efined [R]adio in [A]tmospheric Physics
#   File        :   Quantity.py
#   Author      :   M.Eng. Nico Pfeffer
#   Institution :   Leibniz-Institute of Atmospheric Physics
#   Department  :   Radar Remote Sensing
#   Group       :   Radio Science Group
#
###############################################################################

class Quantity(object):

    ###########################################################################
    # DESCRIPTORS                                                             #
    ###########################################################################

    __DTYPES = [float,int,long]

    ###########################################################################
    # INITIALIZE                                                              #
    ###########################################################################

    def __init__(self,name,dtype,unit):
        """
        """
        self.__set__name(name)
        self.__set__unit(unit)
        self.__set__dtype(dtype)

    ###########################################################################
    # ATTRIBUTES                                                              #
    ###########################################################################

    def get_name(self,):
        """
        """
        return self.__name

    def __set__name(self,name):
        """
        """
        self.__name = name

    name = property(get_name)

    #-------------------------------------------------------------------------#


    def get_unit(self,):
        """
        """
        return self.__unit

    def __set__unit(self,unit):
        """
        """
        self.__unit = unit

    unit = property(get_unit)

    #-------------------------------------------------------------------------#


    def get_dtype(self):
        """
        """
        return self.__dtype

    def __set__dtype(self,dtype):
        """
        """
        self.__dtype = dtype

    dtype = property(get_dtype)

    #-------------------------------------------------------------------------#

    def get_lower(self,):
        """
        """
        return self.__lower

    def __set__lower(self,lower):
        """
        """
        self.__lower = lower

    lower = property(get_lower,set_lower)

    #-------------------------------------------------------------------------#

    def get_upper(self,):
        """
        """
        return self.__upper

    def __set__upper(self,):
        """
        """
        self.__upper = upper

    upper = property(get_upper,set_upper)

    #-------------------------------------------------------------------------#

    def get_interval(self,):
        """
        """
        return self.__interval

    def __set__interval(self,interval):
        """
        """
        self.__interval = interval

    ###########################################################################
    # METHODS                                                                 #
    ###########################################################################
    
    def verify(self,value):
        """
        """
        pass