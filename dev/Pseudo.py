###############################################################################
#
#   Project     :   SanDRA - [S]oftware [D]efined [R]adio in [A]tmospheric Physics
#   File        :   SandraTxWaveform.py
#   Author      :   M.Eng. Nico Pfeffer
#   Institution :   Leibniz-Institute of Atmospheric Physics
#   Department  :   Radar Remote Sensing
#   Group       :   Radio Science Group
#
###############################################################################

import os
import sys
import h5py
import numpy

###############################################################################

class Pseudo(object):

    #-------------------------------------------------------------------------#

    __LENGTHS = [2,3,4,5,7,11,13]

    #-------------------------------------------------------------------------#

    def __init__(self,length):
        """
        """
        self.set_phase(phase=0.0)
        self.set_amplitude(amplitude=0.0)
        self.set_length(length=length)

        self.__set_array__(
            length=self.__length,
            phase=self.__phase,
            amplitude=self.__amplitude,
        )

    #-------------------------------------------------------------------------#

    def get_phase(self,):
        """
        """
        return self.__phase

    def set_phase(self,phase):
        """
        """
        if not isinstance(phase,float):
            raise TypeError("Attribute <phase> must be %s!"%float)
        self.__phase = phase
        try:
            self.__set_array__(
                length=self.__length,
                phase=self.__phase,
                amplitude=self.__amplitude,
            )
        except AttributeError:
            pass

    phase = property(get_phase,set_phase)

    #-------------------------------------------------------------------------#

    def get_length(self,):
        """
        """
        return self.__length

    def set_length(self,length):
        """
        """
        if not isinstance(length,int):
            raise TypeError("Attribute <length> must be %s!"%int)
        if not length in self.__LENGTHS:
            raise ValueError("Attribute <length> must be in %s!"%self.__LENGTHS)
        self.__length = length
        try:
            self.__set_array__(
                length=self.__length,
                phase=self.__phase,
                amplitude=self.__amplitude,
            )
        except AttributeError:
            pass

    length = property(get_length,set_length)

    #-------------------------------------------------------------------------#

    def get_amplitude(self,):
        """
        """
        return self.__amplitude

    def set_amplitude(self,amplitude):
        """
        """
        if not isinstance(amplitude,float):
            raise TypeError("Attribute <amplitude> must be %s!"%float)
        self.__amplitude = amplitude
        try:
            self.__set_array__(self.__length,self.__phase,self.__amplitude)
        except AttributeError:
            pass

    amplitude = property(get_amplitude,set_amplitude)

    #-------------------------------------------------------------------------#

    def get_array(self,):
        """
        """
        return self.__array

    def __set_array__(self,length,phase,amplitude):
        """
        """
        if length==2:
            array = [+1,-1]
        if length==3:
            array = [+1,+1,-1]
        if length==4:
            array = [+1,-1,+1,+1]
        if length==5:
            array = [+1,+1,+1,-1,+1]
        if length==7:
            array = [+1,+1,+1,-1,-1,+1,-1]
        if length==11:
            array = [+1,+1,+1,-1,-1,-1,+1,-1,-1,+1,-1]
        if length==13:
            array = [+1,+1,+1,+1,+1,-1,-1,+1,+1,-1,+1,-1,+1]
        array  = numpy.complex64(array)
        array *= amplitude
        array *= numpy.exp(1j*numpy.deg2rad(phase))
        self.__array = array

    array = property(get_array)

    #-------------------------------------------------------------------------#

    def revert(self,):
        """
        """
        self.__array = self.__array[::-1]

    #-------------------------------------------------------------------------#

    def reset(self,):
        """
        """
        self.__set_array__(self.__length,0.0,1.0)

    #-------------------------------------------------------------------------#

###############################################################################


def get_pseudo(length,seed):
    numpy.random.seed(seed)
    code = numpy.random.random(length)
    code = numpy.exp(2.0*numpy.pi*1.0j*code)
    code = numpy.angle(code)
    code = -1.0*numpy.sign(code)
    code = code.real
    code = numpy.float32(code)
    return code




if __name__ == '__main__':


    positions = [
        [0.0, 25.0, 0.0],
        [23.776, 7.725, 0.0],
        [14.695, -20.225, 0.0],
        [-14.695, -20.225, 0.0],
        [-23.776, 7.725, 0.0],
        [0.0, 0.0, 0.0],
    ]
    positions = numpy.asarray(positions)
    positions = numpy.float32(positions)


    codes = []
    seeds = [5,20,608,1686,2755,4972]
    length=1000
    for seed in seeds:
        code = get_pseudo(length=length,seed=seed)
        code[code==-1.0] = 0.0
        codes.append(code)
    codes = numpy.asarray(codes)
    codes = numpy.int32(codes)


    print codes.shape
    print positions.shape

    f = h5py.File('kborn_codes_positions.hdf5','w')

    dt_codes = h5py.vlen_dtype(numpy.dtype('int32'))
    ds_codes = f.create_dataset(name='codes',data=codes)

    dt_positions = h5py.vlen_dtype(numpy.dtype('float32'))
    ds_positions = f.create_dataset(name='positions',data=positions)



    #dset_codes = f.create_dataset("codes",codes)
    #dset_positions = f.create_dataset("positions,positions")

    f.close()


    f = h5py.File('kborn_codes_positions.hdf5','r')
    codes = f['codes'][:]
    positions = f['positions'][:]
    f.close()

    print codes.shape       # (channels,ranges)
    print positions.shape   # (channels,positions)


    sys.exit()

###############################################################################