#!/bin/sh

###############################################################################
#                                                                             #
# FILE          :       install-virtualenv                                    #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

sudo -u $USER virtualenv --python=python2.7 $HOME/.sandra

###############################################################################

sudo -u $USER $HOME/.sandra/bin/pip2 install docopt==0.6.2
sudo -u $USER $HOME/.sandra/bin/pip2 install ipython==5.8.0
sudo -u $USER $HOME/.sandra/bin/pip2 install colorclass
sudo -u $USER $HOME/.sandra/bin/pip2 install numpy==1.14.3
sudo -u $USER $HOME/.sandra/bin/pip2 install scipy==1.1.0
sudo -u $USER $HOME/.sandra/bin/pip2 install config==0.4.2
sudo -u $USER $HOME/.sandra/bin/pip2 install colorlog
sudo -u $USER $HOME/.sandra/bin/pip2 install watchdog==0.8.2
sudo -u $USER $HOME/.sandra/bin/pip2 install digital-rf==2.6.1
sudo -u $USER $HOME/.sandra/bin/pip2 install matplotlib==2.0.2
sudo -u $USER $HOME/.sandra/bin/pip2 install terminaltables==3.1.0
sudo -u $USER $HOME/.sandra/bin/pip2 install python-intervals==1.8.0
sudo -u $USER $HOME/.sandra/bin/pip2 install IPy==1.0
sudo -u $USER $HOME/.sandra/bin/pip2 install zmq
sudo -u $USER $HOME/.sandra/bin/pip2 install engfmt

###############################################################################

sudo -u $USER cp -R /usr/lib/wx/                                    $HOME/.sandra/lib/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/wx-3.0-gtk3/   $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/wx.pth         $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/PyQt4/         $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/gtk-2.0/       $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/glib/          $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/gobject/       $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/pygtk.py       $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/pygtk.pth      $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/sip*           $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/lib/python2.7/dist-packages/cairo/         $HOME/.sandra/lib/python2.7/site-packages/.

###############################################################################

sudo -u $USER cp -R /usr/local/lib/python2.7/dist-packages/pkgconfig      $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/local/lib/python2.7/dist-packages/gnuradio       $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/local/lib/python2.7/dist-packages/grc_gnuradio   $HOME/.sandra/lib/python2.7/site-packages/.
sudo -u $USER cp -R /usr/local/lib/python2.7/dist-packages/pmt            $HOME/.sandra/lib/python2.7/site-packages/.

###############################################################################

sudo -u $USER mkdir -p $HOME/.sandra/cfg/tx
sudo -u $USER mkdir -p $HOME/.sandra/cfg/rx
sudo -u $USER mkdir -p $HOME/.sandra/cfg/trx

###############################################################################

sudo -u $USER mkdir -p $HOME/.sandra/log/tx
sudo -u $USER mkdir -p $HOME/.sandra/log/rx
sudo -u $USER mkdir -p $HOME/.sandra/log/trx

###############################################################################