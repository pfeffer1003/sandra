# this module will be imported in the into your flowgraph

import numpy

def get_pattern(length, period, codetype, seed):
    """	
    """
    if codetype == "cw":
        code = numpy.ones(length)
        code = numpy.float32(code)
        pattern = numpy.zeros(period,numpy.float32)
        pattern[0:length] = code
    if codetype == "barker":
        if length == 2: code = numpy.float32([+1,-1])
        if length == 3: code = numpy.float32([+1,+1,-1])
        if length == 4: code = numpy.float32([+1,-1,+1,+1])
        if length == 5: code = numpy.float32([+1,+1,+1,-1,+1])
        if length == 7: code = numpy.float32([+1,+1,+1,-1,-1,+1,-1])
        if length == 11: code = numpy.float32([+1,+1,+1,-1,-1,-1,+1,-1,-1,+1,-1])
        if length == 13: code = numpy.float32([+1,+1,+1,+1,+1,-1,-1,+1,+1,-1,+1,-1,+1])
        pattern = numpy.zeros(period,numpy.float32)
        pattern[0:length] = code
    if codetype == "complementary":
        if length == 4:
            code = numpy.float32([
                        [+1,+1,+1,-1],
                        [+1,+1,-1,+1],
            ])
        if length == 8:
            code = numpy.float32([
                [+1,+1,+1,-1,+1,+1,-1,+1],
                [+1,+1,+1,-1,-1,-1,+1,-1],
            ])
        if length == 16:
            code = numpy.float32([
                [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1],
                [+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
            ])
        if length == 32:
            code = numpy.float32([
                [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
                [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,-1,-1,-1,+1,-1,-1,+1,-1,+1,+1,+1,-1,-1,-1,+1,-1],
            ])
        pattern = numpy.zeros((2,period),numpy.float32)
        pattern[:,0:length] = code
        pattern = pattern.flatten()
    if codetype == "pseudo":
        numpy.random.seed(seed)
        code = numpy.random.random(length)
        code = numpy.exp(2.0*numpy.pi*1.0j*code)
        code = numpy.angle(code)
        code = -1.0*numpy.sign(code)
        code = code.real
        code = numpy.float32(code)
        pattern = numpy.zeros(period,numpy.float32)
        pattern[0:length] = code
    pattern = numpy.complex64(pattern)
    return pattern
