#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Tx Sim
# Author: Kiara Chau
# Generated: Wed Jul 21 09:06:30 2021
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import zeromq
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import kcodes  # embedded python module
import numpy
import pmt
import sys
from gnuradio import qtgui


class tx_sim(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Tx Sim")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Tx Sim")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "tx_sim")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.seed = seed = 0
        self.samplerate = samplerate = 100000.0
        self.phaseamplifier_2 = phaseamplifier_2 = 0.0
        self.phaseamplifier_1 = phaseamplifier_1 = 0.0
        self.phaseamplifier_0 = phaseamplifier_0 = 0.0
        self.phase_2 = phase_2 = 0.0
        self.phase_1 = phase_1 = 0.0
        self.phase_0 = phase_0 = 0.0
        self.period = period = 100
        self.noise_power = noise_power = 0.0
        self.magnitudeamplifier_2 = magnitudeamplifier_2 = 0.0
        self.magnitudeamplifier_1 = magnitudeamplifier_1 = 0.0
        self.magnitudeamplifier_0 = magnitudeamplifier_0 = 0.0
        self.magnitude_2 = magnitude_2 = 1.0
        self.magnitude_1 = magnitude_1 = 1.0
        self.magnitude_0 = magnitude_0 = 1.0
        self.length = length = 13
        self.delay = delay = 100
        self.cpu = cpu = 0
        self.codetype = codetype = 'pseudo'

        ##################################################
        # Blocks
        ##################################################
        self._phaseamplifier_2_range = Range(-15, 15, 0.5, 0.0, 200)
        self._phaseamplifier_2_win = RangeWidget(self._phaseamplifier_2_range, self.set_phaseamplifier_2, 'arg(2)_amp', "counter_slider", float)
        self.top_grid_layout.addWidget(self._phaseamplifier_2_win, 6,1,1,1)
        self._phaseamplifier_1_range = Range(-15, 15, 0.5, 0.0, 200)
        self._phaseamplifier_1_win = RangeWidget(self._phaseamplifier_1_range, self.set_phaseamplifier_1, 'arg(1)_amp', "counter_slider", float)
        self.top_grid_layout.addWidget(self._phaseamplifier_1_win, 4,1,1,1)
        self._phaseamplifier_0_range = Range(-15, 15, 0.5, 0.0, 200)
        self._phaseamplifier_0_win = RangeWidget(self._phaseamplifier_0_range, self.set_phaseamplifier_0, 'arg(0)_amp', "counter_slider", float)
        self.top_grid_layout.addWidget(self._phaseamplifier_0_win, 2,1,1,1)
        self._phase_2_range = Range(-180, 180, 1.0, 0.0, 100)
        self._phase_2_win = RangeWidget(self._phase_2_range, self.set_phase_2, 'arg(2)', "counter_slider", float)
        self.top_grid_layout.addWidget(self._phase_2_win, 6,0,1,1)
        self._phase_1_range = Range(-180, 180, 1.0, 0.0, 100)
        self._phase_1_win = RangeWidget(self._phase_1_range, self.set_phase_1, 'arg(1)', "counter_slider", float)
        self.top_grid_layout.addWidget(self._phase_1_win, 4,0,1,1)
        self._phase_0_range = Range(-180, 180, 1.0, 0.0, 100)
        self._phase_0_win = RangeWidget(self._phase_0_range, self.set_phase_0, 'arg(0)', "counter_slider", float)
        self.top_grid_layout.addWidget(self._phase_0_win, 2,0,1,1)
        self._noise_power_range = Range(0.0, 1.0, 0.001, 0.0, 50)
        self._noise_power_win = RangeWidget(self._noise_power_range, self.set_noise_power, 'Noise Power', "counter_slider", float)
        self.top_grid_layout.addWidget(self._noise_power_win, 0,0,2,1)
        self._magnitudeamplifier_2_range = Range(-0.1, 0.1, 0.01, 0.0, 200)
        self._magnitudeamplifier_2_win = RangeWidget(self._magnitudeamplifier_2_range, self.set_magnitudeamplifier_2, 'abs(2)_amp', "counter_slider", float)
        self.top_grid_layout.addWidget(self._magnitudeamplifier_2_win, 5,1,1,1)
        self._magnitudeamplifier_1_range = Range(-0.1, 0.1, 0.01, 0.0, 200)
        self._magnitudeamplifier_1_win = RangeWidget(self._magnitudeamplifier_1_range, self.set_magnitudeamplifier_1, 'abs(1)_amp', "counter_slider", float)
        self.top_grid_layout.addWidget(self._magnitudeamplifier_1_win, 3,1,1,1)
        self._magnitudeamplifier_0_range = Range(-0.1, 0.1, 0.01, 0.0, 200)
        self._magnitudeamplifier_0_win = RangeWidget(self._magnitudeamplifier_0_range, self.set_magnitudeamplifier_0, 'abs(0)_amp', "counter_slider", float)
        self.top_grid_layout.addWidget(self._magnitudeamplifier_0_win, 1,1,1,1)
        self._magnitude_2_range = Range(0, 1, 0.01, 1.0, 200)
        self._magnitude_2_win = RangeWidget(self._magnitude_2_range, self.set_magnitude_2, 'abs(2)', "counter_slider", float)
        self.top_grid_layout.addWidget(self._magnitude_2_win, 5, 0, 1,1)
        self._magnitude_1_range = Range(0.0, 1.0, 0.01, 1.0, 200)
        self._magnitude_1_win = RangeWidget(self._magnitude_1_range, self.set_magnitude_1, 'abs(1)', "counter_slider", float)
        self.top_grid_layout.addWidget(self._magnitude_1_win, 3, 0, 1,1)
        self._magnitude_0_range = Range(0, 1, 0.01, 1.0, 200)
        self._magnitude_0_win = RangeWidget(self._magnitude_0_range, self.set_magnitude_0, 'abs(0)', "counter_slider", float)
        self.top_grid_layout.addWidget(self._magnitude_0_win, 1, 0, 1,1)
        self._delay_range = Range(0, 1000, 1, 100, 50)
        self._delay_win = RangeWidget(self._delay_range, self.set_delay, 'Delay', "counter_slider", int)
        self.top_grid_layout.addWidget(self._delay_win, 0,1,2,1)
        self.zeromq_pub_sink_0 = zeromq.pub_sink(gr.sizeof_gr_complex, 3, 'tcp://127.0.0.1:7000', 100, True, -1)
        (self.zeromq_pub_sink_0).set_processor_affinity([0])
        self.interp_fir_filter_xxx_0_1 = filter.interp_fir_filter_ccf(1, ([1.0]))
        self.interp_fir_filter_xxx_0_1.declare_sample_delay(0)
        self.interp_fir_filter_xxx_0_0 = filter.interp_fir_filter_ccf(1, ([1.0]))
        self.interp_fir_filter_xxx_0_0.declare_sample_delay(0)
        self.interp_fir_filter_xxx_0 = filter.interp_fir_filter_ccf(1, ([1.0]))
        self.interp_fir_filter_xxx_0.declare_sample_delay(0)
        self.constant_2 = blocks.multiply_const_vcc(((magnitude_2+magnitudeamplifier_2)*numpy.exp(1j*numpy.deg2rad(phase_2 + phaseamplifier_2)), ))
        (self.constant_2).set_processor_affinity([0])
        self.constant_1 = blocks.multiply_const_vcc(((magnitude_1+magnitudeamplifier_1)*numpy.exp(1j*numpy.deg2rad(phase_1 + phaseamplifier_1)), ))
        (self.constant_1).set_processor_affinity([0])
        self.constant_0 = blocks.multiply_const_vcc(((magnitude_0+magnitudeamplifier_0)*numpy.exp(1j*numpy.deg2rad(phase_0 + phaseamplifier_0)), ))
        (self.constant_0).set_processor_affinity([0])
        self.blocks_vector_source_x_0_0_0 = blocks.vector_source_c(tuple(kcodes.get_pattern(length,period,codetype,seed).tolist()), True, 1, [])
        (self.blocks_vector_source_x_0_0_0).set_processor_affinity([0])
        self.blocks_vector_source_x_0_0 = blocks.vector_source_c(tuple(kcodes.get_pattern(length,period,codetype,seed).tolist()), True, 1, [])
        (self.blocks_vector_source_x_0_0).set_processor_affinity([0])
        self.blocks_vector_source_x_0 = blocks.vector_source_c(tuple(kcodes.get_pattern(length,period,codetype,seed).tolist()), True, 1, [])
        (self.blocks_vector_source_x_0).set_processor_affinity([0])
        self.blocks_throttle_0_0_0 = blocks.throttle(gr.sizeof_gr_complex*1, samplerate,True)
        (self.blocks_throttle_0_0_0).set_processor_affinity([0])
        self.blocks_throttle_0_0 = blocks.throttle(gr.sizeof_gr_complex*1, samplerate,True)
        (self.blocks_throttle_0_0).set_processor_affinity([0])
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samplerate,True)
        (self.blocks_throttle_0).set_processor_affinity([0])
        self.blocks_tags_strobe_0 = blocks.tags_strobe(gr.sizeof_gr_complex*1, pmt.intern("value0"), 100, pmt.intern("key0"))
        (self.blocks_tags_strobe_0).set_processor_affinity([0])
        self.blocks_streams_to_vector_0 = blocks.streams_to_vector(gr.sizeof_gr_complex*1, 3)
        (self.blocks_streams_to_vector_0).set_processor_affinity([0])
        self.blocks_null_sink_0_0_1 = blocks.null_sink(gr.sizeof_gr_complex*1)
        (self.blocks_null_sink_0_0_1).set_processor_affinity([0])
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_gr_complex*1)
        (self.blocks_null_sink_0_0).set_processor_affinity([0])
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_gr_complex*1)
        (self.blocks_null_sink_0).set_processor_affinity([0])
        self.blocks_delay_0_0_0_0 = blocks.delay(gr.sizeof_gr_complex*1, delay)
        (self.blocks_delay_0_0_0_0).set_processor_affinity([0])
        self.blocks_delay_0_0_0 = blocks.delay(gr.sizeof_gr_complex*1, delay)
        (self.blocks_delay_0_0_0).set_processor_affinity([0])
        self.blocks_delay_0_0 = blocks.delay(gr.sizeof_gr_complex*1, delay)
        (self.blocks_delay_0_0).set_processor_affinity([0])
        self.blocks_add_xx_0_1_1 = blocks.add_vcc(1)
        (self.blocks_add_xx_0_1_1).set_processor_affinity([0])
        self.blocks_add_xx_0_1_0 = blocks.add_vcc(1)
        (self.blocks_add_xx_0_1_0).set_processor_affinity([0])
        self.blocks_add_xx_0_1 = blocks.add_vcc(1)
        (self.blocks_add_xx_0_1).set_processor_affinity([0])
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        (self.blocks_add_xx_0).set_processor_affinity([0])
        self.analog_noise_source_x_0_0_0 = analog.noise_source_c(analog.GR_GAUSSIAN, noise_power, seed+2)
        (self.analog_noise_source_x_0_0_0).set_processor_affinity([0])
        self.analog_noise_source_x_0_0 = analog.noise_source_c(analog.GR_GAUSSIAN, noise_power, seed+1)
        (self.analog_noise_source_x_0_0).set_processor_affinity([0])
        self.analog_noise_source_x_0 = analog.noise_source_c(analog.GR_GAUSSIAN, noise_power, seed)
        (self.analog_noise_source_x_0).set_processor_affinity([0])

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_noise_source_x_0, 0), (self.blocks_add_xx_0_1, 1))
        self.connect((self.analog_noise_source_x_0_0, 0), (self.blocks_add_xx_0_1_0, 1))
        self.connect((self.analog_noise_source_x_0_0_0, 0), (self.blocks_add_xx_0_1_1, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.constant_0, 0))
        self.connect((self.blocks_add_xx_0_1, 0), (self.blocks_streams_to_vector_0, 0))
        self.connect((self.blocks_add_xx_0_1, 0), (self.interp_fir_filter_xxx_0, 0))
        self.connect((self.blocks_add_xx_0_1_0, 0), (self.blocks_streams_to_vector_0, 1))
        self.connect((self.blocks_add_xx_0_1_0, 0), (self.interp_fir_filter_xxx_0_0, 0))
        self.connect((self.blocks_add_xx_0_1_1, 0), (self.blocks_streams_to_vector_0, 2))
        self.connect((self.blocks_add_xx_0_1_1, 0), (self.interp_fir_filter_xxx_0_1, 0))
        self.connect((self.blocks_delay_0_0, 0), (self.blocks_add_xx_0_1, 0))
        self.connect((self.blocks_delay_0_0_0, 0), (self.blocks_add_xx_0_1_0, 0))
        self.connect((self.blocks_delay_0_0_0_0, 0), (self.blocks_add_xx_0_1_1, 0))
        self.connect((self.blocks_streams_to_vector_0, 0), (self.zeromq_pub_sink_0, 0))
        self.connect((self.blocks_tags_strobe_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.blocks_throttle_0_0, 0), (self.blocks_null_sink_0_0, 0))
        self.connect((self.blocks_throttle_0_0_0, 0), (self.blocks_null_sink_0_0_1, 0))
        self.connect((self.blocks_vector_source_x_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.blocks_vector_source_x_0_0, 0), (self.constant_1, 0))
        self.connect((self.blocks_vector_source_x_0_0_0, 0), (self.constant_2, 0))
        self.connect((self.constant_0, 0), (self.blocks_delay_0_0, 0))
        self.connect((self.constant_1, 0), (self.blocks_delay_0_0_0, 0))
        self.connect((self.constant_2, 0), (self.blocks_delay_0_0_0_0, 0))
        self.connect((self.interp_fir_filter_xxx_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.interp_fir_filter_xxx_0_0, 0), (self.blocks_throttle_0_0, 0))
        self.connect((self.interp_fir_filter_xxx_0_1, 0), (self.blocks_throttle_0_0_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "tx_sim")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_seed(self):
        return self.seed

    def set_seed(self, seed):
        self.seed = seed
        self.blocks_vector_source_x_0_0_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])
        self.blocks_vector_source_x_0_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])
        self.blocks_vector_source_x_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])

    def get_samplerate(self):
        return self.samplerate

    def set_samplerate(self, samplerate):
        self.samplerate = samplerate
        self.blocks_throttle_0_0_0.set_sample_rate(self.samplerate)
        self.blocks_throttle_0_0.set_sample_rate(self.samplerate)
        self.blocks_throttle_0.set_sample_rate(self.samplerate)

    def get_phaseamplifier_2(self):
        return self.phaseamplifier_2

    def set_phaseamplifier_2(self, phaseamplifier_2):
        self.phaseamplifier_2 = phaseamplifier_2
        self.constant_2.set_k(((self.magnitude_2+self.magnitudeamplifier_2)*numpy.exp(1j*numpy.deg2rad(self.phase_2 + self.phaseamplifier_2)), ))

    def get_phaseamplifier_1(self):
        return self.phaseamplifier_1

    def set_phaseamplifier_1(self, phaseamplifier_1):
        self.phaseamplifier_1 = phaseamplifier_1
        self.constant_1.set_k(((self.magnitude_1+self.magnitudeamplifier_1)*numpy.exp(1j*numpy.deg2rad(self.phase_1 + self.phaseamplifier_1)), ))

    def get_phaseamplifier_0(self):
        return self.phaseamplifier_0

    def set_phaseamplifier_0(self, phaseamplifier_0):
        self.phaseamplifier_0 = phaseamplifier_0
        self.constant_0.set_k(((self.magnitude_0+self.magnitudeamplifier_0)*numpy.exp(1j*numpy.deg2rad(self.phase_0 + self.phaseamplifier_0)), ))

    def get_phase_2(self):
        return self.phase_2

    def set_phase_2(self, phase_2):
        self.phase_2 = phase_2
        self.constant_2.set_k(((self.magnitude_2+self.magnitudeamplifier_2)*numpy.exp(1j*numpy.deg2rad(self.phase_2 + self.phaseamplifier_2)), ))

    def get_phase_1(self):
        return self.phase_1

    def set_phase_1(self, phase_1):
        self.phase_1 = phase_1
        self.constant_1.set_k(((self.magnitude_1+self.magnitudeamplifier_1)*numpy.exp(1j*numpy.deg2rad(self.phase_1 + self.phaseamplifier_1)), ))

    def get_phase_0(self):
        return self.phase_0

    def set_phase_0(self, phase_0):
        self.phase_0 = phase_0
        self.constant_0.set_k(((self.magnitude_0+self.magnitudeamplifier_0)*numpy.exp(1j*numpy.deg2rad(self.phase_0 + self.phaseamplifier_0)), ))

    def get_period(self):
        return self.period

    def set_period(self, period):
        self.period = period
        self.blocks_vector_source_x_0_0_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])
        self.blocks_vector_source_x_0_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])
        self.blocks_vector_source_x_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])

    def get_noise_power(self):
        return self.noise_power

    def set_noise_power(self, noise_power):
        self.noise_power = noise_power
        self.analog_noise_source_x_0_0_0.set_amplitude(self.noise_power)
        self.analog_noise_source_x_0_0.set_amplitude(self.noise_power)
        self.analog_noise_source_x_0.set_amplitude(self.noise_power)

    def get_magnitudeamplifier_2(self):
        return self.magnitudeamplifier_2

    def set_magnitudeamplifier_2(self, magnitudeamplifier_2):
        self.magnitudeamplifier_2 = magnitudeamplifier_2
        self.constant_2.set_k(((self.magnitude_2+self.magnitudeamplifier_2)*numpy.exp(1j*numpy.deg2rad(self.phase_2 + self.phaseamplifier_2)), ))

    def get_magnitudeamplifier_1(self):
        return self.magnitudeamplifier_1

    def set_magnitudeamplifier_1(self, magnitudeamplifier_1):
        self.magnitudeamplifier_1 = magnitudeamplifier_1
        self.constant_1.set_k(((self.magnitude_1+self.magnitudeamplifier_1)*numpy.exp(1j*numpy.deg2rad(self.phase_1 + self.phaseamplifier_1)), ))

    def get_magnitudeamplifier_0(self):
        return self.magnitudeamplifier_0

    def set_magnitudeamplifier_0(self, magnitudeamplifier_0):
        self.magnitudeamplifier_0 = magnitudeamplifier_0
        self.constant_0.set_k(((self.magnitude_0+self.magnitudeamplifier_0)*numpy.exp(1j*numpy.deg2rad(self.phase_0 + self.phaseamplifier_0)), ))

    def get_magnitude_2(self):
        return self.magnitude_2

    def set_magnitude_2(self, magnitude_2):
        self.magnitude_2 = magnitude_2
        self.constant_2.set_k(((self.magnitude_2+self.magnitudeamplifier_2)*numpy.exp(1j*numpy.deg2rad(self.phase_2 + self.phaseamplifier_2)), ))

    def get_magnitude_1(self):
        return self.magnitude_1

    def set_magnitude_1(self, magnitude_1):
        self.magnitude_1 = magnitude_1
        self.constant_1.set_k(((self.magnitude_1+self.magnitudeamplifier_1)*numpy.exp(1j*numpy.deg2rad(self.phase_1 + self.phaseamplifier_1)), ))

    def get_magnitude_0(self):
        return self.magnitude_0

    def set_magnitude_0(self, magnitude_0):
        self.magnitude_0 = magnitude_0
        self.constant_0.set_k(((self.magnitude_0+self.magnitudeamplifier_0)*numpy.exp(1j*numpy.deg2rad(self.phase_0 + self.phaseamplifier_0)), ))

    def get_length(self):
        return self.length

    def set_length(self, length):
        self.length = length
        self.blocks_vector_source_x_0_0_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])
        self.blocks_vector_source_x_0_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])
        self.blocks_vector_source_x_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay
        self.blocks_delay_0_0_0_0.set_dly(self.delay)
        self.blocks_delay_0_0_0.set_dly(self.delay)
        self.blocks_delay_0_0.set_dly(self.delay)

    def get_cpu(self):
        return self.cpu

    def set_cpu(self, cpu):
        self.cpu = cpu

    def get_codetype(self):
        return self.codetype

    def set_codetype(self, codetype):
        self.codetype = codetype
        self.blocks_vector_source_x_0_0_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])
        self.blocks_vector_source_x_0_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])
        self.blocks_vector_source_x_0.set_data(tuple(kcodes.get_pattern(self.length,self.period,self.codetype,self.seed).tolist()), [])


def main(top_block_cls=tx_sim, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
