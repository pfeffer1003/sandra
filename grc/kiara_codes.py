# this module will be imported in the into your flowgraph

import numpy

def get_pattern(length, period, codetype, seed):
    """	
    """
    if codetype == "cw":
        code = numpy.ones(length)
        code = numpy.float32(code)
    elif codetype == "barker":
        if length == 2: code = numpy.float32([+1,-1])
        if length == 3: code = numpy.float32([+1,+1,-1])
        if length == 4: code = numpy.float32([+1,-1,+1,+1])
        if length == 5: code = numpy.float32([+1,+1,+1,-1,+1])
        if length == 7: code = numpy.float32([+1,+1,+1,-1,-1,+1,-1])
        if length == 11: code = numpy.float32([+1,+1,+1,-1,-1,-1,+1,-1,-1,+1,-1])
        if length == 13: code = numpy.float32([+1,+1,+1,+1,+1,-1,-1,+1,+1,-1,+1,-1,+1])
    elif codetype == "complementary":
        if length == 4: code = numpy.float32([[+1,+1,+1,-1],
                                              [+1,+1,-1,+1]])
        if length == 8: code = numpy.float32([[+1,+1,+1,-1,+1,+1,-1,+1],
                                              [+1,+1,+1,-1,-1,-1,+1,-1]])
        if length == 16: code = numpy.float32([[+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1],
                                               [+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1]])
        if length == 32: code = numpy.float32([[+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,+1,+1+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
                                               [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,-1,-1,-1,+1,-1,-1,+1,-1,+1,+1,+1,-1,-1,-1,+1,-1]])
    elif codetype == "pseudo":
        numpy.random.seed(seed)
        code = numpy.random.random(length)
        code = numpy.exp(2.0*numpy.pi*1.0j*code)
        code = numpy.angle(code)
        code = -1.0*numpy.sign(code)
        code = code.real
        code = numpy.float32(code)
    elif codetype == "complementary":
        code = numpy.zeros((2,period),numpy.float32)
        code[:,0:length] = code
        code = code.flatten()
    else:
        code = numpy.zeros(self.period,numpy.float32)
        code[0:self.length] = code
    code = numpy.complex64(code)
    return code

pattern = get_pattern(12,100,"cw",0)
print(pattern)
print(pattern.dtype)