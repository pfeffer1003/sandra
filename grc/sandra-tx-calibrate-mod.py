#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Tx Cal
# Author: Kiara Chau
# Generated: Wed Jul 21 09:13:10 2021
##################################################

if __name__ == '__main__':

    import ctypes
    import sys

    if sys.platform.startswith('linux'):

        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()

        except:
            print "Warning: failed to XInitThreads()"


from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import zeromq
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import kcodes  # embedded python module
import numpy
import sip
import sys
from gnuradio import qtgui




class SandraTxCalibrate(gr.top_block, Qt.QWidget):

    def __init__(self):

        gr.top_block.__init__(self, "SanDRA TX Calibrate")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("SanDRA TX Calibrate")
        qtgui.util.check_set_qss()

        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass

        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "SandraTxCalibrate")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Parameters
        ##################################################
        self.CHANNELS = CHANNELS = 3
        self.seed = seed = 0
        self.samplerate = samplerate = 100000.0
        self.period = period = 100
        self.length = length = 13
        self.cpu = cpu = 0
        self.codetype = codetype = 'pseudo'


        ##################################################
        # Variables
        ##################################################
        if self.CHANNELS>=2:
            self.set_channel(1)
        else:
            self.set_channel(0)

        ##################################################
        # Blocks
        ##################################################
        self._channel_range = Range(1, CHANNELS-1, 1, 1, 200)
        self._channel_win = RangeWidget(self._channel_range, self.set_channel, 'Channel Select', "counter_slider", int)
        self.top_layout.addWidget(self._channel_win)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_float*1, samplerate,True)
        self.block_vector_source = zeromq.sub_source(gr.sizeof_gr_complex, CHANNELS, 'tcp://127.0.0.1:7000', 100, True, -1)
        (self.block_vector_source).set_processor_affinity([0])
        self.block_vector2streams = blocks.vector_to_streams(gr.sizeof_gr_complex*1, CHANNELS)
        (self.block_vector2streams).set_processor_affinity([0])
        self.block_subtract_01 = blocks.sub_cc(1)
        (self.block_subtract_01).set_processor_affinity([0])
        


        #---------------------------------------------------------------------#

        # self.block_scope_0_1 = qtgui.time_sink_f(
        #     period, #size
        #     samplerate, #samp_rate
        #     "", #name
        #     2 #number of inputs
        # )
        # self.block_scope_0_1.set_update_time(0.10)
        # self.block_scope_0_1.set_y_axis(-15, 15)
        # self.block_scope_0_1.set_y_label('Magnitude', "")
        # self.block_scope_0_1.enable_tags(-1, True)
        # self.block_scope_0_1.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "key0")
        # self.block_scope_0_1.enable_autoscale(True)
        # self.block_scope_0_1.enable_grid(True)
        # self.block_scope_0_1.enable_axis_labels(True)
        # self.block_scope_0_1.enable_control_panel(False)
        # if not True:
        #     self.block_scope_0_1.disable_legend()
        # labels = ['abs(0)', 'abs(1)', '', '', '',
        #           '', '', '', '', '']
        # widths = [2, 2, 1, 1, 1,
        #           1, 1, 1, 1, 1]
        # colors = ["black", "Dark Blue", "green", "black", "cyan",
        #           "magenta", "yellow", "dark red", "dark green", "blue"]
        # styles = [1, 1, 1, 1, 1,
        #           1, 1, 1, 1, 1]
        # markers = [-1, -1, -1, -1, -1,
        #            -1, -1, -1, -1, -1]
        # alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
        #           1.0, 1.0, 1.0, 1.0, 1.0]
        # for i in xrange(2):
        #     if len(labels[i]) == 0:
        #         self.block_scope_0_1.set_line_label(i, "Data {0}".format(i))
        #     else:
        #         self.block_scope_0_1.set_line_label(i, labels[i])
        #     self.block_scope_0_1.set_line_width(i, widths[i])
        #     self.block_scope_0_1.set_line_color(i, colors[i])
        #     self.block_scope_0_1.set_line_style(i, styles[i])
        #     self.block_scope_0_1.set_line_marker(i, markers[i])
        #     self.block_scope_0_1.set_line_alpha(i, alphas[i])
        # self._block_scope_0_1_win = sip.wrapinstance(self.block_scope_0_1.pyqwidget(), Qt.QWidget)
        # self.top_grid_layout.addWidget(self._block_scope_0_1_win, 0,0,1,1)
        # (self.block_scope_0_1).set_processor_affinity([0])
        
        #---------------------------------------------------------------------#

        # self.block_scope_01 = qtgui.time_sink_f(
        #     period, #size
        #     samplerate, #samp_rate
        #     "", #name
        #     1 #number of inputs
        # )
        # self.block_scope_01.set_update_time(0.10)
        # self.block_scope_01.set_y_axis(-1, 2)
        # self.block_scope_01.set_y_label('Magnitude', "")
        # self.block_scope_01.enable_tags(-1, True)
        # self.block_scope_01.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "key0")
        # self.block_scope_01.enable_autoscale(True)
        # self.block_scope_01.enable_grid(True)
        # self.block_scope_01.enable_axis_labels(True)
        # self.block_scope_01.enable_control_panel(False)
        # if not True:
        #     self.block_scope_01.disable_legend()
        # labels = ['abs(0)-abs(1)', 'abs(1)', '', '', '',
        #           '', '', '', '', '']
        # widths = [2, 2, 1, 1, 1,
        #           1, 1, 1, 1, 1]
        # colors = ["dark red", "Dark Blue", "green", "black", "cyan",
        #           "magenta", "yellow", "dark red", "dark green", "blue"]
        # styles = [1, 1, 1, 1, 1,
        #           1, 1, 1, 1, 1]
        # markers = [-1, -1, -1, -1, -1,
        #            -1, -1, -1, -1, -1]
        # alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
        #           1.0, 1.0, 1.0, 1.0, 1.0]
        # for i in xrange(1):
        #     if len(labels[i]) == 0:
        #         self.block_scope_01.set_line_label(i, "Data {0}".format(i))
        #     else:
        #         self.block_scope_01.set_line_label(i, labels[i])
        #     self.block_scope_01.set_line_width(i, widths[i])
        #     self.block_scope_01.set_line_color(i, colors[i])
        #     self.block_scope_01.set_line_style(i, styles[i])
        #     self.block_scope_01.set_line_marker(i, markers[i])
        #     self.block_scope_01.set_line_alpha(i, alphas[i])
        # self._block_scope_01_win = sip.wrapinstance(self.block_scope_01.pyqwidget(), Qt.QWidget)
        # self.top_grid_layout.addWidget(self._block_scope_01_win, 1,0,1,1)
        # (self.block_scope_01).set_processor_affinity([0])
        
        #---------------------------------------------------------------------#
        self.block_scope_0_1 = self.__get__time_sink(n_inputs=2, ylim=-15, ymax=15, labels=['abs(0)', 'abs(1)', 'abs(2)', '', '',
                  '', '', '', '', ''], colors=["black", "Dark Blue", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"], location=[0,0])

        self.block_scope_01 = self.__get__time_sink(n_inputs=1, 
                                                ylim=-1, ymax=2, 
                                                labels=['abs(0)-abs(1)', '', '', '', '', '', '', '', '', ''], 
                                                colors=["dark red", "Dark Blue", "green", "black", "cyan", "magenta", "yellow", "dark red", "dark green", "blue"], 
                                                location=[1,0])

        #---------------------------------------------------------------------#

        self.block_magnitude_1 = blocks.complex_to_mag(1)
        (self.block_magnitude_1).set_processor_affinity([0])
        self.block_magnitude_01 = blocks.complex_to_mag(1)
        (self.block_magnitude_01).set_processor_affinity([0])
        self.block_magnitude_0 = blocks.complex_to_mag(1)
        (self.block_magnitude_0).set_processor_affinity([0])
        self.block_decoder_1 = filter.fir_filter_ccc(1, (tuple(numpy.complex64(kcodes.get_pattern(length,length,codetype,seed)[::-1]).tolist())))
        self.block_decoder_1.declare_sample_delay(0)
        (self.block_decoder_1).set_processor_affinity([0])
        self.block_decoder_0 = filter.fir_filter_ccc(1, (tuple(numpy.complex64(kcodes.get_pattern(length,length,codetype,seed)[::-1]).tolist())))
        self.block_decoder_0.declare_sample_delay(0)
        (self.block_decoder_0).set_processor_affinity([0])
        self.block_decimating_1 = filter.fir_filter_ccf(1, ([1.0]))
        self.block_decimating_1.declare_sample_delay(0)
        (self.block_decimating_1).set_processor_affinity([0])
        self.block_decimating_0 = filter.fir_filter_ccc(1, ([1]))
        self.block_decimating_0.declare_sample_delay(0)
        (self.block_decimating_0).set_processor_affinity([0])
        



        #---------------------------------------------------------------------#

        # self.block_constellation_0_1 = qtgui.const_sink_c(
        #     period, #size
        #     "", #name
        #     2 #number of inputs
        # )
        # self.block_constellation_0_1.set_update_time(0.10)
        # self.block_constellation_0_1.set_y_axis(-2, 2)
        # self.block_constellation_0_1.set_x_axis(-2, 2)
        # self.block_constellation_0_1.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, "key0")
        # self.block_constellation_0_1.enable_autoscale(True)
        # self.block_constellation_0_1.enable_grid(True)
        # self.block_constellation_0_1.enable_axis_labels(True)
        # if not False:
        #     self.block_constellation_0_1.disable_legend()
        # labels = ['abs(0)', 'abs(1)', '', '', '',
        #           '', '', '', '', '']
        # widths = [1, 1, 1, 1, 1,
        #           1, 1, 1, 1, 1]
        # colors = ["black", "Dark Blue", "red", "red", "red",
        #           "red", "red", "red", "red", "red"]
        # styles = [0, 0, 0, 0, 0,
        #           0, 0, 0, 0, 0]
        # markers = [0, 0, 0, 0, 0,
        #            0, 0, 0, 0, 0]
        # alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
        #           1.0, 1.0, 1.0, 1.0, 1.0]
        # for i in xrange(2):
        #     if len(labels[i]) == 0:
        #         self.block_constellation_0_1.set_line_label(i, "Data {0}".format(i))
        #     else:
        #         self.block_constellation_0_1.set_line_label(i, labels[i])
        #     self.block_constellation_0_1.set_line_width(i, widths[i])
        #     self.block_constellation_0_1.set_line_color(i, colors[i])
        #     self.block_constellation_0_1.set_line_style(i, styles[i])
        #     self.block_constellation_0_1.set_line_marker(i, markers[i])
        #     self.block_constellation_0_1.set_line_alpha(i, alphas[i])
        # self._block_constellation_0_1_win = sip.wrapinstance(self.block_constellation_0_1.pyqwidget(), Qt.QWidget)
        # self.top_grid_layout.addWidget(self._block_constellation_0_1_win, 0,1,1,1)
        # (self.block_constellation_0_1).set_processor_affinity([0])
        

        #---------------------------------------------------------------------#

        # self.block_constallation_01 = qtgui.const_sink_c(
        #     period, #size
        #     "", #name
        #     1 #number of inputs
        # )
        # self.block_constallation_01.set_update_time(0.10)
        # self.block_constallation_01.set_y_axis(-2, 2)
        # self.block_constallation_01.set_x_axis(-2, 2)
        # self.block_constallation_01.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, "key0")
        # self.block_constallation_01.enable_autoscale(True)
        # self.block_constallation_01.enable_grid(True)
        # self.block_constallation_01.enable_axis_labels(True)
        # if not False:
        #   self.block_constallation_01.disable_legend()
        # labels = ['', 'abs(1)', '', '', '',
        #           '', '', '', '', '']
        # widths = [1, 2, 1, 1, 1,
        #           1, 1, 1, 1, 1]
        # colors = ["dark red", "Dark Blue", "red", "red", "red",
        #           "red", "red", "red", "red", "red"]
        # styles = [0, 0, 0, 0, 0,
        #           0, 0, 0, 0, 0]
        # markers = [0, 0, 0, 0, 0,
        #            0, 0, 0, 0, 0]
        # alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
        #           1.0, 1.0, 1.0, 1.0, 1.0]
        # for i in xrange(1):
        #     if len(labels[i]) == 0:
        #         self.block_constallation_01.set_line_label(i, "Data {0}".format(i))
        #     else:
        #         self.block_constallation_01.set_line_label(i, labels[i])
        #     self.block_constallation_01.set_line_width(i, widths[i])
        #     self.block_constallation_01.set_line_color(i, colors[i])
        #     self.block_constallation_01.set_line_style(i, styles[i])
        #     self.block_constallation_01.set_line_marker(i, markers[i])
        #     self.block_constallation_01.set_line_alpha(i, alphas[i])
        # self._block_constallation_01_win = sip.wrapinstance(self.block_constallation_01.pyqwidget(), Qt.QWidget)
        # self.top_grid_layout.addWidget(self._block_constallation_01_win, 1,1,1,1)
        # (self.block_constallation_01).set_processor_affinity([0])

        #---------------------------------------------------------------------#



        self.block_constallation_01 = self.__get__constellation(n_inputs=1, labels=['', 'abs(1)', '', '', '',
                  '', '', '', '', ''], widths=[1, 2, 1, 1, 1, 1, 1, 1, 1, 1], colors=["dark red", "Dark Blue", "red", "red", "red",
                  "red", "red", "red", "red", "red"], location=[1,1])

        self.block_constellation_0_1 = self.__get__constellation(n_inputs=2, labels=['abs(0)', 'abs(1)', '', '', '',
                  '', '', '', '', ''], widths=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1], colors=["black", "Dark Blue", "red", "red", "red",
                  "red", "red", "red", "red", "red"], location=[0,1])
        #---------------------------------------------------------------------#


        if self.CHANNELS != 1:
            self.block_channel_select = blocks.multiply_matrix_cc((self.switch,), gr.TPP_ALL_TO_ALL)
            (self.block_channel_select).set_processor_affinity([0])

        ##################################################
        # Connections
        ##################################################
        
        self.connect((self.block_decimating_0, 0), (self.block_decoder_0, 0))
        self.connect((self.block_decoder_0, 0), (self.block_constellation_0_1, 0))
        self.connect((self.block_decoder_0, 0), (self.block_magnitude_0, 0))
        self.connect((self.block_decoder_0, 0), (self.block_subtract_01, 0))
        self.connect((self.block_decimating_1, 0), (self.block_decoder_1, 0))
        self.connect((self.block_decoder_1, 0), (self.block_constellation_0_1, 1))
        self.connect((self.block_decoder_1, 0), (self.block_magnitude_1, 0))
        self.connect((self.block_decoder_1, 0), (self.block_subtract_01, 1))
        self.connect((self.block_magnitude_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.block_magnitude_01, 0), (self.block_scope_01, 0))
        self.connect((self.block_magnitude_1, 0), (self.block_scope_0_1, 1))
        self.connect((self.block_subtract_01, 0), (self.block_constallation_01, 0))
        self.connect((self.block_subtract_01, 0), (self.block_magnitude_01, 0))

        self.connect((self.block_vector_source, 0), (self.block_vector2streams, 0))
        self.connect((self.block_vector2streams, 0), (self.block_decimating_0, 0))
        self.connect((self.block_vector2streams, 1), (self.block_channel_select, 0))
        self.connect((self.block_vector2streams, 2), (self.block_channel_select, 1))

        self.connect((self.block_channel_select, 0), (self.block_decimating_1, 0))

        self.connect((self.blocks_throttle_0, 0), (self.block_scope_0_1, 0))

    def __get__time_sink(self, n_inputs, ylim, ymax, labels, colors, location=[1,0]):
        '''
        Parameter <location> must be a list.
        '''
        time_sink = qtgui.time_sink_f(self.period, self.samplerate, "", n_inputs)
        time_sink.set_update_time(0.10)
        time_sink.set_y_axis(ylim, ymax)
        time_sink.set_y_label('Magnitude', "")
        time_sink.enable_tags(-1, True)
        time_sink.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "key0")
        time_sink.enable_autoscale(True)
        time_sink.enable_grid(True)
        time_sink.enable_axis_labels(True)
        time_sink.enable_control_panel(False)
        widths = [2, 2, 1, 1, 1,
                  1, 1, 1, 1, 1]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(n_inputs):
            if len(labels[i]) == 0:
                time_sink.set_line_label(i, "Data {0}".format(i))
            else:
                if i == 0:
                    time_sink.set_line_label(i, labels[i])
                elif i == 1:
                    time_sink.set_line_label(i, "abs("+str(self.get_channel())+")")

            time_sink.set_line_width(i, widths[i])
            time_sink.set_line_color(i, colors[i])
            time_sink.set_line_style(i, styles[i])
            time_sink.set_line_marker(i, markers[i])
            time_sink.set_line_alpha(i, alphas[i])
        time_sink_win = sip.wrapinstance(time_sink.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(time_sink_win, location[0], location[1],1,1)
        (time_sink).set_processor_affinity([0])

        
        return time_sink
        
    def __get__constellation(self, n_inputs, labels, widths, colors, location):
        constellation = qtgui.const_sink_c(
                    self.period, #size
                    "", #name
                    n_inputs #number of inputs
                )
        constellation.set_update_time(0.10)
        constellation.set_y_axis(-2, 2)
        constellation.set_x_axis(-2, 2)
        constellation.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, "key0")
        constellation.enable_autoscale(True)
        constellation.enable_grid(True)
        constellation.enable_axis_labels(True)
        constellation.disable_legend()
        styles = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        markers = [0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(n_inputs):
            if len(labels[i]) == 0:
                constellation.set_line_label(i, "Data {0}".format(i))
            else:
                constellation.set_line_label(i, labels[i])
            constellation.set_line_width(i, widths[i])
            constellation.set_line_color(i, colors[i])
            constellation.set_line_style(i, styles[i])
            constellation.set_line_marker(i, markers[i])
            constellation.set_line_alpha(i, alphas[i])
        constellation_win = sip.wrapinstance(constellation.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(constellation_win, location[0],location[1],1,1)
        (constellation).set_processor_affinity([0])

        return constellation

    def closeEvent(self, event):
        """
        """
        self.settings = Qt.QSettings("GNU Radio", "tx_cal")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_channel(self):
        """
        """
        return self.channel

    def set_channel(self, channel):
        """
        """
        self.channel = int(channel)
        self.switch = tuple(
            numpy.roll(
                a=(1,)+(0,)*(self.CHANNELS-2),
                shift=channel-1,
            )
        )
        #self.block_scope_0_1.set_line_label(i, "abs("+str(self.get_channel())+")")
        try:
            self.block_channel_select.set_A(
                (self.switch,)
            )
        except AttributeError:
            pass




    #def get_seed(self):
    #"""
    #"""
    #    return self.seed

    #def set_seed(self, seed):
    #"""
    #"""
    #    self.seed = seed
    #    self.block_decoder_1.set_taps((tuple(numpy.complex64(kcodes.get_pattern(self.length,self.length,self.codetype,self.seed)[::-1]).tolist())))
    #    self.block_decoder_0.set_taps((tuple(numpy.complex64(kcodes.get_pattern(self.length,self.length,self.codetype,self.seed)[::-1]).tolist())))

    #def get_samplerate(self):
    #"""
    #"""
    #    return self.samplerate

    #def set_samplerate(self, samplerate):
    #"""
    #"""
    #    self.samplerate = samplerate
    #    self.blocks_throttle_0.set_sample_rate(self.samplerate)
    #    self.block_scope_0_1.set_samp_rate(self.samplerate)
    #    self.block_scope_01.set_samp_rate(self.samplerate)

    #def get_period(self):
    #"""
    #"""
    #    return self.period

    #def set_period(self, period):
    #"""
    #"""
    #    self.period = period

    #def get_length(self):
    #"""
    #"""
    #    return self.length

    #def set_length(self, length):
    #"""
    #"""
    #    self.length = length
    #    self.block_decoder_1.set_taps((tuple(numpy.complex64(kcodes.get_pattern(self.length,self.length,self.codetype,self.seed)[::-1]).tolist())))
    #    self.block_decoder_0.set_taps((tuple(numpy.complex64(kcodes.get_pattern(self.length,self.length,self.codetype,self.seed)[::-1]).tolist())))

    #def get_cpu(self):
    #"""
    #"""
    #    return self.cpu

    #def set_cpu(self, cpu):
    #"""
    #"""
    #    self.cpu = cpu

    #def get_codetype(self):
    #"""
    #"""
    #    return self.codetype

    #def set_codetype(self, codetype):
    #"""
    #"""
    #    self.codetype = codetype
    #    self.block_decoder_1.set_taps((tuple(numpy.complex64(kcodes.get_pattern(self.length,self.length,self.codetype,self.seed)[::-1]).tolist())))
    #    self.block_decoder_0.set_taps((tuple(numpy.complex64(kcodes.get_pattern(self.length,self.length,self.codetype,self.seed)[::-1]).tolist())))


    def __set__seed(self,):
        """
        """
        pass




def main(top_block_cls=SandraTxCalibrate, options=None):

    
    from distutils.version import StrictVersion
    
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):

        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():

        tb.stop()
        tb.wait()

    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
