#!/usr/bin/env python

###############################################################################
#                                                                             #
# FILE          :       setup.py                                              #
#                                                                             #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
#                                                                             #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
import sys
import setuptools

###############################################################################

setuptools.setup(
    name='sandra',
    version='1.0',
    description='SanDRA - Software Defined Radar in Atmopsheric Research',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Topic :: Scientific/Engineering :: Atmospheric Science',
    ],
    keywords='radar dsp sdr gnuradio tx rx mimo',
    author='M.Eng. Nico Pfeffer',
    author_email='pfeffer@iap-kborn.de',
    license='MIT',
    test_suite='nose.collector',
    tests_require=['nose'],
    packages=setuptools.find_packages(),
    zip_safe=False,
)

###############################################################################