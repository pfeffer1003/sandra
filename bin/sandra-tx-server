#!/usr/bin/env python
"""
Usage:
   sandra-tx-server CONFIG
"""

###############################################################################
#                                                                             #
# FILE          :       sandra-tx-server                                      #
#                                                                             #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
#                                                                             #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
import sys

###############################################################################

activate_this = "%s/.sandra/bin/activate_this.py"%os.getenv('HOME')
execfile(activate_this, dict(__file__=activate_this))

###############################################################################

import pmt
import time
import numpy
import sandra
import threading
import gnuradio.gr
import gnuradio.uhd
import gnuradio.blocks
import SimpleXMLRPCServer

###############################################################################

from docopt import docopt
from gnuradio.gr import top_block as Flowgraph
from gnuradio.blocks import delay as Delay
from gnuradio.blocks import add_vcc as Add
from gnuradio.blocks import throttle as Throttle
from gnuradio.zeromq import pub_sink as StreamSink
from gnuradio.blocks import tags_strobe as StrobeSource
from gnuradio.blocks import vector_source_c as Vector
from gnuradio.blocks import multiply_const_vcc as Phasor
from gnuradio.filter import interp_fir_filter_ccf as Interpolator

###############################################################################

class Main(object):

    # ----------------------------------------------------------------------- #

    def __init__(self):

        """
        """


        f = open('%s/.sandra/log/tx/main.stderr'%os.getenv('HOME'),"w")
        f.close()


        self._log = sandra.tx.Logging.file("%s/.sandra/log/tx/main.log" % os.getenv("HOME"))
        self._log.info("")
        self._log.info("Initializing Transmitter")

        
        try:


            self._cfg = sandra.tx.Config()
            self._cfg.load("%s/.sandra/cfg/tx/main.cfg" % os.getenv("HOME"))
            self._log.info("  Configuration loaded")


            self._server_host = self._cfg.get_control_host()
            self._server_port = self._cfg.get_control_port()
            self._server_addr = (self._server_host,self._server_port)
            self._usrp_cpus = self._cfg.get_usrp_cpus()
            self.__set__chan_cpus()

            self._cores = str(list(set(self._usrp_cpus)))
            self._cores = self._cores[1::]
            self._cores = self._cores[0:-1]
            self._cores = self._cores.replace(' ','')
            os.system("taskset -cp %s %s"%(self._cores,os.getpid()))
            self._log.info("  Pinning Process to cores %s" % self._cores)


            self.server = SimpleXMLRPCServer.SimpleXMLRPCServer(self._server_addr,allow_none=True,logRequests=False)
            self.server.register_instance(self)
            self.server_thread = threading.Thread(target=self.server.serve_forever)
            self.server_thread.daemon = True
            self.server_thread.start()
            self._log.info("  Starting RPC Server")


            self._delay = self._cfg.get_symbol_delay()
            self._phases = self._cfg.get_channel_phases()
            self._amplitudes = self._cfg.get_channel_amplitudes()
            self._channels = range(self._cfg.get_usrp_channels())
            self._motherboards = range(self._cfg.get_usrp_motherboards())
            self._addresses = ','.join(['addr%s=%s'%(mb,self._cfg.get_usrp_address(mb)) for mb in self._motherboards])
            self._log.info("  Collect Channel Parameters")            


            self._start_time  = time.time()
            self._start_time  = numpy.floor(self._start_time)
            self._start_time  = numpy.int(self._start_time)
            self._start_time += 10.0
            self._start_time -= self._start_time%self._cfg.get_symbol_sync_second()
            self._start_time += self._cfg.get_symbol_sync_second()
            self._log.info("  Calculating Start Timestamp")


            self._pps_time = time.time()
            while((self._pps_time-numpy.floor(self._pps_time)>0.2)or
                  (self._pps_time-numpy.floor(self._pps_time)>0.3)):
                self._pps_time = time.time()
                time.sleep(0.01)
            self._pps_time = numpy.ceil(self._pps_time)+1
            self._log.info("  Calculating Source Timestamp")

            self._flowgraph = Flowgraph()
            self._log.info("  Initialize Flowgraph")


            if self._cfg.get_control_mode() == 'simulate':

                self._log.info("  Mode is: SIMULATION")

                self._blocks_strobe_source = StrobeSource(8,pmt.intern("Trigger"),self._cfg.get_code_period()*self._cfg.get_symbol_interpolation(),pmt.intern("Strobe"))
                self._blocks_add = [Add(1) for c in self._channels]
                self._blocks_vector = [Vector(tuple(self._cfg.get_channel_pattern(c).tolist()),True,1,[]) for c in self._channels]
                self._blocks_delay  = [Delay(8,0) for c in self._channels]
                self._blocks_phasor = [Phasor((0.0,),) for c in self._channels]
                self._blocks_interpolator = [Interpolator(self._cfg.get_symbol_interpolation(),tuple(self._cfg.get_symbol_taps().tolist())) for c in self._channels]
                self._blocks_throttle = [Throttle(8,self._cfg.get_usrp_sample_rate(),True) for c in self._channels]
                self._blocks_stream_sink = [StreamSink(8,1,'tcp://127.0.0.1:%s' % self._cfg.get_channel_stream_port(c),100,True,-1) for c in self._channels]
                self._log.info("  Creating the Flowgraph Block")

                [self._blocks_vector[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_delay[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_phasor[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_interpolator[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_throttle[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_stream_sink[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_add[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                self._blocks_strobe_source.set_processor_affinity([self._chan_cpus[0]])
                self._log.info("  Setting Core Affinities")

                [self._flowgraph.connect((self._blocks_vector[c],0),(self._blocks_delay[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_delay[c],0),(self._blocks_phasor[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_phasor[c],0),(self._blocks_interpolator[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_interpolator[c],0),(self._blocks_throttle[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_throttle[c],0),(self._blocks_add[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_strobe_source,0),(self._blocks_add[c],1)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_add[c],0),(self._blocks_stream_sink[c],0)) for c in self._channels]                
                self._log.info("  Connecting Flowgraph")

            if self._cfg.get_control_mode() == 'calibrate':

                self._log.info("  Mode is: CALIBRATION")

                self._blocks_strobe_source = StrobeSource(8,pmt.intern("Trigger"),self._cfg.get_code_period()*self._cfg.get_symbol_interpolation(),pmt.intern("Strobe"))
                self._blocks_vector = [Vector(tuple(self._cfg.get_channel_pattern(0).tolist()),True,1,[]) for c in self._channels]
                self._blocks_delay  = [Delay(8,0) for c in self._channels]
                self._blocks_phasor = [Phasor((0.0,),) for c in self._channels]
                self._blocks_interpolator = [Interpolator(self._cfg.get_symbol_interpolation(),tuple(self._cfg.get_symbol_taps().tolist())) for c in self._channels]
                self._blocks_add = [Add(1) for c in self._channels]
                self._blocks_stream_sink = [StreamSink(8,1,'tcp://127.0.0.1:%s' % self._cfg.get_channel_stream_port(c),100,True,-1) for c in self._channels]
                try:
                    self._block_usrp = gnuradio.uhd.usrp_sink(
                        device_addr=self._addresses,
                        stream_args=gnuradio.uhd.stream_args(
                            cpu_format='fc32',
                            otw_format='sc16',
                            channels=self._channels
                        )
                    )
                except RuntimeError as exception:
                    self._log.error("  Interface not bind!")
                    self._log.error("    - USRP not connected?")
                    self._log.error("    - USRP incorrect configured?")
                    raise str(exception)
                self._log.info("  Creating the Flowgraph Block")

                [self._block_usrp.set_clock_source(self._cfg.get_usrp_clock_source(mb),mb) for mb in self._motherboards]
                if not 'internal' in self._cfg.get_usrp_clock_sources():
                    [self._block_usrp.set_time_source(self._cfg.get_usrp_time_source(mb),mb) for mb  in self._motherboards]
                [self._block_usrp.set_subdev_spec(self._cfg.get_usrp_route(mb),mb) for mb  in self._motherboards]
                [self._block_usrp.set_center_freq(self._cfg.get_symbol_carrier(),ch) for ch  in self._channels]
                self._block_usrp.set_processor_affinity(self._usrp_cpus)
                self._block_usrp.set_samp_rate(self._cfg.get_usrp_sample_rate())
                self._block_usrp.clear_command_time(gnuradio.uhd.ALL_MBOARDS)
                time.sleep(0.2)
                self._block_usrp.start()
                time.sleep(0.5)
                self._block_usrp.stop()
                time.sleep(0.2)
                if not 'internal' in self._cfg.get_usrp_clock_sources():
                    self._block_usrp.set_time_unknown_pps(gnuradio.uhd.time_spec(self._pps_time))
                    self._block_usrp.set_start_time(gnuradio.uhd.time_spec(self._start_time))
                else:
                    self._block_usrp.set_time_now(gnuradio.uhd.time_spec(self._pps_time),gnuradio.uhd.ALL_MBOARDS)
                    self._block_usrp.set_start_time(gnuradio.uhd.time_spec(self._start_time))
                self._log.info("  USRP N200 setup complete")

                [self._blocks_vector[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_delay[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_phasor[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_interpolator[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_stream_sink[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_add[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                self._blocks_strobe_source.set_processor_affinity([self._chan_cpus[0]])
                self._log.info("  Setting Core Affinities")

                [self._flowgraph.connect((self._blocks_vector[c],0),(self._blocks_delay[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_delay[c],0),(self._blocks_phasor[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_phasor[c],0),(self._blocks_interpolator[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_interpolator[c],0),(self._blocks_add[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_strobe_source,0),(self._blocks_add[c],1)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_add[c],0),(self._blocks_stream_sink[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_interpolator[c],0),(self._block_usrp,c)) for c in self._channels]  
                self._log.info("  Connecting Flowgraph")

            if self._cfg.get_control_mode() == 'operate':

                self._log.info("  Mode is: OPERATION")
                
                self._blocks_strobe_source = StrobeSource(8,pmt.intern("Trigger"),self._cfg.get_code_period()*self._cfg.get_symbol_interpolation(),pmt.intern("Strobe"))
                self._blocks_vector = [Vector(tuple(self._cfg.get_channel_pattern(c).tolist()),True,1,[]) for c in self._channels]
                self._blocks_delay  = [Delay(8,0) for c in self._channels]
                self._blocks_phasor = [Phasor((0.0,),) for c in self._channels]
                self._blocks_interpolator = [Interpolator(self._cfg.get_symbol_interpolation(),tuple(self._cfg.get_symbol_taps().tolist())) for c in self._channels]
                self._blocks_add = [Add(1) for c in self._channels]
                self._blocks_stream_sink = [StreamSink(8,1,'tcp://127.0.0.1:%s' % self._cfg.get_channel_stream_port(c),100,True,-1) for c in self._channels]
                try:
                    self._block_usrp = gnuradio.uhd.usrp_sink(
                        device_addr=self._addresses,
                        stream_args=gnuradio.uhd.stream_args(
                            cpu_format='fc32',
                            otw_format='sc16',
                            channels=self._channels
                        )
                    )
                except RuntimeError as exception:
                    self._log.error("  Interface not bind!")
                    self._log.error("    - USRP not connected?")
                    self._log.error("    - USRP incorrect configured?")
                    raise str(exception)
                self._log.info("  Creating the Flowgraph Block")

                [self._block_usrp.set_clock_source(self._cfg.get_usrp_clock_source(mb),mb) for mb in self._motherboards]
                if not 'internal' in self._cfg.get_usrp_clock_sources():
                    [self._block_usrp.set_time_source(self._cfg.get_usrp_time_source(mb),mb) for mb  in self._motherboards]
                [self._block_usrp.set_subdev_spec(self._cfg.get_usrp_route(mb),mb) for mb  in self._motherboards]
                [self._block_usrp.set_center_freq(self._cfg.get_symbol_carrier(),ch) for ch  in self._channels]
                self._block_usrp.set_processor_affinity(self._usrp_cpus)
                self._block_usrp.set_samp_rate(self._cfg.get_usrp_sample_rate())
                self._block_usrp.clear_command_time(gnuradio.uhd.ALL_MBOARDS)
                time.sleep(0.2)
                self._block_usrp.start()
                time.sleep(0.5)
                self._block_usrp.stop()
                time.sleep(0.2)
                if not 'internal' in self._cfg.get_usrp_clock_sources():
                    self._block_usrp.set_time_unknown_pps(gnuradio.uhd.time_spec(self._pps_time))
                    self._block_usrp.set_start_time(gnuradio.uhd.time_spec(self._start_time))
                else:
                    self._block_usrp.set_time_now(gnuradio.uhd.time_spec(self._pps_time),gnuradio.uhd.ALL_MBOARDS)
                    self._block_usrp.set_start_time(gnuradio.uhd.time_spec(self._start_time))
                self._log.info("  USRP N200 setup complete")

                [self._blocks_vector[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_delay[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_phasor[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_interpolator[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_stream_sink[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                [self._blocks_add[c].set_processor_affinity([self._chan_cpus[c]]) for c in self._channels]
                self._blocks_strobe_source.set_processor_affinity([self._chan_cpus[c]])
                self._log.info("  Setting Core Affinities")

                [self._flowgraph.connect((self._blocks_vector[c],0),(self._blocks_delay[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_delay[c],0),(self._blocks_phasor[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_phasor[c],0),(self._blocks_interpolator[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_interpolator[c],0),(self._blocks_add[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_strobe_source,0),(self._blocks_add[c],1)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_add[c],0),(self._blocks_stream_sink[c],0)) for c in self._channels]
                [self._flowgraph.connect((self._blocks_interpolator[c],0),(self._block_usrp,c)) for c in self._channels]                 
                self._log.info("  Connecting Flowgraph")

        except:
            self.stop()

    # ----------------------------------------------------------------------- #

    def set_channel_delays(self, value):
        """
        """
        self._log.info("  Setting Delay %s" % value)
        self._delay = value
        [self._blocks_delay[ch].set_dly(value) for ch in self._channels]

    # ----------------------------------------------------------------------- #

    def set_channel_phase(self, value, ch):
        """
        """
        self._log.info("  Setting Phase %s for Channel %s" % (value,ch))
        self._phases[ch] = value
        phasor  = value*1.0j*numpy.pi/180.0
        phasor  = numpy.exp(phasor)
        phasor *= self._amplitudes[ch]
        phasor *= 1.0 # factor
        phasor  = numpy.complex64(phasor)
        self._blocks_phasor[ch].set_k((phasor.tolist(),))

    # ----------------------------------------------------------------------- #

    def set_channel_amplitude(self, value, ch):
        """
        """
        self._log.info("  Setting Amplitude %s for Channel %s" % (value,ch))
        self._amplitudes[ch] = value
        phasor  = self._phases[ch]*1.0j*numpy.pi/180.0
        phasor  = numpy.exp(phasor)
        phasor *= value
        phasor *= 1.0 # factor
        phasor  = numpy.complex64(phasor)
        self._blocks_phasor[ch].set_k((phasor.tolist(),))

    # ----------------------------------------------------------------------- #

    def __set__chan_cpus(self):
        """
        """
        chan_cpus = []
        for mb in xrange(self._cfg.get_usrp_motherboards()):
            if self._cfg.get_usrp_route(mb) == "A:A" : chan_cpus.append([self._usrp_cpus[mb]]*1)
            if self._cfg.get_usrp_route(mb) == "A:B" : chan_cpus.append([self._usrp_cpus[mb]]*1)
            if self._cfg.get_usrp_route(mb) == "A:AB": chan_cpus.append([self._usrp_cpus[mb]]*1)
            #if self.__usrp_routes[mb] == "A:A A:B": chan_cpus.append([self.__usrp_cpus[mb]]*2)
        self._chan_cpus = sum(chan_cpus,[])

    # ----------------------------------------------------------------------- #






    # ----------------------------------------------------------------------- #

    def exception(self):
        """
        """
        self._log.exception('!!! TRACEBACK ERROR RAISED !!!')

    # ----------------------------------------------------------------------- #

    def start(self):
        """
        """
        if self._cfg.get_control_mode() == 'simulate':
            while True:
                if time.time() < self._start_time:
                    continue
                else:
                    break
            self._flowgraph.start()
        else:
            self._flowgraph.start()
            while True:
                if time.time()>=self._start_time:
                    break
                else:
                    time.sleep(0.1)
                    continue
        self._log.info("")
        self._log.info("####################")
        self._log.info("TRANSMITTER RUNNING!"); time.sleep(1.0)
        self._log.info("####################")
        self._log.info("")

        time.sleep(1.0)
        
        self.set_channel_delays(self._cfg.get_symbol_delay_init())
        [self.set_channel_phase(self._phases[c],c) for c in self._channels]
        [self.set_channel_amplitude(self._amplitudes[c],c) for c in self._channels]

        f = open('%s/.sandra/log/tx/main.stderr'%os.getenv('HOME'),"w")
        f.close()

        while True:
            try:
                file_size = os.path.getsize('%s/.sandra/log/tx/main.stderr'%os.getenv('HOME'))
                #if file_size != 0:
                #    raise RuntimeError(
                #        "A streaming error occured!\n")
                if self._cfg.get_control_mode() != 'simulate':
                    if not 'internal' in self._cfg.get_usrp_clock_sources():
                        for mb in self._motherboards:
                            if self._cfg.get_usrp_clock_source(mb)=='external':
                                if not str(self._block_usrp.get_mboard_sensor("ref_locked",mb))=="Ref: locked":
                                    raise RuntimeError("Reference Failure on Motherboard %s!" % mb)
                                elif (time.time()-self._block_usrp.get_time_last_pps(mb).get_real_secs())>10.0:
                                    raise RuntimeError("PPS Failure on Motherboard %s!" % mb)
                                else:
                                    continue
                            if self._cfg.get_usrp_clock_source(mb)=='mimo':
                                if not str(self._block_usrp.get_mboard_sensor("mimo_locked",mb))=="MIMO: locked":
                                    raise RuntimeError("Reference Failure on Motherboard %s!" % mb)
                                else:
                                    continue
                time.sleep(2.0)
            except RuntimeError as error:
                raise RuntimeError("One of the USRP's not working properly! %s" % str(error))

    # ----------------------------------------------------------------------- #

    def stop(self):

        self._log.exception('!!! TRACEBACK ERROR RAISED !!!')
        self._log.info("")
        self._log.info("###################")
        self._log.info("TRANSMITTER FAILED!")
        self._log.info("###################")
        os.system('rm -r %s/.sandra/log/tx/main.stderr'%os.getenv('HOME'))
        sys.exit()

    # ----------------------------------------------------------------------- #

    def exit(self):

        self._log.info("")
        self._log.info("###################")
        self._log.info("TRANSMITTER FAILED!")
        self._log.info("###################")
        os.system('rm -r %s/.sandra/log/tx/main.stderr'%os.getenv('HOME'))
        sys.exit()

    # ----------------------------------------------------------------------- #



###############################################################################

if __name__ == '__main__':

    #args = docopt(__doc__,options_first=True)
    #while True:
    #    print args['CONFIG']
    #    time.sleep(1.0)

    try:
        main = Main()
        main.start()
    except:
        try:
            main.stop()
        except NameError:
            main.exit()
            sys.exit()

###############################################################################