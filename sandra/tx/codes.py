###############################################################################
#                                                                             #
# FILE          :       codes.py                                              #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
import sys
import numpy
import sandra

###############################################################################

def cw(length):
    
    """
    """

    length = sandra.verify.INT(
        name='CodeLength',
        value=length,
        left="[",
        lower=1,
        upper="+inf",
        right=")",
        choices=[],
    )

    code = numpy.ones(length)
    code = numpy.complex64(code)
    
    return code

###############################################################################

def barker(length):
    
    """
    """

    length = sandra.verify.INT(
        name='CodeLength',
        value=length,
        left="[",
        lower=2,
        upper=13,
        right="]",
        choices=[2,3,4,5,7,11,13],
    )

    if length ==  2: code = [+1,-1]
    if length ==  3: code = [+1,+1,-1]
    if length ==  4: code = [+1,-1,+1,+1]
    if length ==  5: code = [+1,+1,+1,-1,+1]
    if length ==  7: code = [+1,+1,+1,-1,-1,+1,-1]
    if length == 11: code = [+1,+1,+1,-1,-1,-1,+1,-1,-1,+1,-1]
    if length == 13: code = [+1,+1,+1,+1,+1,-1,-1,+1,+1,-1,+1,-1,+1]
    code = numpy.complex64(code)

    return code

###############################################################################

def complementary(length):
    
    """
    """

    length = sandra.verify.INT(
        name='CodeLength',
        value=length,
        left="[",
        lower=2,
        upper=32,
        right="]",
        choices=[2,4,8,16,32],
    )

    if length == 4:
        code = [
            [+1,+1,+1,-1],
            [+1,+1,-1,+1],
        ]
    if length == 8:
        code = [
            [+1,+1,+1,-1,+1,+1,-1,+1],
            [+1,+1,+1,-1,-1,-1,+1,-1],
        ]
    if length == 16:
        code = [
            [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1],
            [+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
        ]
    if length == 32:
        code = [
            [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,
             +1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1,],
            [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,
             -1,-1,-1,+1,-1,-1,+1,-1,+1,+1,+1,-1,-1,-1,+1,-1,],
        ]
    code = numpy.complex64(code)

    return code

###############################################################################

def pseudo(length,seed):
    
    """
    """

    length = sandra.verify.INT(
        name='CodeLength',
        value=length,
        left="[",
        lower=1,
        upper="+inf",
        right=")",
        choices=[],
    )

    seed = sandra.verify.INT(
        name='CodeSeed',
        value=seed,
        left="[",
        lower=0,
        upper="+inf",
        right=")",
        choices=[],
    )

    numpy.random.seed(seed)
    code = numpy.random.random(length)
    code = numpy.exp(2.0*numpy.pi*1.0j*code)
    code = numpy.angle(code)
    code = -1.0*numpy.sign(code)
    code = code.real
    code = numpy.complex64(code)

    return code

###############################################################################