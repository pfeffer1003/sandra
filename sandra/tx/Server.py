import os
import sys
import time
import numpy


class TxWaveform(object):

    def __init__(self,): pass

    def get_tx_seed(self,): return self.__tx_seed
    def get_tx_delay(self,): return self.__tx_delay
    def get_tx_phase(self,): return self.__tx_phase
    def get_tx_length(self,): return self.__tx_length
    def get_tx_period(self,): return self.__tx_period
    def get_tx_doppler(self,): return self.__tx_doppler
    def get_tx_amplitude(self,): return self.__tx_amplitude


class TxChannel(object):

    def __init__(self,):
        self.__waveforms = []

    def add_waveform(self,): pass
    def get_waveform(self,): pass
    def del_waveform(self,): pass

    def reset(self,): pass
    def verify(self,): pass



#class TxDevice(object): pass


#class RxDevice(object): pass
#class RxChannel(object): pass







class Server(object):

    def __init__(self,
            sdr_
            tx_devices,
            rx_devices,
        ):
        """
        """
        self.__init__tx_channels

        self.__dict_template_tx_usrp = {}
        self.__dict_template_tx_channel = {}
        self.__dict_template_tx_waveform = {}

    #def set_tx
    #def set_rx
    #def get_tx 
    #def get_rx
    #def add_tx
    #def add_rx
    #def set_tx_carrier(self,): pass

    #def enable_tx(self,device,channel): pass
    #def enable_rx(self,device,channel): pass
    #def get_tx_seed(self,device,channel): pass
    #def get_tx_delay(self,device,channel): pass
    #def set_tx_seed(self,device,channel): pass
    #def set_tx_delay(self,device,channel): pass
    #tx_seed = property(get_seed,set_seed)


    def add_tx_device(self,): pass
    def get_tx_device(self,): pass
    def del_tx_device(self,): pass

    def add_tx_waveform(self,): pass
    def get_tx_waveform(self,): pass
    def del_tx_waveform(self,): pass

    def add_rx_device(self,): pass
    def get_rx_device(self,): pass
    def del_rx_device(self,): pass





    #def stop(self,): pass
    #def start(self,): pass
    #def info(self,): pass
    #def verify(self,): pass

class Transmitter(object):

    def __init__(self,tx_devices):
        """
        """
        self.__tx_devices = tx_devices

    def get_tx_address(self,address,device): pass
    def set_tx_address(self,address,device): pass
    
    def set_tx_time_source(self,time_source,device): pass
    def set_tx_clock_source(self,clock_source,device): pass






    def tx_init(self,): pass
    def tx_start(self,): pass
    def tx_stop(self,): pass



if __name__ == "__main__":
    sys.exit()