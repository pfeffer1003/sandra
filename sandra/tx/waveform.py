###############################################################################
#                                                                             #
# FILE          :       waveform.py                                           #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
import sys
import numpy
#import sandra

###############################################################################

class Limit(object):

    def __init__(self,
            carrier,
            amplitude,
            bandwidth,
            dutycycle,
        ):

        self.__carrier = carrier
        self.__amplitude = amplitude
        self.__bandwidth = bandwidth
        self.__dutycycle = dutycycle

    def get_carrier(self,): return self.__carrier
    def get_amplitude(self,): return self.__amplitude
    def get_bandwidth(self,): return self.__bandwidth
    def get_dutycycle(self,): return self.__dutycycle

    carrier   = property(get_carrier)#,set_carrier)
    amplitude = property(get_amplitude)#,set_amplitude)
    bandwidth = property(get_bandwidth)#,set_bandwidth)
    dutycycle = property(get_dutycycle)#,set_dutycycle)




class Calibrate(object):

    def __init__(self,
            delay=0,
            phase=0.0,
            carrier=0.0,
            amplitude=0.0,
        ):
        pass



class Shape(object):

    def __init__(self,):
        pass


class Pulse(object):


    def __str__(self,):
        info  = ""
        return info

    def __init__(self,
            code,
            limit,
            calibrate,
        ):

        self.__code = code
        self.__limit = limit
        self.__calibrate = calibrate

        self.__delay = 0
        self.__phase = 0.0
        self.__doppler = 0.0
        self.__amplitude = 0.0


    def get_delay(self,): return self.__delay
    def get_phase(self,): return self.__phase
    def get_doppler(self,): return self.__doppler
    def get_amplitude(self,): return self.__amplitude

    def get_code(self,): return self.__code
    def get_limit(self,): return self.__limit
    def get_calibrate(self,): return self.__calibrate

    code = property(get_code)
    limit = property(get_limit)
    calibrate = property(get_calibrate)



#pulse.delay
#pulse.phase
#pulse.array
#pulse.period
#pulse.doppler
#pulse.amplitude
#
#pulse.code.type
#pulse.code.seed
#pulse.code.array
#pulse.code.length
#
#pulse.limit.amplitude
#pulse.limit.dutycycle
#
#pulse.calibrate.delay
#pulse.calibrate.phase
#pulse.calibrate.doppler
#pulse.calibrate.amplitude
#
#pulse.signal.delay
#pulse.signal.phase
#pulse.signal.doppler
#pulse.signal.amplitude






'''
pulse.delay
pulse.phase
pulse.period
pulse.doppler
pulse.rolloff

pulse.amplitude
pulse.amplitude.limit
pulse.amplitude.calibrate

pulse.dutycycle
pulse.symbolrate
pulse.samplerate
pulse.interpolation
'''



#pulse.shape.window



class Quantity(object):

    def __init__(self,dtype):

        self.__unit = str(unit)
        self.__dtype = dtype
        self.__options = []

    def get_name(self,): return self.__name
    def get_unit(self,): return self.__unit
    def get_dtype(self,): return self.__dtype
    def get_value(self,): return self.__value
    def get_limit(self,): return self.__limit
    def get_adjust(self,): return self.__adjust
    def get_offset(self,): return self.__offset
    def get_interval(self,): return self.__interval
    def get_options(self,): return self.__options


    name = property(get_name)
    unit = property(get_unit)
    dtype = property(get_dtype)
    value = property(get_value)

    limit = property(get_limit,set_limit)
    adjust = property(get_adjust,set_adjust)
    offset = property(get_offset,set_offset)
    interval = property(get_interval,set_interval)
    options = property(get_options,set_options)



    def verify(self,): pass