#!/usr/bin/env python

###############################################################################

import os
import sys
import numpy
import config
import xmlrpclib
import scipy.special

###############################################################################

from Verify import Verify

###############################################################################

class Config(object):

    # ----------------------------------------------------------------------- #

    __SYMBOL_DELAYS = {
        1000    :  4,
        5000    :  4,
        8000    :  4,
        10000   :  4,
        12500   :  4,
        20000   :  4,
        25000   :  5,
        40000   :  6,
        50000   :  6,
        100000  : 10,
        200000  : 13,
        250000  : 13,
        500000  : 13,
        625000  : 13,
        1000000 : 13,
        1250000 : 13,
        2500000 : 14,
        5000000 : 16,
    }

    __SYMBOL_RATES = [
        250,
        400,
        500,
        800,
        1000,
        2000,
        2500,
        4000,
        5000,
        8000, 
        10000,
        12500,
        20000,
        25000,
        40000,
        50000, 
        100000,
        200000,
        250000,
        500000,
        625000, 
        1000000,
        1250000,
        2500000,
        5000000
    ]

    __SYMBOL_SYNCSECONDS = [
        1,
        2,
        3,
        4,
        5,
        6,
        10,
        12,
        15,
        20,
        30,
        60,
    ]

    # ======================================================================= #

    def __init__(self):

        self._Data = config.Config()

    # ======================================================================= #

    def get_gps_lat              (self,  ): return self._Data.GlobalPosition.Latitude
    def get_gps_lon              (self,  ): return self._Data.GlobalPosition.Longitude
    def get_gps_alt              (self,  ): return self._Data.GlobalPosition.Altitude
    def get_code_type            (self,  ): return self._Data.Code.Type
    def get_code_length          (self,  ): return self._Data.Code.Length    
    def get_code_period          (self,  ): return self._Data.Code.Period
    def get_symbol_rate          (self,  ): return self._Data.Symbol.Rate
    def get_symbol_delay         (self,  ): return self._Data.Symbol.Delay
    def get_symbol_carrier       (self,  ): return self._Data.Symbol.Carrier
    def get_symbol_sync_second   (self,  ): return self._Data.Symbol.SyncSecond
    def get_limit_amplitude      (self,  ): return self._Data.Limits.Amplitude
    def get_control_mode         (self,  ): return self._Data.Control.Mode
    def get_control_host         (self,  ): return self._Data.Control.Host
    def get_control_port         (self,  ): return self._Data.Control.Port
    def get_usrp_cpu             (self,mb): return self._Data['Usrp'][mb]['Cpu']
    def get_usrp_route           (self,mb): return self._Data['Usrp'][mb]['Route']
    def get_usrp_address         (self,mb): return self._Data['Usrp'][mb]['Address']
    def get_usrp_time_source     (self,mb): return self._Data['Usrp'][mb]['TimeSource']
    def get_usrp_clock_source    (self,mb): return self._Data['Usrp'][mb]['ClockSource']
    def get_channel_seed         (self,ch): return self._Data['Channel'][ch]['Seed']
    def get_channel_phase        (self,ch): return self._Data['Channel'][ch]['Phase']
    def get_channel_antenna      (self,ch): return self._Data['Channel'][ch]['Antenna']
    def get_channel_amplitude    (self,ch): return self._Data['Channel'][ch]['Amplitude']
    def get_channel_stream_port  (self,ch): return self._Data['Channel'][ch]['StreamPort']

    def get_code_periods         (self,  ): return self._code_periods
    def get_symbol_taps          (self,  ): return self._symbol_taps
    def get_symbol_rates         (self,  ): return self.__SYMBOL_RATES
    def get_symbol_delays        (self,  ): return self.__SYMBOL_DELAYS
    def get_symbol_delay_init    (self,  ): return self._symbol_delay_init
    def get_symbol_sync_seconds  (self,  ): return self.__SYMBOL_SYNCSECONDS
    def get_symbol_interpolation (self,  ): return self._symbol_interpolation
    def get_usrp_channels        (self,  ): return self._usrp_channels
    def get_usrp_sample_rate     (self,  ): return self._usrp_sample_rate
    def get_usrp_motherboards    (self,  ): return self._usrp_motherboards
    def get_channel_codes        (self,  ): return self._channel_codes
    def get_channel_phasors      (self,  ): return self._channel_phasors
    def get_channel_patterns     (self,  ): return self._channel_patterns
    def get_channel_code         (self,ch): return self._channel_codes[ch]
    def get_channel_phasor       (self,ch): return self._channel_phasors[ch]
    def get_channel_pattern      (self,ch): return self._channel_patterns[ch]

    def get_usrp_cpus            (self,  ): return [self.get_usrp_cpu           (mb) for mb in xrange(self._usrp_motherboards)]
    def get_usrp_routes          (self,  ): return [self.get_usrp_route         (mb) for mb in xrange(self._usrp_motherboards)]
    def get_usrp_addresses       (self,  ): return [self.get_usrp_address       (mb) for mb in xrange(self._usrp_motherboards)]
    def get_usrp_time_sources    (self,  ): return [self.get_usrp_time_source   (mb) for mb in xrange(self._usrp_motherboards)]
    def get_usrp_clock_sources   (self,  ): return [self.get_usrp_clock_source  (mb) for mb in xrange(self._usrp_motherboards)]
    def get_channel_seeds        (self,  ): return [self.get_channel_seed       (ch) for ch in xrange(self._usrp_channels    )]
    def get_channel_phases       (self,  ): return [self.get_channel_phase      (ch) for ch in xrange(self._usrp_channels    )]
    def get_channel_antennas     (self,  ): return [self.get_channel_antenna    (ch) for ch in xrange(self._usrp_channels    )]
    def get_channel_amplitudes   (self,  ): return [self.get_channel_amplitude  (ch) for ch in xrange(self._usrp_channels    )]
    def get_channel_stream_ports (self,  ): return [self.get_channel_stream_port(ch) for ch in xrange(self._usrp_channels    )]

    # ======================================================================= #

    def set_gps_lat             (self,value   ): self._Data.GlobalPosition.Latitude      = value
    def set_gps_lon             (self,value   ): self._Data.GlobalPosition.Longitude     = value
    def set_gps_alt             (self,value   ): self._Data.GlobalPosition.Altitude      = value

    def set_code_type           (self,value   ): self._Data.Code.Type                    = value
    def set_code_length         (self,value   ): self._Data.Code.Length                  = value    
    def set_code_period         (self,value   ): self._Data.Code.Period                  = value

    def set_symbol_rate         (self,value   ): self._Data.Symbol.Rate                  = value
    def set_symbol_delay        (self,value   ): self._Data.Symbol.Delay                 = value
    def set_symbol_carrier      (self,value   ): self._Data.Symbol.Carrier               = value
    def set_symbol_sync_second  (self,value   ): self._Data.Symbol.SyncSecond            = value  

    def set_limit_amplitude     (self,value   ): self._Data.Limits.Amplitude             = value

    def set_control_mode        (self,value   ): self._Data.Control.Mode                 = value
    def set_control_host        (self,value   ): self._Data.Control.Host                 = value
    def set_control_port        (self,value   ): self._Data.Control.Port                 = value

    def set_usrp_cpu            (self,value,mb): self._Data['Usrp'][mb]['Cpu']           = value
    def set_usrp_route          (self,value,mb): self._Data['Usrp'][mb]['Route']         = value
    def set_usrp_address        (self,value,mb): self._Data['Usrp'][mb]['Address']       = value
    def set_usrp_time_source    (self,value,mb): self._Data['Usrp'][mb]['TimeSource']    = value
    def set_usrp_clock_source   (self,value,mb): self._Data['Usrp'][mb]['ClockSource']   = value

    def set_channel_seed        (self,value,ch): self._Data['Channel'][ch]['Seed']       = value
    def set_channel_phase       (self,value,ch): self._Data['Channel'][ch]['Phase']      = value
    def set_channel_antenna     (self,value,ch): self._Data['Channel'][ch]['Antenna']    = value
    def set_channel_amplitude   (self,value,ch): self._Data['Channel'][ch]['Amplitude']  = value
    def set_channel_stream_port (self,value,ch): self._Data['Channel'][ch]['StreamPort'] = value

    # ----------------------------------------------------------------------- #

    def _set_code_periods(self, sync_second, symbol_rate, code_type, code_length):
        
        value  = sync_second
        value *= symbol_rate
        code_periods = set()
        for i in range(1,int(numpy.sqrt(value))+1):
            div,mod = divmod(value,i)
            if mod == 0:
                code_periods |= {i,div}
        code_periods = sorted(code_periods)
        if code_type == 'complementary':
            code_periods = [code_period for code_period in code_periods if code_period%2 == 0]
        code_periods = numpy.int64(code_periods)
        code_periods = code_periods[code_periods>=code_length]
        code_periods = list(code_periods)
        self._code_periods = code_periods

    # ----------------------------------------------------------------------- #

    def _set_symbol_delay_init(self, symbol_rate):

        self._symbol_delay_init = -self.__SYMBOL_DELAYS[symbol_rate]

    # ----------------------------------------------------------------------- #

    def _set_symbol_taps(self, symbol_rate, interpolation):
        
        if symbol_rate >= 200000:
            taps = [1.0]
        else:
            rolloff = 0.518
            radix = int(numpy.ceil(numpy.log2(7.0*float(interpolation)+1.0)))
            sigma = rolloff/float(interpolation)/numpy.pi
            freqs = numpy.arange(-0.5,+0.5,1.0/2.0**radix,numpy.float32)
            freqs_pos = (numpy.copy(freqs)+0.5/float(interpolation))/sigma/numpy.sqrt(2.0)
            freqs_neg = (numpy.copy(freqs)-0.5/float(interpolation))/sigma/numpy.sqrt(2.0)
            idx_low = int(0.5*len(freqs))-int(3.5*float(interpolation))
            idx_upp = int(0.5*len(freqs))+int(3.5*float(interpolation))+1
            taps = scipy.special.erf(freqs_pos)-scipy.special.erf(freqs_neg)
            taps = numpy.fft.fftshift(numpy.fft.ifft(numpy.fft.fftshift(taps)))
            taps = taps.real[idx_low:idx_upp]*float(interpolation)/2.0
        taps = tuple(numpy.float64(taps).tolist())
        taps = numpy.float64(taps)
        self._symbol_taps = taps

    # ----------------------------------------------------------------------- #

    def _set_symbol_interpolation(self, symbol_rate, output_rate):

        self._symbol_interpolation  = output_rate
        self._symbol_interpolation /= symbol_rate  

    # ----------------------------------------------------------------------- #

    def _set_usrp_motherboards(self):

        self._usrp_motherboards = len(self._Data.Usrp)

    # ----------------------------------------------------------------------- #
    
    def _set_usrp_sample_rate(self, symbol_rate):

        if symbol_rate < 200000:
            self._usrp_sample_rate = 200000
        else:
            self._usrp_sample_rate = symbol_rate
    
    # ----------------------------------------------------------------------- #

    def _set_usrp_channels(self, motherboards):

        channels = len(self._Data.Channel)
        if not channels == motherboards:
            raise LookupError(
                "\n\tChannels 'count=%s' must be equal to USRP devices 'count=%s')." % (
                    channels,
                    motherboards,
                )
            )
        self._usrp_channels = channels

    # ----------------------------------------------------------------------- #

    def _set_channel_phasors(self, amplitudes, phases, channels):

        phasors = []
        for ch in xrange(channels):
            phasor  = phases[ch]*1.0j*numpy.pi/180.0
            phasor  = numpy.exp(phasor)
            phasor *= amplitudes[ch]
            phasor *= 1.0 # factor
            phasor  = numpy.complex64(phasor)
            phasors.append(phasor)
        self._channel_phasors = phasors

    # ----------------------------------------------------------------------- #

    def _set_channel_codes(self, channels, ctype, length, seeds):

        codes = []
        for c in xrange(channels):
            if ctype == 'cw':
                code = numpy.ones(length)
            if ctype == 'barker':
                if length == 2: code = numpy.float32([+1,-1])
                if length == 3: code = numpy.float32([+1,+1,-1])
                if length == 4: code = numpy.float32([+1,-1,+1,+1])
                if length == 5: code = numpy.float32([+1,+1,+1,-1,+1])
                if length == 7: code = numpy.float32([+1,+1,+1,-1,-1,+1,-1])
                if length == 11: code = numpy.float32([+1,+1,+1,-1,-1,-1,+1,-1,-1,+1,-1])
                if length == 13: code = numpy.float32([+1,+1,+1,+1,+1,-1,-1,+1,+1,-1,+1,-1,+1])
            if ctype == 'complementary':
                if length == 4:
                    code = numpy.float32([
                        [+1,+1,+1,-1],
                        [+1,+1,-1,+1],
                    ])
                if length == 8:
                    code = numpy.float32([
                        [+1,+1,+1,-1,+1,+1,-1,+1],
                        [+1,+1,+1,-1,-1,-1,+1,-1],
                    ])
                if length == 16:
                    code = numpy.float32([
                        [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1],
                        [+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
                    ])
                if length == 32:
                    code = numpy.float32([
                        [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
                        [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,-1,-1,-1,+1,-1,-1,+1,-1,+1,+1,+1,-1,-1,-1,+1,-1],
                    ])
            if ctype == 'pseudo':
                numpy.random.seed(seeds[c])
                code = numpy.random.random(length)
                code = numpy.exp(2.0*numpy.pi*1.0j*code)
                code = numpy.angle(code)
                code = -1.0*numpy.sign(code)
                code = code.real
                code = numpy.float32(code)
            codes.append(code)
        self._channel_codes = codes

    # ----------------------------------------------------------------------- #

    def _set_channel_patterns(self, channels, ctype, length, period, codes):

        patterns = []
        for c in xrange(channels):
            if ctype == 'complementary':
                pattern_a = numpy.zeros(period)
                pattern_b = numpy.zeros(period)
                pattern_a[0:length] = codes[c][0,:]
                pattern_b[0:length] = codes[c][1,:]
                pattern = numpy.append(
                    pattern_a,
                    pattern_b,
                )
            else:
                pattern = numpy.zeros(period)
                pattern[0:length] = codes[c]
            patterns.append(numpy.float32(pattern))
        self._channel_patterns = patterns

    # ======================================================================= #

    def __add__item(self,obj,val):      obj.append(val,'')
    def __add__dict(self,obj,name,val): obj.addMapping(name,val,'',False)

    # ======================================================================= #

    def load(self, filename):
        """
        """
        self._Data = config.Config(file(filename,'r'))
        self.verify()

    # ----------------------------------------------------------------------- #

    def save(self, filename):
        """
        """
        
        self.verify()

        Data                = config.Config()
        Data.GlobalPosition = config.Mapping()
        Data.Code           = config.Mapping()
        Data.Limits         = config.Mapping()
        Data.Symbol         = config.Mapping()
        Data.Control        = config.Mapping()
        Data.Usrp           = config.Sequence()
        Data.Channel        = config.Sequence()
        
        self.__add__dict( Data.GlobalPosition , 'Latitude'   , self.get_gps_lat            () )
        self.__add__dict( Data.GlobalPosition , 'Longitude'  , self.get_gps_lon            () )
        self.__add__dict( Data.GlobalPosition , 'Altitude'   , self.get_gps_alt            () )
        self.__add__dict( Data.Code           , 'Type'       , self.get_code_type          () )
        self.__add__dict( Data.Code           , 'Length'     , self.get_code_length        () )
        self.__add__dict( Data.Code           , 'Period'     , self.get_code_period        () )
        self.__add__dict( Data.Limits         , 'Amplitude'  , self.get_limit_amplitude    () )
        self.__add__dict( Data.Symbol         , 'Rate'       , self.get_symbol_rate        () )
        self.__add__dict( Data.Symbol         , 'Delay'      , self.get_symbol_delay       () )
        self.__add__dict( Data.Symbol         , 'Carrier'    , self.get_symbol_carrier     () )
        self.__add__dict( Data.Symbol         , 'SyncSecond' , self.get_symbol_sync_second () )
        self.__add__dict( Data.Control        , 'Mode'       , self.get_control_mode       () )
        self.__add__dict( Data.Control        , 'Host'       , self.get_control_host       () )
        self.__add__dict( Data.Control        , 'Port'       , self.get_control_port       () )
        
        for mb in xrange(self.get_usrp_motherboards()):
            tmp = config.Mapping()
            self.__add__dict( tmp , 'Cpu'         , self.get_usrp_cpu            (mb) )
            self.__add__dict( tmp , 'Route'       , self.get_usrp_route          (mb) )
            self.__add__dict( tmp , 'Address'     , self.get_usrp_address        (mb) )
            self.__add__dict( tmp , 'TimeSource'  , self.get_usrp_time_source    (mb) )
            self.__add__dict( tmp , 'ClockSource' , self.get_usrp_clock_source   (mb) )
            self.__add__item( Data.Usrp,dict(tmp))
        
        for ch in xrange(self.get_usrp_channels()):
            tmp = config.Mapping()
            self.__add__dict( tmp , 'Seed'        , self.get_channel_seed        (ch) )
            self.__add__dict( tmp , 'Phase'       , self.get_channel_phase       (ch) )
            self.__add__dict( tmp , 'Antenna'     , self.get_channel_antenna     (ch) )
            self.__add__dict( tmp , 'Amplitude'   , self.get_channel_amplitude   (ch) )
            self.__add__dict( tmp , 'StreamPort'  , self.get_channel_stream_port (ch) )
            self.__add__item( Data.Channel,dict(tmp))
        
        Data.save(file(name=filename,mode='w'))

    # ----------------------------------------------------------------------- #

    def create(self, filename, usrp_channels):

        usrp_motherboards = usrp_channels

        self._Data                = config.Config()
        self._Data.GlobalPosition = config.Mapping()
        self._Data.Code           = config.Mapping()
        self._Data.Limits         = config.Mapping()
        self._Data.Symbol         = config.Mapping()
        self._Data.Control        = config.Mapping()
        self._Data.Usrp           = config.Sequence()
        self._Data.Channel        = config.Sequence()

        self.__add__dict( self._Data.GlobalPosition , 'Latitude'   , 0.0         )
        self.__add__dict( self._Data.GlobalPosition , 'Longitude'  , 0.0         )
        self.__add__dict( self._Data.GlobalPosition , 'Altitude'   , 0.0         )
        self.__add__dict( self._Data.Code           , 'Type'       , 'cw'        )
        self.__add__dict( self._Data.Code           , 'Length'     , 1           )
        self.__add__dict( self._Data.Code           , 'Period'     , 100         )
        self.__add__dict( self._Data.Limits         , 'Amplitude'  , 0.0         )
        self.__add__dict( self._Data.Symbol         , 'Rate'       , 100000      )
        self.__add__dict( self._Data.Symbol         , 'Delay'      , 0           )
        self.__add__dict( self._Data.Symbol         , 'Carrier'    , 32550000.0  )
        self.__add__dict( self._Data.Symbol         , 'SyncSecond' , 1           )
        self.__add__dict( self._Data.Control        , 'Mode'       , 'operate'   )
        self.__add__dict( self._Data.Control        , 'Host'       , 'localhost' )
        self.__add__dict( self._Data.Control        , 'Port'       , 6000        )

        for mb in xrange(usrp_motherboards):
            tmp = config.Mapping()
            self.__add__dict( tmp , 'Cpu'         , 0                      )
            self.__add__dict( tmp , 'Route'       , 'A:A'                  )
            self.__add__dict( tmp , 'Address'     , '192.168.10.%s'%(mb+2) )
            self.__add__dict( tmp , 'TimeSource'  , 'external'             )
            self.__add__dict( tmp , 'ClockSource' , 'external'             )
            self.__add__item( self._Data.Usrp,dict(tmp))

        for ch in xrange(usrp_channels):
            tmp = config.Mapping()
            self.__add__dict( tmp , 'Seed'        , 0             )
            self.__add__dict( tmp , 'Phase'       , 0.0           )
            self.__add__dict( tmp , 'Antenna'     , [0.0,0.0,0.0] )
            self.__add__dict( tmp , 'Amplitude'   , 0.0           )
            self.__add__dict( tmp , 'StreamPort'  , 6001+ch       )
            self.__add__item( self._Data.Channel,dict(tmp))

        self.save(filename)

    # ----------------------------------------------------------------------- #

    @staticmethod
    def edit(filename):
        """
        """
        os.system("nano %s" % filename)

    # ----------------------------------------------------------------------- #
    
    def verify(self):
        """
        """

        Verify.number(self.get_limit_amplitude(),'LimitAmplitude',float,"[",0.0,1.0,"]",[])
        Verify.number(self.get_gps_lat   (),'Latitude'      ,float,"[",-90.0,+90.0,"]",[])
        Verify.number(self.get_gps_lon  (),'Longitude'     ,float,"[",-180.0,+180.0,"]",[])
        Verify.number(self.get_gps_alt   (),'Altitude'      ,float,"(","-inf","+inf",")",[])
        Verify.string(self.get_code_type      (),'CodeType',['cw','mono','barker','pseudo','complementary'])
        
        if self.get_code_type() == 'cw'            : Verify.number(self.get_code_length(),'CodeLength',int,"[",1,"+inf",")",[])
        if self.get_code_type() == 'barker'        : Verify.number(self.get_code_length(),'CodeLength',int,"[",2,13,"]",[2,3,4,5,7,11,13])
        if self.get_code_type() == 'complementary' : Verify.number(self.get_code_length(),'CodeLength',int,"[",2,32,"]",[2,4,8,16,32])
        if self.get_code_type() == 'pseudo'        : Verify.number(self.get_code_length(),'CodeLength',int,"[",1,"+inf",")",[])

        Verify.number(self.get_symbol_rate        (),'SymbolRate'   ,int,  "[",200,5000000,"]",self.__SYMBOL_RATES)
        Verify.number(self.get_symbol_carrier     (),'SymbolCarrier',float,"[",0.0,"+inf",")",[])
        Verify.number(self.get_symbol_sync_second (),'SyncSecond'   ,int,  "[",1,60,"]",self.__SYMBOL_SYNCSECONDS)

        self._set_usrp_motherboards()
        self._set_usrp_sample_rate(self.get_symbol_rate())
        self._set_usrp_channels(self.get_usrp_motherboards())
        
        self._set_symbol_interpolation(self.get_symbol_rate(),self.get_usrp_sample_rate())
        self._set_symbol_taps(self.get_symbol_rate(),self.get_symbol_interpolation())
        self._set_symbol_delay_init(self.get_symbol_rate())

        self._set_code_periods(self.get_symbol_sync_second(),self.get_symbol_rate(),self.get_code_type(),self.get_code_length())

        Verify.number(self.get_code_period           (),'CodePeriod',int,"[",self.get_code_length(),"+inf",")",self.get_code_periods())
        Verify.number(self.get_symbol_delay          (),'SymbolDelay',int,"[",-self.get_code_period(),+self.get_code_period(),"]",[])
        Verify.string(self.get_control_mode          (),'Mode',['calibrate','simulate','operate'])
        Verify.ip    (self.get_control_host      (),"Host")
        Verify.number(self.get_control_port      (),"Port",int,"[",1024,32768,")",[])

        for mb in xrange(self._usrp_motherboards):
            Verify.number(self.get_usrp_cpu(mb),"Cpu[%s]"%(mb+1),int,"[",0,24,"]",[])
            Verify.ip(self.get_usrp_address(mb),"Address[%s]" % (mb+1))
            Verify.string(self.get_usrp_route(mb),"Route[%s]" % (mb+1),['A:A','A:B','A:AB','A:BA'])
            Verify.string(self.get_usrp_clock_source(mb),"ClockSource[%s]" % (mb+1),['internal','external','mimo'])
            
            if self.get_usrp_clock_source(mb) == 'internal':
                Verify.string(self.get_usrp_time_source(mb),"TimeSource[%s]" % (mb+1),['none'])
            else:
                Verify.string(self.get_usrp_time_source(mb),"TimeSource[%s]" % (mb+1),[self.get_usrp_clock_source(mb)])

        for c in xrange(self._usrp_channels):
            
            Verify.number(self.get_channel_seed(c),"Seed[%s]" % (c+1),int,"[",0,'+inf',")",[])
            Verify.number(self.get_channel_phase(c),"Phase[%s]" % (c+1),float,"[",-180.0,+180.0,"]",[])
            Verify.number(self.get_channel_amplitude(c),"Amplitude[%s]" % (c+1),float,"[",0.0,self.get_limit_amplitude(),"]",[])
            Verify.number(self.get_channel_stream_port(c),"StreamPort[%s]" % (c+1),int,"[",1024,32768,")",[])
            Verify.struct(list(self.get_channel_antenna(c)),"Antenna[%s]" % (c+1),list,3)
            
            for i,item in enumerate(list(self.get_channel_antenna(c))):
            
                Verify.number(item,"Antenna[%s][%s]" % (c+1,i+1),float,"(","-inf","+inf",")",[])               

        self._set_channel_codes(self.get_usrp_channels(),self.get_code_type(),self.get_code_length(),self.get_channel_seeds())
        self._set_channel_phasors(self.get_channel_amplitudes(),self.get_channel_phases(),self.get_usrp_channels())
        self._set_channel_patterns(self.get_usrp_channels(),self.get_code_type(),self.get_code_length(),self.get_code_period(),self.get_channel_codes())

    # ======================================================================= #



if __name__ == "__main__":
    
    cfg = Config()
    cfg.load(filename='/home/radar/.simone-tx/cfg/main.cfg')

    print cfg.get_channel_code(1)
    print cfg.get_channel_pattern(1)

    #cfg.create(filename='/home/radar/Schreibtisch/cfg_test.cfg',channels=6)

    sys.exit()
