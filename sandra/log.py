###############################################################################
#                                                                             #
# FILE          :       log.py                                            #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
import sys
import sandra
import logging
import logging.handlers
import colorlog

###############################################################################

def info(mode,config):
    
    """
    """
    
    os.system(
        "tail -n 100 %s/%s/%s.log"%(
            sandra.DIRS['log'],
            mode,
            config,
        )
    )

###############################################################################

def stream(mode,config):
    
    """
    """
    
    os.system(
        "tail -f %s/%s/%s.log"%(
            sandra.DIRS['log'],
            mode,
            config,
        )
    )

###############################################################################

def file(mode,config):
    
    """
    """
    
    filename   = sandra.DIRS['log']
    filename  += "/%s/%s.log"%(mode,config)
    
    formatter  = ''
    formatter += '%(asctime)-15s '
    formatter += '%(levelname)-8s '
    formatter += '%(message)s'
    formatter  = logging.Formatter(formatter)
    
    handler = logging.handlers.TimedRotatingFileHandler(
        filename=filename,
        when="midnight",
        interval=1,
        backupCount=7,
        encoding=None,
        delay=False,
        utc=True)
    handler.setFormatter(formatter)
    
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    log.addHandler(handler)
    
    return log

###############################################################################

def terminal():
    
    """
    """
    
    formatter  = "  %(log_color)s%(levelname)-8s%(reset)s | "
    formatter += "%(log_color)s%(message)s%(reset)s"
    formatter  = colorlog.ColoredFormatter(
        formatter,
        log_colors={
            'DEBUG'    : 'blue',
            'INFO'     : 'blue',
            'WARNING'  : 'yellow',
            'ERROR'    : 'red',
            'CRITICAL' : 'bold_red',
        }
    )
    
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)
    
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    log.addHandler(handler)
    
    return log

###############################################################################