###############################################################################
#                                                                             #
# FILE          :       variables.py                                          #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
import sys
import numpy
import intervals
#import sandra

###############################################################################

class Quantity(object):

    # ----------------------------------------------------------------------- #

    def __str__(self,):
        info  = ""
        return info

    # ----------------------------------------------------------------------- #

    def __init__(self,name='Default',unit='a.u.',dtype=int,value=0):
        
        """
        """
        

        self.__init__name(name)
        self.__init__unit(unit)
        self.__init__dtype(dtype)
        #self.__init__value(value)
        
        self.__adjust = 0

        #self.__value    =  self.__dtype(0)
        #self.__bounds   =  'open'
        #self.__maximum  =  intervals.inf
        #self.__minimum  = -intervals.inf
        #self.__interval =  intervals.open(self.__minimum,self.__maximum)

    # ----------------------------------------------------------------------- #

    def __init__name(self,name):
        """
        """
        if not isinstance(name,str):
            raise TypeError()
        self.__name = name
    
    def get_name(self,):
        """
        """
        return self.__name

    def set_name(self,name):
        """
        """
        self.__init__name(name)

    name = property(get_name,set_name)

    # ----------------------------------------------------------------------- #

    def __init__unit(self,unit):
        """
        """
        if not isinstance(unit,str):
            raise TypeError()
        self.__unit = unit

    def get_unit(self,):
        """
        """
        return self.__unit

    def set_unit(self,unit):
        """
        """
        self.__init__unit(unit)

    unit = property(get_unit,set_unit)

    # ----------------------------------------------------------------------- #

    def __init__dtype(self,dtype):
        """
        """
        if not dtype in [int,float,long]:
            raise ValueError()
        self.__dtype = dtype

    def get_dtype(self,):
        """
        """
        return self.__dtype

    dtype = property(get_dtype)

    # ----------------------------------------------------------------------- #  

###############################################################################

class Option(Quantity):

    # ----------------------------------------------------------------------- #

    def __str__(self,):
        info  = ""
        return info

    # ----------------------------------------------------------------------- #

    def __init__(self,name='Default',unit='a.u.',dtype=int,value=0):
        """
        """
        Quantity.__init__(self,name,unit,dtype)

    # ----------------------------------------------------------------------- #

###############################################################################

class Interval(Quantity):

    # ----------------------------------------------------------------------- #

    def __str__(self,):
        info  = ""
        return info

    # ----------------------------------------------------------------------- #

    def __init__(self,name='Default',unit='a.u.',dtype=int,value=0):
        """
        """
        Quantity.__init__(self,name,unit,dtype)

    # ----------------------------------------------------------------------- #


###############################################################################


o = Option()
print o.name
print o.unit
print o.dtype


'''
###############################################################################

class Quantity(object):

    # ----------------------------------------------------------------------- #

    def __str__(self,):
        info  = ""
        return info

    # ----------------------------------------------------------------------- #

    def __init__(self,name='Default',unit='a.u.',dtype=int):
        
        """
        """
        
        self.__init__name(name)
        self.__init__unit(unit)
        self.__init__dtype(dtype)
        
        #self.__value    =  self.__dtype(0)
        #self.__bounds   =  'open'
        #self.__maximum  =  intervals.inf
        #self.__minimum  = -intervals.inf
        #self.__interval =  intervals.open(self.__minimum,self.__maximum)

    # ----------------------------------------------------------------------- #

    def __init__name(self,name):
        """
        """
        if not isinstance(name,str):
            raise TypeError()
        self.__name = name
    
    def get_name(self,):
        """
        """
        return self.__name

    def set_name(self,name):
        """
        """
        self.__init__name(name)

    name = property(get_name,set_name)

    # ----------------------------------------------------------------------- #

    def __init__unit(self,unit):
        """
        """
        if not isinstance(unit,str):
            raise TypeError()
        self.__unit = unit

    def get_unit(self,):
        """
        """
        return self.__unit

    def set_unit(self,unit):
        """
        """
        self.__init__unit(unit)

    unit = property(get_unit,set_unit)

    # ----------------------------------------------------------------------- #

    def __init__dtype(self,dtype):
        """
        """
        if not dtype in [int,float,long]:
            raise ValueError()
        self.__dtype = dtype

    def get_dtype(self,):
        """
        """
        return self.__dtype

    dtype = property(get_dtype)

    # ----------------------------------------------------------------------- #

    #def get_value(self,): return self.__dtype
    #def get_maximum(self,): return self.__maximum
    #def get_minimum(self,): return self.__minimum
    #def get_interval(self,): return self.__interval
    #def get_bounds(self,): return self.__bounds # open, closed, openclosed, closedopen


    #def set_bounds(self,bounds):
    #    """
    #    """
    #    if bounds == 'open': 
    #    if bounds == 'openclosed'
    #    if bounds == 'closed': 
    #    if bounds == 'closedopen': 


    #interval = property(get_interval)

###############################################################################

class Delay(Quantity):

    def __init__(self,name='Delay',unit='Samples'):
        Quantity.__init__(self,name=name,unit=unit,dtype=int)

class Phase(Quantity):

    def __init__(self,name='Phase',unit='deg'):
        Quantity.__init__(self,name=name,unit=unit,dtype=float)

class Doppler(Quantity):

    def __init__(self,name='Doppler',unit='Hz'):
        Quantity.__init__(self,name=name,unit=unit,dtype=float)

class Amplitude(Quantity):

    def __init__(self,name='Amplitude',unit='a.u.'):
        Quantity.__init__(self,name=name,unit=unit,dtype=float)

class Power(Quantity):

    def __init__(self,name='Power',unit='dB'):
        Quantity.__init__(self,name=name,unit=unit,dtype=float)


###############################################################################


q = Quantity()

q.name = 'Hello'
q.unit = 'm'

print q.name
print q.unit
print q.dtype

a = Amplitude()
print a.unit
print a.dtype
print a.name
'''


'''
q = Quantity()
q.name 
q.unit
q.dtype
q.value
q.offset
q.maximum
q.minimum
q.interval
'''

###############################################################################

'''
o = Options()
o.name
o.unit
o.value
o.dtype
o.value
o.options
'''

###############################################################################


adjust = 1.2
offset = 0.3
maximum = 0.8

value = adjust-offset
print value