###############################################################################
#                                                                             #
# FILE          :       system.py                                             #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
import sys

###############################################################################

def list(mode,config):
    
    """
    """
    
    os.system(
        'systemctl --user list-units sandra-%s@%s.service --no-pager'%(
            mode,
            config,
        )
    )

###############################################################################

def stop(mode,config):
    
    """
    """
    
    os.system(
        'systemctl --user stop sandra-%s@%s.service'%(
            mode,
            config,
        )
    )

###############################################################################

def start(mode,config):
    
    """
    """
    
    os.system(
        'systemctl --user start sandra-%s@%s.service'%(
            mode,
            config,
        )
    )

###############################################################################

def status(mode,config):
    
    """
    """
    
    os.system(
        'systemctl --user status sandra-%s@%s.service --no-pager'%(
            mode,
            config,
        )
    )

###############################################################################

def reload():
    
    """
    """
    
    os.system('systemctl --user daemon-reload')

###############################################################################

def enable(mode,config):
    
    """
    """
    
    os.system(
        'systemctl --user enable sandra-%s@%s.service'%(
            mode,
            config,
        )
    )

###############################################################################

def disable(mode,config):
    
    """
    """
    
    os.system(
        'systemctl --user disable sandra-%s@%s.service'%(
            mode,
            config,
        )
    )

###############################################################################

def restart(mode,config):
    
    """
    """
    
    os.system(
        'systemctl --user restart sandra-%s@%s.service'%(
            mode,
            config,
        )
    )

###############################################################################

def journal(mode,config):
    
    """
    """
    
    os.system(
        'journalctl --user-unit sandra-%s@%s.service -n 100 --no-pager'%(
            mode,
            config,
        )
    )

###############################################################################

def enabled(mode,config):
    
    """
    """
    
    return None

###############################################################################

def started(mode,config):
    
    """
    """
    
    return None

###############################################################################