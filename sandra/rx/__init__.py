###############################################################################
#                                                                             #
# FILE          :       __init__.py                                           #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

#from Config import Config
#from DigitalRfDiskUsage import DigitalRfDiskUsage

###############################################################################

SYMBOL_DELAYS = {
    'USRP' : {
        'N200' : {
            5000    : 4  , 8000    : 4  , 10000   : 4  ,
            12500   : 4  , 20000   : 5  , 25000   : 5  ,
            40000   : 5  , 50000   : 7  , 100000  : 9  ,
            200000  : 10 , 250000  : 11 , 500000  : 11 ,
            625000  : 11 , 1000000 : 11 , 1250000 : 12 ,
            2500000 : 12 , 5000000 : 13 ,
        },
        'N210' : {
            5000    : 4  , 8000    : 4  , 10000   : 4  ,
            12500   : 4  , 20000   : 5  , 25000   : 5  ,
            40000   : 5  , 50000   : 7  , 100000  : 9  ,
            200000  : 10 , 250000  : 11 , 500000  : 11 ,
            625000  : 11 , 1000000 : 11 , 1250000 : 12 ,
            2500000 : 12 , 5000000 : 13 ,
        },
    },
}

###############################################################################

SYMBOL_RATES = {
    'USRP' : {
        'N200' : sorted(SYMBOL_DELAYS['USRP']['N200'].keys()),
        'N210' : sorted(SYMBOL_DELAYS['USRP']['N210'].keys()),
    },
}

###############################################################################