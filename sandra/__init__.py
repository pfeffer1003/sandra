###############################################################################
#                                                                             #
# FILE          :       __init__.py                                           #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
#import tx
#import rx
#import trx
import log
import system
import verify
#import variables

###############################################################################

MODES = ['tx','rx','trx']

###############################################################################

DIRS = {
    'home'          : os.getenv('HOME'),
    'venv'          : os.getenv('HOME')+"/.sandra",
    'bin'           : os.getenv('HOME')+"/.sandra/bin",
    'cfg'           : os.getenv('HOME')+"/.sandra/cfg",
    'cfg/tx'        : os.getenv('HOME')+"/.sandra/cfg/tx",
    'cfg/rx'        : os.getenv('HOME')+"/.sandra/cfg/rx",
    'cfg/trx'       : os.getenv('HOME')+"/.sandra/cfg/trx",
    'log'           : os.getenv('HOME')+"/.sandra/log",
    'log/tx'        : os.getenv('HOME')+"/.sandra/log/tx",
    'log/rx'        : os.getenv('HOME')+"/.sandra/log/rx",
    'log/trx'       : os.getenv('HOME')+"/.sandra/log/trx",
    'systemctl'     : os.getenv('HOME')+"/.config/systemctl/user",
}

###############################################################################

FILES = {
    'activate'      : DIRS['bin']+"/activate",
    'activate_this' : DIRS['bin']+"/activate_this.py",
    'systemctl/tx'  : DIRS['systemctl']+"/sandra-tx@.service",
    'systemctl/rx'  : DIRS['systemctl']+"/sandra-rx@.service",
    'systemctl/trx' : DIRS['systemctl']+"/sandra-trx@.service",
}

###############################################################################

SYMBOL_SYNCSECONDS = {
    'USRP' : {
        'N200' : [1,2,3,4,5,6,10,12,15,20,30,60],
        'N210' : [1,2,3,4,5,6,10,12,15,20,30,60],
    },
}

###############################################################################
