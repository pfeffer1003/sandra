###############################################################################
#                                                                             #
# FILE          :       verify.py                                             #
# AUTHOR        :       M.Eng. Nico Pfeffer                                   #
# E-MAIL        :       pfeffer@iap-kborn.de                                  #
# INSTITUTION   :       Leibniz-Institute of Atmospheric Physics              #
# DEPARTMENT    :       Radar Remote Sensing                                  #
# GROUP         :       Radio Science                                         #
# COUNTRY       :       Germany                                               #
# STATE         :       Mecklenburg-Vorpommern                                #
# LOCATION      :       Kuehlungsborn                                         #
# POSTAL        :       18225                                                 #
# ADDRESS       :       Schlossstr. 6                                         #
#                                                                             #
###############################################################################

import os
import sys
import intervals

###############################################################################

from IPy import IP

###############################################################################

def BOOL(name='Default',value=True):
    
    """
    """
    
    if not isinstance(name,str):
        raise ValueError()
    if not isinstance(value,bool):
        raise TypeError(
            "\n\t%s: 'value=%s' has %s, but must be %s!" % (
                name,
                value,
                type(value),
                bool,
            )
        )
    if not value in [True,False]:
        raise ValueError(
            "\n\t%s: 'value=%s' not in 'choices=%s'!" % (
                name,
                value,
                [True,False],
            )
        )

    return value

###############################################################################

def NUMBER(
        name='Default',
        value=0,
        dtype=int,
        left="(",
        lower="-inf",
        upper="+inf",
        right=")",
        choices=[],
    ):
    
    """
    """

    if not isinstance(name,str):
        raise ValueError()
    if not dtype in [int,long,float]:
        raise ValueError()
    if not left in ["[","("]:
        raise ValueError(
            "\n\t%s: 'left=%s' must be '[' or '('" %(
                name,
                left,
            )
        )
    else:
        if left == "[": left = True
        if left == "(": left = False
    if not right in ["]",")"]:
        raise ValueError(
            "\n\t%s: 'right=%s' must be ']' or ')'" %(
                name,
                right,
            )
        )
    else:
        if right == "]": right = True
        if right == ")": right = False
    if lower == '-inf':
        lower = -intervals.inf
    else:
        if not isinstance(lower,dtype):
            raise TypeError(
                "\n\t%s: 'lower=%s' has %s, but must be %s!" % (
                    name,
                    lower,
                    type(lower),
                    dtype,
                )
            )
    if upper == '+inf':
        upper = intervals.inf
    else:
        if not isinstance(upper,dtype):
            raise TypeError(
                "\n\t%s: 'upper=%s' has %s, but must be %s!" % (
                    name,
                    upper,
                    type(upper),
                    dtype,
                )
            )
    interval = intervals.AtomicInterval(
        left=left,
        lower=lower,
        upper=upper,
        right=right,
    )
    if not isinstance(choices,list):
        raise TypeError(
            "\n\t%s: 'choices=%s' has %s, but must be %s!" % (
                name,
                choices,
                type(choices),
                list,
            )
        )
    if choices != []:
        for index,choice in enumerate(choices):
            if not isinstance(choice,dtype):
                raise TypeError(
                    "\n\t%s: 'choices[%s]=%s' has %s, but must be %s!" % (
                        name,
                        index,
                        choice,
                        type(choice),
                        dtype,
                    )
                )
            if not choice in interval:
                raise ValueError(
                    "\n\t%s: 'choices[%s]=%s' not in 'interval=%s'!" % (
                        name,
                        index,
                        choice,
                        interval,
                    )
                )
    if not isinstance(value,dtype):
        raise TypeError(
            "\n\t%s: 'value=%s' has %s, but must be %s!" % (
                name,
                value,
                type(value),
                dtype,
            )
        )
    if not value in interval:
        raise ValueError(
            "\n\t%s: 'value=%s' not in 'interval=%s'!" % (
                name,
                value,
                interval,
            )
        )
    if choices != []:
        if not value in choices:
            raise ValueError(
                "\n\t%s: 'value=%s' not in 'choices=%s'!" % (
                    name,
                    value,
                    choices,
                )
            )

    return value

###############################################################################

def STRING(name="Default",value="HelloWorld",choices=[]):
    
    """
    """
    
    if not isinstance(name,str):
        raise TypeError()       
    if not isinstance(choices,list):
        raise TypeError(
            "\n\t%s: 'choices=%s' has %s, but must be %s!" % (
                name,
                choices,
                type(choicess),
                list,
            )
        )
    if choices != []:
        for index,choice in enumerate(choices):
            if not isinstance(choice,str):
                raise TypeError(
                    "\n\t%s: 'choices[%s]=%s' has %s, but must be %s!" % (
                        name,
                        index,
                        choice,
                        type(choice),
                        str,
                    )
                )
    if not isinstance(value,str):
        raise TypeError(
            "\n\t%s: 'value=%s' has %s, but must be %s!" % (
                name,
                value,
                type(value),
                str,
            )
        )
    if choices != []:
        if not value in choices:
            raise ValueError(
                "\n\t%s: 'value=%s' not in 'choices=%s'!" % (
                    name,
                    value,
                    choices,
                )
            )

    return value

###############################################################################

def IPV4(name,value):

    """
    """

    if value == 'localhost':
        value = '127.0.0.1'
    else:
        IP(value)
    
    return value

###############################################################################

def STRUCT(name,value,dtype,length=None):

    """
    """

    if not isinstance(name,str):
        raise TypeError()
    if dtype not in [set,list,tuple]:
        raise ValueError()
    if length is not None:
        if not isinstance(length,int):
            raise TypeError()
        if length < 1:
            raise ValueError()
    if not isinstance(value,dtype):
        raise TypeError(
            "\n\t%s: 'value=%s' has %s, but must be %s!" % (
                name,
                value,
                type(value),
                dtype,
            )
        )
    if length is not None:
        if length != len(value):
            raise LookupError(
                "\n\t%s: 'value=%s' has 'length=%s', but needs 'length=%s'!" % (
                    name,
                    value,
                    len(value),
                    length,
                )               
            )

    return value

###############################################################################

def INT(name,value,left,lower,upper,right,choices=[]):
    
    """
    """
    
    return NUMBER(name,value,int,left,lower,upper,right,choices)

###############################################################################

def LONG(name,value,left,lower,upper,right,choices=[]):

    """
    """

    return NUMBER(name,value,long,left,lower,upper,right,choices)

###############################################################################

def FLOAT(name,value,left,lower,upper,right,choices=[]):

    """
    """

    return NUMBER(name,value,float,left,lower,upper,right,choices)

###############################################################################

def SET(name,value,length=None):

    """
    """

    return STRUCT(name,value,set,length)

###############################################################################

def LIST(name,value,length=None):

    """
    """

    return STRUCT(name,value,list,length)

###############################################################################

def TUPLE(name,value,length=None):

    """
    """

    return STRUCT(name,value,tuple,length)

###############################################################################