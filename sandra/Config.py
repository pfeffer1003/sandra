import os
import sys
import yaml
import config
from collections import OrderedDict





class ConfigFile(object):

    def __init__(self,):
        pass

    def create(self,filename,sdr,devices=1,waveforms=1):

        if sdr == 'USRP-N200':
            tx_channels = 1
            rx_channels = 2
            tx_subdevspecs = ['A:A']
            rx_subdevspecs = ['A:A','A:B']

        if sdr == 'USRP-N210':
            tx_channels = 1
            rx_channels = 2

        if sdr == 'USRP-X300':
            tx_channels = 2
            rx_channels = 2

        self.__Data = config.Config()

        self.__Data.Location = config.Mapping()
        self.__Data.Location.addMapping('Latitude',0.0,'',False)
        self.__Data.Location.addMapping('Longitude',0.0,'',False)
        self.__Data.Location.addMapping('Altitude',0.0,'',False)
        

        #self.__Data.Code = config.Mapping()
        #self.__Data.Code.addMapping('Type','cw','',False)
        #self.__Data.Code.addMapping('Length',1,'',False)
        #self.__Data.Code.addMapping('Period',100,'',False)

        #self.__Data.Limits = config.Mapping()
        #self.__Data.Limits.addMapping('Amplitude',0.0,'',False)
        #self.__Data.Limits.addMapping('DutyCycle',100.0,'',False)
        #self.__Data.Limits.addMapping('Bandwidth',2000000.0,'',False)
        #self.__Data.Limits.addMapping('CenterFrequency',32000000.0,'',False)

        #self.__Data.Symbol = config.Mapping()
        #self.__Data.Symbol.addMapping('Rate',100000,'',False)
        #self.__Data.Symbol.addMapping('Delay',0,'',False)
        #self.__Data.Symbol.addMapping('Carrier',32550000.0,'',False)
        #self.__Data.Symbol.addMapping('SyncSecond',1,'',False)

        self.__Data.XmlRpc = config.Mapping()
        self.__Data.XmlRpc.addMapping('Mode','operate','',False)
        self.__Data.XmlRpc.addMapping('Host','localhost','',False)
        self.__Data.XmlRpc.addMapping('Port',6000,'',False)
 
        self.__Data.Usrp = config.Sequence()
        for mb in xrange(devices):
            usrp = config.Mapping()
            usrp.addMapping('Enable',True,'',False)
            usrp.addMapping('Affinity',[0],'',False)
            usrp.addMapping('Address','192.168.10.%s'%(mb+2),'',False)
            usrp.addMapping('OtwFormat','sc16','',False)
            usrp.addMapping('CpuFormat','fc32','',False)
            usrp.addMapping('TimeSource','external','',False)
            usrp.addMapping('ClockSource','external','',False)
            usrp.Transmitter = config.Sequence()
            for ch in xrange(tx_channels):
                usrp_tx_channel = config.Mapping()
                usrp_tx_channel.addMapping('Enable',True,'',False)
                usrp_tx_channel.addMapping('SubDevSpec','A:A','',False)
                usrp_tx_channel.addMapping('Code','cw','',False)
                usrp_tx_channel.addMapping('Length',10,'',False)
                usrp_tx_channel.addMapping('Period',100,'',False)
                usrp_tx_channel.addMapping('Seed',0,'',False)
                usrp_tx_channel.addMapping('Delay',0,'',False)
                usrp_tx_channel.addMapping('Phase',0.0,'',False)
                usrp_tx_channel.addMapping('Doppler',0.0,'',False)
                usrp_tx_channel.addMapping('Amplitude',0.0,'',False)
                usrp_tx_channel.addMapping('StreamPort',6001+ch,'',False)
                usrp.Transmitter.append(usrp_tx_channel,'')
            usrp.Receiver = config.Sequence()
            #usrp.addMapping('Transmitter',[],'',False)
            #usrp.addMapping('Receiver',[],'',False)

            #usrp_tx = config.Sequence()
            #for ch in xrange(tx_channels):
            #    usrp_tx_channel = config.Mapping()
            #usrp_rx = config.Sequence()
            #for ch in xrange(rx_channels):
            #    usrp_rx_channel = config.Mapping()
            self.__Data.Usrp.append(usrp,'')

        #self.__Data.Usrp = config.Sequence()
        #for mb in xrange(devices):
        #    tmp = config.Mapping()
        #    self.__add__dict( tmp , 'Cpu'         , 0                      )
        #    self.__add__dict( tmp , 'Route'       , 'A:A'                  )
        #    self.__add__dict( tmp , 'Address'     , '192.168.10.%s'%(mb+2) )
        #    self.__add__dict( tmp , 'TimeSource'  , 'external'             )
        #    self.__add__dict( tmp , 'ClockSource' , 'external'             )
        #    self.__Data.Usrp.append(dict(tmp),'')

        #self.__Data.Channel = config.Sequence()
        #for ch in xrange(devices):
        #    tmp = config.Mapping()
        #    self.__add__dict( tmp , 'Seed'        , 0             )
        #    self.__add__dict( tmp , 'Phase'       , 0.0           )
        #    self.__add__dict( tmp , 'Antenna'     , [0.0,0.0,0.0] )
        #    self.__add__dict( tmp , 'Amplitude'   , 0.0           )
        #    self.__add__dict( tmp , 'StreamPort'  , 6001+ch       )
        #    self.__Data.Channel.append(dict(tmp),'')


        self.__Data.Transmitter = config.Mapping()
        self.__Data.Receiver = config.Mapping()
  
        self.__Data.save(file(name=filename,mode='w'))

    #def __add__item(self,obj,val):      obj.append(val,'')
    #def __add__dict(self,obj,name,val): obj.addMapping(name,val,'',False)











if __name__ == '__main__':

    cfg = ConfigFile()
    cfg.create(filename="/home/radar/Schreibtisch/default.cfg",sdr='USRP-X300',devices=6)

    sys.exit()